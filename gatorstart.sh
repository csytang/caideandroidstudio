#!/usr/bin/env bash

# export the GatorRoot
gatorlocation=$1
export GatorRoot=$gatorlocation
source ~/.bash-profile

# target apk location
targetAPK=$2

# run python command
python $gatorlocation/AndroidBench/runGatorOnApk.py $targetAPK -client WTGDemoClient


