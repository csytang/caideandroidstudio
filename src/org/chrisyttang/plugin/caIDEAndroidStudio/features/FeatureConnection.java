package org.chrisyttang.plugin.caIDEAndroidStudio.features;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.LinkedList;

public class FeatureConnection implements PropertyConstants {

    private Feature source;

    private Feature target;

    /**
     *
     * @param source the source of the feature connection
     */
    public FeatureConnection(Feature source) {
        this.source = source;
    }

    /**
     *
     * @return the source of feature connection
     */
    public Feature getSource() {
        return source;
    }

    /**
     *
     * @return the target of feature connection
     */
    public Feature getTarget() {
        return target;
    }

    /**
     *
     * @param target set the target of feature connection
     */
    public void setTarget(Feature target) {
        if (this.target == target)
            return;
        this.target = target;
        fireParentChanged();
    }

    private LinkedList<PropertyChangeListener> listenerList = new LinkedList<PropertyChangeListener>();

    /**
     *
     * @param listener add a property change listener to this feature connection
     */
    public void addListener(PropertyChangeListener listener) {
        if (!listenerList.contains(listener))
            listenerList.add(listener);
    }

    /**
     *
     * @param listener remove a certain listener of feature connection
     */
    public void removeListener(PropertyChangeListener listener) {
        listenerList.remove(listener);
    }


    /**
     *
     */
    private void fireParentChanged() {
        PropertyChangeEvent event = new PropertyChangeEvent(this, PARENT_CHANGED, false, true);
        for (PropertyChangeListener listener : listenerList)
            listener.propertyChange(event);
    }

}
