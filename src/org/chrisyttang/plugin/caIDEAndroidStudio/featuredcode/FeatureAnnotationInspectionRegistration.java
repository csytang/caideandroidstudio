package org.chrisyttang.plugin.caIDEAndroidStudio.featuredcode;

import com.intellij.codeInspection.InspectionToolProvider;
import com.intellij.openapi.components.ApplicationComponent;
import org.jetbrains.annotations.NotNull;

public class FeatureAnnotationInspectionRegistration implements ApplicationComponent, InspectionToolProvider {



    public FeatureAnnotationInspectionRegistration(){

    }

    @Override
    public void initComponent() {
        // do nothing
    }

    @Override
    public void disposeComponent() {
        // do nothing
    }


    @NotNull
    @Override
    public String getComponentName() {
        return "FeatureAnnotationInspectionregistration";
    }

    @NotNull
    @Override
    public Class[] getInspectionClasses() {
        return new Class[]{FeatureAnnotationInspection.class};
    }
}
