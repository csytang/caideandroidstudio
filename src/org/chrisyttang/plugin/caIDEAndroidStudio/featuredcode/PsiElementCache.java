package org.chrisyttang.plugin.caIDEAndroidStudio.featuredcode;

import com.intellij.openapi.components.AbstractProjectComponent;
import com.intellij.openapi.project.Project;
import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiElement;
import org.chrisyttang.plugin.caIDEAndroidStudio.util.PsiFormater;

import java.util.HashMap;
import java.util.Map;

public class PsiElementCache extends AbstractProjectComponent {

    private Project _project;
    private Map<PsiElement,String> psiElementIdMap = new HashMap<PsiElement,String>();

    private PsiElementCache(Project project) {
        super(project);
        this._project = project;
    }

    private static PsiElementCache instance;

    public static PsiElementCache getInstance(Project project){
        if(instance==null){
            instance = new PsiElementCache(project);
        }
        return instance;
    }



    public String getId(PsiElement psiElement){
        if(psiElementIdMap.containsKey(psiElement)){
            return psiElementIdMap.get(psiElement);
        }
        String id = PsiFormater.getExternalName(psiElement,true);
        if (psiElement.getParent()!=null){
            if(psiElement instanceof PsiClass){
                return id;
            }
            id = getId(psiElement.getParent()) + "/" + id;
        }
        psiElementIdMap.put(psiElement,id);
        return id;
    }
}
