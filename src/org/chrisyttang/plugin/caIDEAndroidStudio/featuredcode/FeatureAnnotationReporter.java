package org.chrisyttang.plugin.caIDEAndroidStudio.featuredcode;

import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.command.WriteCommandAction;
import com.intellij.openapi.editor.Document;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;
import org.chrisyttang.plugin.caIDEAndroidStudio.features.Feature;
import org.chrisyttang.plugin.caIDEAndroidStudio.util.PsiFormater;
import org.chrisyttang.plugin.caIDEAndroidStudio.util.RunnableHelper;

import java.util.Map;
import java.util.Set;

public class FeatureAnnotationReporter {

    private Map<PsiFile, Set<FeaturedCodeDescriptor>> _problems;
    private Project _project;

    // add the featured code descriptor to feature annotation file if not exits
    private FeatureAnnotationFile annotationFile = null;

    private static FeatureAnnotationReporter instance;
    private FeatureAnnotationReporter(Project project){
        this._project = project;
        FeatureAnnotationCache cache = FeatureAnnotationCache.getInstance(this._project);
        _problems = cache.getAnnotations();
        instance = this;
    }

    public static FeatureAnnotationReporter getInstance(Project project){
        if(instance==null){
            instance = new FeatureAnnotationReporter(project);
        }
        return instance;
    }

    /**
     * start a new annotation
     * @param file
     * @param element
     * @param document
     * @param feature
     */
    public void publishNewFeatureAnnotation(PsiFile file, PsiElement element, Document document, Feature feature,boolean addToXML) {

        VirtualFile virtualFile = file.getVirtualFile();

        final FeaturedCodeDescriptor descriptor = new FeaturedCodeDescriptor(element, document, feature);


        // annotate to feature file
        FeatureAnnotationCache cache = FeatureAnnotationCache.getInstance(_project);

        //FeatureIntentionAction intentionAction = new FeatureIntentionAction(descriptor);

        cache.addFileToAnnotations(file, descriptor);


        Map<VirtualFile, FeatureAnnotationFile> _sourceToAnnotationXMLs = cache.getsourceToAnnotationXMls();

        Runnable mappingrunnable = new Runnable() {
            @Override
            public void run() {
                if (_sourceToAnnotationXMLs.containsKey(virtualFile)) {
                    annotationFile = _sourceToAnnotationXMLs.get(virtualFile);
                } else {
                    // add the featured code descriptor to feature annotation file if not exits
                    annotationFile = new FeatureAnnotationFile(file);
                    cache.addSourceToAnnotationXML(virtualFile, annotationFile);
                }
            }
        };

        RunnableHelper.runWriteCommand(_project, mappingrunnable);


        if (addToXML) {
            // add the PsiElement to xml
            /*
             * @param listOwner
             * @param annotationFQName feature
             * @param codeUsageFile
             * @param values
             * @param externalName
             */

            PsiElement[] pairs = new PsiElement[1];
            PsiElement pair = element;
            pairs[0] = pair;


            Runnable runnable = new Runnable() {
                @Override
                public void run() {


                    String externalName = PsiFormater.getExternalName(element, true);

                    annotationFile.addNewAnnotation(feature.getName(), pairs, externalName);
                }
            };

            RunnableHelper.runWriteCommand(this._project, runnable);

        }
    }


}
