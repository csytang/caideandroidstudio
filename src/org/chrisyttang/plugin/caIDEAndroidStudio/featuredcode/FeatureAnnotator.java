package org.chrisyttang.plugin.caIDEAndroidStudio.featuredcode;

import com.intellij.lang.annotation.Annotation;
import com.intellij.lang.annotation.AnnotationHolder;
import com.intellij.lang.annotation.Annotator;
import com.intellij.openapi.editor.markup.EffectType;
import com.intellij.openapi.editor.markup.TextAttributes;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.util.TextRange;
import com.intellij.psi.PsiAnnotation;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;
import com.intellij.ui.JBColor;
import org.chrisyttang.plugin.caIDEAndroidStudio.features.Feature;
import org.jetbrains.annotations.NotNull;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class FeatureAnnotator implements Annotator {

    private Project _project;
    public FeatureAnnotator(){

    }



    /**
     *
     * @param psiElement here the psiElement is any element, then, check whether it should be annotated
     * @param annotationHolder
     */
    @Override
    public void annotate(@NotNull PsiElement psiElement, @NotNull AnnotationHolder annotationHolder) {

        final Project project = psiElement.getProject();
        _project = project;

        final FeatureAnnotationCache cache = project.getComponent(FeatureAnnotationCache.class);

        if(cache==null){
            return;
        }

        Map<PsiFile, Set<FeaturedCodeDescriptor>> annotations = cache.getAnnotations();

        final PsiFile file = psiElement.getContainingFile();

        if(annotations.containsKey(file)){
            addAnnotation(psiElement, new ArrayList<FeaturedCodeDescriptor>(annotations.get(file)), annotationHolder);
        }
    }

    /**
     * start the annotation
     * @param psiElement the
     * @param featuredCodeDescriptors
     * @param annotationHolder
     */
    private void addAnnotation(final PsiElement psiElement, final Iterable<FeaturedCodeDescriptor> featuredCodeDescriptors,
                               @NotNull final AnnotationHolder annotationHolder){
        final List<FeaturedCodeDescriptor> matchingDescriptors = new ArrayList<FeaturedCodeDescriptor>();

        for(final FeaturedCodeDescriptor descriptor: featuredCodeDescriptors){
            final PsiElement codeelement = descriptor.getPsiElement();
            if(psiElement==codeelement) {
                matchingDescriptors.add(descriptor);
                // create the annotation for this element;
                addAnnotation(descriptor, matchingDescriptors, codeelement, annotationHolder);
            }
        }

    }


    private void addAnnotation(final FeaturedCodeDescriptor featuredCodeDescriptor, final List<FeaturedCodeDescriptor> matchingDescriptors, final PsiElement psiElement, @NotNull final AnnotationHolder annotationHolder){
        final Feature feature = featuredCodeDescriptor.getFeature();
        final Annotation annotation;
        final PsiAnnotation psiAnnotation;
        final PsiElement codeElement = featuredCodeDescriptor.getPsiElement();
        final TextRange textRange = codeElement.getTextRange();


        annotation = annotationHolder.createInfoAnnotation(psiElement,feature.getName());
        if(feature.getColor(_project)!=null){
            annotation.setEnforcedTextAttributes(new TextAttributes(null, feature.getColor(_project), JBColor.BLACK, EffectType.WAVE_UNDERSCORE, Font.PLAIN));
        }else{
            annotation.setEnforcedTextAttributes(new TextAttributes(null, null, JBColor.BLACK, EffectType.WAVE_UNDERSCORE, Font.PLAIN));
        }


    }
}
