package org.chrisyttang.plugin.caIDEAndroidStudio.featuredcode;

import com.intellij.psi.PsiElement;
import org.chrisyttang.plugin.caIDEAndroidStudio.features.Feature;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class FeaturedCodeCollection {

    private Map<Feature,Set<PsiElement>> featureToProgElements = null;

    private static FeaturedCodeCollection instance = null;

    private FeaturedCodeCollection(){
        featureToProgElements = new HashMap<Feature,Set<PsiElement>>();
        instance = this;
    }

    public static FeaturedCodeCollection getInstance(){
        if(instance==null){
            instance = new FeaturedCodeCollection();
        }
        return instance;
    }

    /**
     *
     * @param feature given input feature
     * @return elements belong to this feature
     */
    public Set<PsiElement> getElementsForFeature(Feature feature){
        if(featureToProgElements.containsKey(feature)){
            return featureToProgElements.get(feature);
        }else{
            return new HashSet<PsiElement>();
        }
    }

    /**
     *
     * @param feature given the input feature
     * @param elementset map the feature with its implementation
     */
    public void addFeatureToProgElements(Feature feature,Set<PsiElement>elementset){
        if(featureToProgElements.containsKey(feature)){
            Set<PsiElement> existElements = featureToProgElements.get(feature);
            existElements.addAll(elementset);
            featureToProgElements.put(feature,existElements);
        }else{
            featureToProgElements.put(feature,elementset);
        }
    }

    /**
     *
     * @param feature the given feature
     * @param element the psielement belongs to this feature
     */
    public void addFeatureToProgElements(Feature feature,PsiElement element){
        if(featureToProgElements.containsKey(feature)){
            Set<PsiElement> existElements = featureToProgElements.get(feature);
            existElements.add(element);
            featureToProgElements.put(feature,existElements);
        }else{
            Set<PsiElement> elementset = new HashSet<PsiElement>();
            elementset.add(element);
            featureToProgElements.put(feature,elementset);
        }
    }

    /**
     *
     * @param feature the given feature
     * @param elementset psielement set belongs to this feature
     */
    public void setFeatureToProgElements(Feature feature,Set<PsiElement>elementset){
        addFeatureToProgElements(feature,elementset);
    }



}
