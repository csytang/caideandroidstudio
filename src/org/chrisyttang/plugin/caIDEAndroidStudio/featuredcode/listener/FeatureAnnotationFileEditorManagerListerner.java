package org.chrisyttang.plugin.caIDEAndroidStudio.featuredcode.listener;

import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.fileEditor.FileEditorManager;
import com.intellij.openapi.fileEditor.FileEditorManagerEvent;
import com.intellij.openapi.fileEditor.FileEditorManagerListener;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiFile;
import org.chrisyttang.plugin.caIDEAndroidStudio.featuredcode.FeatureAnnotationCache;
import org.chrisyttang.plugin.caIDEAndroidStudio.util.PsiFileUtil;
import org.jetbrains.annotations.NotNull;

import java.util.Map;

public class FeatureAnnotationFileEditorManagerListerner implements FileEditorManagerListener {



    @Override
    public void fileOpened(@NotNull FileEditorManager source, @NotNull VirtualFile file) {
        Project project = source.getProject();
        PsiFile psiFile = PsiFileUtil.VirtualFileToPsiFileConverter(file,project);
        FeatureAnnotationCache cache = FeatureAnnotationCache.getInstance(project);

        // get current editor
        Editor editor = source.getSelectedTextEditor();

        Map<PsiFile,FeatureAnnotationDocumentListener>  _sourceToDocumentationListeners = cache.getSourceToDocumentationListeners();

        if(!_sourceToDocumentationListeners.containsKey(psiFile)){
            FeatureAnnotationDocumentListener listener = new FeatureAnnotationDocumentListener(editor,psiFile);
            source.getSelectedTextEditor().getDocument().addDocumentListener(listener);

            // add the listerner to cache
            cache.addSourceToFeatureAnnotationDocumentListener(psiFile,listener);
        }

    }

    @Override
    public void fileClosed(@NotNull FileEditorManager source, @NotNull VirtualFile file) {

    }

    @Override
    public void selectionChanged(@NotNull FileEditorManagerEvent event) {

    }
}
