package org.chrisyttang.plugin.caIDEAndroidStudio.featuredcode.listener;

import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.editor.event.DocumentEvent;
import com.intellij.openapi.editor.event.DocumentListener;
import com.intellij.psi.PsiFile;

public class FeatureAnnotationDocumentListener implements DocumentListener {

    private PsiFile _psiFile;
    private Editor _editor;
    public FeatureAnnotationDocumentListener(Editor editor, PsiFile psiFile) {
        _psiFile = psiFile;
        _editor = editor;

    }

    public void beforeDocumentChange(DocumentEvent event) {


    }

    public void documentChanged(DocumentEvent event) {

    }

    public synchronized void redraw() {

    }
}
