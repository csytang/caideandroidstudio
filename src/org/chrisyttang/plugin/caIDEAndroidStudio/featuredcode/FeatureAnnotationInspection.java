package org.chrisyttang.plugin.caIDEAndroidStudio.featuredcode;

import com.intellij.codeHighlighting.HighlightDisplayLevel;
import com.intellij.codeInspection.AbstractBaseJavaLocalInspectionTool;
import com.intellij.codeInspection.InspectionManager;
import com.intellij.codeInspection.ProblemDescriptor;
import com.intellij.openapi.diagnostic.Logger;
import com.intellij.openapi.editor.Document;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.*;
import com.intellij.psi.xml.XmlElement;
import com.intellij.psi.xml.XmlFile;
import com.intellij.psi.xml.XmlTag;
import org.chrisyttang.plugin.caIDEAndroidStudio.featuremodel.FeatureModel;
import org.chrisyttang.plugin.caIDEAndroidStudio.features.Feature;
import org.chrisyttang.plugin.caIDEAndroidStudio.util.EditorPsiElementUtil;
import org.chrisyttang.plugin.caIDEAndroidStudio.util.PsiFileUtil;
import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class FeatureAnnotationInspection extends AbstractBaseJavaLocalInspectionTool {


    private Logger LOG = Logger.getInstance("org.chrisyttang.plugin.caIDEAndroidStudio.featuredcode.FeatureAnnotationInspection");


    @Override
    public boolean isEnabledByDefault() {
        return true;
    }

    @NotNull
    @Override
    public String getGroupDisplayName() {
        return "caIDEAndroidStudio";
    }

    @NotNull
    @Override
    public String[] getGroupPath() {
        return new String[]{"caIDEAndroidStudio","Java"};
    }

    @Nls
    @NotNull
    @Override
    public String getDisplayName() {
        return "Feature Annotation Inspection";
    }

    @NotNull
    @Override
    public HighlightDisplayLevel getDefaultLevel() {
        return HighlightDisplayLevel.INFO;
    }



    /**
     *
     * @param file each file to be checked
     * @param manager
     * @param isOnTheFly
     * @return
     */
    @Nullable
    @Override
    public ProblemDescriptor[] checkFile(@NotNull PsiFile file, @NotNull InspectionManager manager, boolean isOnTheFly) {
        FeatureAnnotationCache featureAnnotationCache = FeatureAnnotationCache.getInstance(file.getProject());

        if(featureAnnotationCache.getAnnotations().containsKey(file)){
            Set<FeaturedCodeDescriptor> annotationset = featureAnnotationCache.getAnnotations().get(file);
            return annotationset.toArray(new ProblemDescriptor[annotationset.size()]);
        }else{
            // check whether the its color file exist in the repository
            String filetype = file.getFileType().getName();
            if(filetype.toLowerCase().equals("java")){
                PsiJavaFile psiJavaFile = (PsiJavaFile)file;
                Document javaDocument = PsiDocumentManager.getInstance(file.getProject()).getDocument(file);

                VirtualFile sourcefile = file.getVirtualFile();
                String fileName  = sourcefile.getName();
                String xmlfileName = fileName.substring(0,fileName.length()-".java".length())+"_color.xml";
                VirtualFile colorFile = sourcefile.getParent().findChild(xmlfileName);
                PsiClass[] psiClasses = psiJavaFile.getClasses();

                if(colorFile!=null){
                    // add the mapping between source code file and annotation xml
                    FeatureAnnotationFile annotationFile = new FeatureAnnotationFile(file);
                    featureAnnotationCache.addSourceToAnnotationXML(file.getVirtualFile(),annotationFile);

                    // now let's extract all annotation from the xml file
                    PsiFile psiColorFile = PsiFileUtil.VirtualFileToPsiFileConverter(colorFile,file.getProject());

                    Set<FeaturedCodeDescriptor> annotationset = new HashSet<FeaturedCodeDescriptor>();

                    assert psiColorFile instanceof XmlFile;

                    XmlFile colorFileXml = (XmlFile)psiColorFile;


                    // get all element in the tag
                    XmlTag rootTag = colorFileXml.getRootTag();
                    XmlTag[] itemTags = rootTag.getSubTags();

                    for(XmlTag itemTag:itemTags){

                        itemTag.accept(new XmlElementVisitor() {
                            @Override
                            public void visitXmlElement(XmlElement element) {
                                XmlTag annotationTag = itemTag.getSubTags()[0];
                                String itemName = itemTag.getAttributeValue("name");
                                String className = itemName.substring(0,itemName.indexOf("[")).split(" ")[0];

                                // return the element range
                                String elementRange = itemName.substring(1+itemName.lastIndexOf("["),itemName.lastIndexOf("]"));
                                int elementStart = Integer.parseInt(elementRange.substring(0,elementRange.indexOf(",")));
                                int elementEnd = Integer.parseInt(elementRange.substring(1+elementRange.indexOf(","),elementRange.length()));

                                String featureName = annotationTag.getAttributeValue("name");

                                Feature feature = FeatureModel.getInstance(file.getProject()).getFeature(featureName);
                                assert feature!=null;

                                XmlTag annotationvalueTag = annotationTag.getSubTags()[0];

                                List<PsiElement> annotatedelements = EditorPsiElementUtil.getAllPsiElementInRange(elementStart,elementEnd,file);


                                for (PsiElement annotatedelement : annotatedelements) {
                                    FeaturedCodeDescriptor descriptor = new FeaturedCodeDescriptor(annotatedelement, javaDocument, feature);
                                    annotationset.add(descriptor);
                                }

                                //super.visitXmlElement(element);
                            }
                        });

                    }

                    FeatureAnnotationCache cache = FeatureAnnotationCache.getInstance(file.getProject());
                    cache.addFileToAnnotations(file,annotationset);

                    return annotationset.toArray(new ProblemDescriptor[annotationset.size()]);
                }else{
                    return new ProblemDescriptor[0];
                }
            }else{
                return new ProblemDescriptor[0];
            }
        }

    }


}
