package org.chrisyttang.plugin.caIDEAndroidStudio.featuredcode;

import com.intellij.openapi.components.AbstractProjectComponent;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiFile;
import org.chrisyttang.plugin.caIDEAndroidStudio.featuredcode.listener.FeatureAnnotationDocumentListener;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;


public class FeatureAnnotationCache  extends AbstractProjectComponent {

    private static Map<PsiFile, Set<FeaturedCodeDescriptor>> _annotations;
    private static Map<VirtualFile, FeatureAnnotationFile> _sourceToAnnotationXMLs;
    private static Map<PsiFile,FeatureAnnotationDocumentListener>_sourceToDocumentationListeners;
    private Project _project;


    private FeatureAnnotationCache(final Project project) {
        super(project);
        _annotations = new HashMap<PsiFile, Set<FeaturedCodeDescriptor>>();
        _sourceToAnnotationXMLs = new HashMap<VirtualFile, FeatureAnnotationFile>();
        _sourceToDocumentationListeners = new HashMap<PsiFile, FeatureAnnotationDocumentListener>();

    }


    public Map<PsiFile, Set<FeaturedCodeDescriptor>> getAnnotations(){
        return _annotations;
    }

    public Map<VirtualFile, FeatureAnnotationFile> getsourceToAnnotationXMls(){
        return _sourceToAnnotationXMLs;
    }

    public Map<PsiFile,FeatureAnnotationDocumentListener> getSourceToDocumentationListeners(){
        return _sourceToDocumentationListeners;
    }

    public static FeatureAnnotationCache getInstance(Project project) {
        return project.getComponent(FeatureAnnotationCache.class);
    }

    /**
     * this function will add a mapping between a source code file and a color annotation
     * @param sourcefile
     * @param colorannotation
     */
    public void addSourceToAnnotationXML(VirtualFile sourcefile, FeatureAnnotationFile colorannotation){
        _sourceToAnnotationXMLs.put(sourcefile,colorannotation);

    }


    /**
     * this funciton will add feature annotation to psifile mapping
     * @param psiFile
     * @param codeDescriptors
     */
    public void addFileToAnnotations(PsiFile psiFile, Set<FeaturedCodeDescriptor> codeDescriptors){
        if(_annotations.containsKey(psiFile)){
            Set<FeaturedCodeDescriptor> featuredCodeDescriptorList = _annotations.get(psiFile);
            featuredCodeDescriptorList.addAll(codeDescriptors);
            _annotations.put(psiFile,featuredCodeDescriptorList);
        }else{
            Set<FeaturedCodeDescriptor> featuredCodeDescriptorList = new HashSet<FeaturedCodeDescriptor>();
            featuredCodeDescriptorList.addAll(codeDescriptors);
            _annotations.put(psiFile,featuredCodeDescriptorList);
        }
    }

    /**
     * this funciton will add feature annotation to psifile mapping
     * @param psiFile
     * @param codeDescriptor
     */
    public void addFileToAnnotations(PsiFile psiFile, FeaturedCodeDescriptor codeDescriptor){
        if(_annotations.containsKey(psiFile)){
            Set<FeaturedCodeDescriptor> featuredCodeDescriptorList = _annotations.get(psiFile);
            if(!featuredCodeDescriptorList.contains(codeDescriptor))
                featuredCodeDescriptorList.add(codeDescriptor);
            _annotations.put(psiFile,featuredCodeDescriptorList);
        }else{
            Set<FeaturedCodeDescriptor> featuredCodeDescriptorList = new HashSet<FeaturedCodeDescriptor>();
            featuredCodeDescriptorList.add(codeDescriptor);
            _annotations.put(psiFile,featuredCodeDescriptorList);
        }
    }


    public void addSourceToFeatureAnnotationDocumentListener(PsiFile sourceFile, FeatureAnnotationDocumentListener listener){
        _sourceToDocumentationListeners.put(sourceFile,listener);
    }

}
