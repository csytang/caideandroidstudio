package org.chrisyttang.plugin.caIDEAndroidStudio.featuredcode;

import com.intellij.ide.highlighter.XmlFileType;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.command.WriteCommandAction;
import com.intellij.openapi.command.undo.UndoUtil;
import com.intellij.openapi.diagnostic.Logger;
import com.intellij.openapi.module.Module;
import com.intellij.openapi.module.ModuleManager;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.roots.*;
import com.intellij.openapi.util.Comparing;
import com.intellij.openapi.util.text.StringUtil;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.*;
import com.intellij.psi.util.PsiTreeUtil;
import com.intellij.psi.xml.XmlDocument;
import com.intellij.psi.xml.XmlFile;
import com.intellij.psi.xml.XmlTag;
import com.intellij.util.IncorrectOperationException;
import com.intellij.util.messages.MessageBus;
import com.intellij.xml.util.XmlUtil;
import org.jetbrains.annotations.NonNls;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.*;

public class FeatureAnnotationFile {

    private PsiFile _sourcefile; // the source code file
    private VirtualFile _sourcefile_virtualfile; // virtul
    private XmlFile _colorannotation_xml;// the color file will be named in xml format
    private final MessageBus _myBus;
    private Project _project;
    private PsiManager _psiManager;
    private Logger LOG = Logger.getInstance("org.chrisyttang.plugin.caIDEAndroidStudio.featuredcode.FeatureAnnotationFile");
    private Set<VirtualFile> myAnnotationsRoots;
    private static PsiElementCache _psiElementCache;

    /**
     *
     * @param sourcefile input source file
     */
    public FeatureAnnotationFile(PsiFile sourcefile){
        _sourcefile = sourcefile;
        _sourcefile_virtualfile = sourcefile.getVirtualFile();
        _project = sourcefile.getProject();
        _myBus = _project.getMessageBus();
        _psiManager = PsiManager.getInstance(_project);
        final PsiDirectory directory = _psiManager.findDirectory(_sourcefile_virtualfile.getParent());
        String fileName  = sourcefile.getName();
        String xmlfileName = fileName.substring(0,fileName.length()-".java".length())+"_color.xml";
        _psiElementCache = PsiElementCache.getInstance(_project);

        final PsiFile psiFile = directory.findFile(xmlfileName);
        if(psiFile!=null && psiFile instanceof XmlFile){
            _colorannotation_xml = (XmlFile)psiFile;
        }else {
            createAnnotationsXml(_sourcefile_virtualfile,_psiManager);
        }

    }

    /**
     *
     * @return source code file
     */
    public PsiFile getSourceFile(){
        return _sourcefile;
    }

    /**
     *
     * @return color annotation file
     */
    public XmlFile getColorSourceFile() {
        return _colorannotation_xml;
    }





    /**
     *
     * @param annotationFQName the name of feature
     * @param values
     * @return null
     */
    public static String createAnnotationTag(@NotNull String annotationFQName, @Nullable PsiElement[] values) {
        @NonNls String text;
        if (values != null && values.length != 0) {
            text = "  <annotation name=\'" + annotationFQName + "\'>\n";
            text += StringUtil.join(values, pair -> "<element val=\"" + StringUtil.escapeXml(_psiElementCache.getId(pair)) + "\"/>", "    \n");
            text += "  </annotation>";
        }
        else {
            text = "  <annotation name=\'" + annotationFQName + "\'/>\n";
        }
        return text;
    }

    public static PsiModifierListOwner getContainer(final PsiFile file, int offset) {
        PsiReference reference = file.findReferenceAt(offset);
        if (reference != null) {
            PsiElement target = reference.resolve();
            if (target instanceof PsiMember) {
                return (PsiMember)target;
            }
        }

        PsiElement element = file.findElementAt(offset);

        PsiModifierListOwner listOwner = PsiTreeUtil.getParentOfType(element, PsiModifierListOwner.class, false);
        return listOwner;
    }

    /**
     *
     *
     * This function gives how to add the notation to
     *
     *                     <item name = "...">
     *                         <annotation name = "">
     *                         </annotation>
     *                     </item>

     */
    protected void addNewAnnotation(
                                    @NotNull final String annotationFQName,
                                    @Nullable final PsiElement[] values,
                                    @Nullable final String externalName
                                    ) {
        if (_colorannotation_xml == null) {
            try {
                throw new Exception("Cannot add annotation to unfound colorsource file");
            } catch (Exception e) {
                e.printStackTrace();
                System.exit(-1);
            }
        }

            try {
                final XmlDocument document = _colorannotation_xml.getDocument();
                if (document != null) {
                    XmlTag rootTag = document.getRootTag();

                    if (rootTag != null && externalName != null) {
                        XmlTag anchor = null;
                        for (XmlTag item : rootTag.getSubTags()) {
                            int compare = Comparing.compare(externalName, StringUtil.unescapeXml(item.getAttributeValue("name")));
                            if (compare == 0) {
                                anchor = null;
                                for (XmlTag annotation : item.getSubTags()) {
                                    compare = Comparing.compare(annotationFQName, annotation.getAttributeValue("name"));
                                    if (compare == 0) {
                                        annotation.delete();
                                        break;
                                    }
                                    anchor = annotation;
                                }
                                XmlTag newTag = XmlElementFactory.getInstance(_project).createTagFromText(
                                        createAnnotationTag(annotationFQName, values));

                                item.addAfter(newTag, anchor);
                                return;
                            }
                            if (compare < 0)
                                break;
                            anchor = item;
                        }
                        final String text =
                                "<item name=\'" + StringUtil.escapeXml(externalName) + "[" + values[0].getTextRange().getStartOffset() + "," + values[0].getTextRange().getEndOffset() + "]" + "\'>\n" +
                                        createAnnotationTag(annotationFQName, values) + "</item>";


                        new WriteCommandAction.Simple(_project, _colorannotation_xml) {
                            @Override
                            protected void run() throws Throwable {
                                XmlTag subTag = XmlElementFactory.getInstance(_project).createTagFromText(text);
                                subTag = rootTag.addSubTag(subTag, false);
                            }
                        }.execute();

                        return;
                    }
                }
            } catch (IncorrectOperationException e) {

                LOG.error(e + "In files:" + _sourcefile.getVirtualFile().getPath());
            }


    }



    protected synchronized void dropCache() {
        myAnnotationsRoots = null;
    }

    @Nullable
    private XmlFile createAnnotationsXml(VirtualFile sourcefile, PsiManager manager) {

        final PsiDirectory directory = manager.findDirectory(sourcefile.getParent());
        if (directory == null)
            return null;

        String fileName  = sourcefile.getName();
        String xmlfileName = fileName.substring(0,fileName.length()-".java".length())+"_color.xml";

        final PsiFile psiFile = directory.findFile(xmlfileName);
        if (psiFile instanceof XmlFile) {
            _colorannotation_xml = (XmlFile)psiFile;
            return _colorannotation_xml;
        }

        try {

            Runnable runnable = new Runnable() {
                @Override
                public void run() {

                    final PsiFileFactory factory = PsiFileFactory.getInstance(manager.getProject());
                    _colorannotation_xml = (XmlFile)factory.createFileFromText(xmlfileName, XmlFileType.INSTANCE, "<root></root>");
                    directory.add(_colorannotation_xml);

                }
            };
            WriteCommandAction.runWriteCommandAction(this._project, runnable);


            return _colorannotation_xml;
        }
        catch (IncorrectOperationException e) {

        }
        return null;
    }

    public PsiManager getPsiManger() {
        return this._psiManager;
    }


    public boolean hasAnnotationRootsForFile(@NotNull VirtualFile file) {
        if (hasAnyAnnotationsRoots()) {
            ProjectFileIndex fileIndex = ProjectRootManager.getInstance(this._project).getFileIndex();
            for (OrderEntry entry : fileIndex.getOrderEntriesForFile(file)) {
                if (!(entry instanceof ModuleOrderEntry) && AnnotationOrderRootType.getUrls(entry).length > 0) {
                    return true;
                }
            }
        }
        return false;
    }


    protected boolean hasAnyAnnotationsRoots() {
        return !initRoots().isEmpty();
    }


    @NotNull
    private synchronized Set<VirtualFile> initRoots() {
        if (myAnnotationsRoots == null) {
            myAnnotationsRoots = new HashSet<>();
            final Module[] modules = ModuleManager.getInstance(_project).getModules();
            for (Module module : modules) {
                for (OrderEntry entry : ModuleRootManager.getInstance(module).getOrderEntries()) {
                    final VirtualFile[] files = AnnotationOrderRootType.getFiles(entry);
                    if (files.length > 0) {
                        Collections.addAll(myAnnotationsRoots, files);
                    }
                }
            }
        }
        return myAnnotationsRoots;
    }

}
