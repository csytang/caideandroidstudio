package org.chrisyttang.plugin.caIDEAndroidStudio.featuredcode;

import com.intellij.codeInspection.LocalQuickFix;
import com.intellij.codeInspection.ProblemDescriptorBase;
import com.intellij.codeInspection.ProblemHighlightType;
import com.intellij.codeInspection.QuickFix;
import com.intellij.lang.annotation.ProblemGroup;
import com.intellij.openapi.editor.Document;
import com.intellij.openapi.editor.colors.TextAttributesKey;
import com.intellij.openapi.util.TextRange;
import com.intellij.psi.PsiElement;
import org.chrisyttang.plugin.caIDEAndroidStudio.features.Feature;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.HashMap;

/**
 *
 */
public class FeaturedCodeDescriptor extends ProblemDescriptorBase {

    private final Feature _feature;
    private final Document _document;
    private PsiElement _element;
    private int lineStart = 0;
    private ProblemGroup _featuregroup;


    public FeaturedCodeDescriptor(PsiElement element, final Document document, final Feature feature){
        super(element,element,feature.getName(),new LocalQuickFix[0], ProblemHighlightType.INFORMATION,false,element.getTextRange(),false,false);
        this._feature = feature;
        this._element = element;
        this._document = document;
        lineStart = this._document.getLineNumber(this._element.getTextOffset());
    }

    @Override
    public PsiElement getPsiElement() {
        return _element;
    }

    @Override
    public PsiElement getStartElement() {
        return _element;
    }

    @Override
    public PsiElement getEndElement() {
        return _element;
    }

    @Override
    public TextRange getTextRangeInElement() {
        return _element.getTextRange();
    }

    @Override
    public int getLineNumber() {
        return lineStart;
    }

    @NotNull
    @Override
    public ProblemHighlightType getHighlightType() {
        return ProblemHighlightType.INFORMATION;
    }

    @Override
    public boolean isAfterEndOfLine() {
        return false;
    }

    @Override
    public void setTextAttributes(TextAttributesKey textAttributesKey) {

    }

    @Nullable
    @Override
    public ProblemGroup getProblemGroup() {
        return _featuregroup;
    }

    @Override
    public void setProblemGroup(@Nullable ProblemGroup problemGroup) {
        _featuregroup = problemGroup;
    }

    @Override
    public boolean showTooltip() {
        return false;
    }

    @NotNull
    @Override
    public String getDescriptionTemplate() {
        return "<description template>";
    }

    /**
     *
     * @return no fixes, since they are general information
     */
    @Override
    public QuickFix[] getFixes() {
        return null;
    }

    @Nullable
    public String getProblemName() {
        return "[caIDE]:"+_feature.getName();
    }

    public Feature getFeature(){
        return _feature;
    }

    /**
     * Indicates whether some other object is "equal to" this one.
     * <p>
     * The {@code equals} method implements an equivalence relation
     * on non-null object references:
     * <ul>
     * <li>It is <i>reflexive</i>: for any non-null reference value
     * {@code x}, {@code x.equals(x)} should return
     * {@code true}.
     * <li>It is <i>symmetric</i>: for any non-null reference values
     * {@code x} and {@code y}, {@code x.equals(y)}
     * should return {@code true} if and only if
     * {@code y.equals(x)} returns {@code true}.
     * <li>It is <i>transitive</i>: for any non-null reference values
     * {@code x}, {@code y}, and {@code z}, if
     * {@code x.equals(y)} returns {@code true} and
     * {@code y.equals(z)} returns {@code true}, then
     * {@code x.equals(z)} should return {@code true}.
     * <li>It is <i>consistent</i>: for any non-null reference values
     * {@code x} and {@code y}, multiple invocations of
     * {@code x.equals(y)} consistently return {@code true}
     * or consistently return {@code false}, provided no
     * information used in {@code equals} comparisons on the
     * objects is modified.
     * <li>For any non-null reference value {@code x},
     * {@code x.equals(null)} should return {@code false}.
     * </ul>
     * <p>
     * The {@code equals} method for class {@code Object} implements
     * the most discriminating possible equivalence relation on objects;
     * that is, for any non-null reference values {@code x} and
     * {@code y}, this method returns {@code true} if and only
     * if {@code x} and {@code y} refer to the same object
     * ({@code x == y} has the value {@code true}).
     * <p>
     * Note that it is generally necessary to override the {@code hashCode}
     * method whenever this method is overridden, so as to maintain the
     * general contract for the {@code hashCode} method, which states
     * that equal objects must have equal hash codes.
     *
     * @param obj the reference object with which to compare.
     * @return {@code true} if this object is the same as the obj
     * argument; {@code false} otherwise.
     * @see #hashCode()
     * @see HashMap
     */
    @Override
    public boolean equals(Object obj) {
        if(obj instanceof FeaturedCodeDescriptor){
            FeaturedCodeDescriptor descriptor = (FeaturedCodeDescriptor)obj;

            if(descriptor._element.equals(this._element)){
                return true;
            }else{
                return false;
            }

        }else{
            return false;
        }
    }
}
