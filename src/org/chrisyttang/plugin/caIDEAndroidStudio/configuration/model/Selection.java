package org.chrisyttang.plugin.caIDEAndroidStudio.configuration.model;

import org.chrisyttang.plugin.caIDEAndroidStudio.configuration.nodes.Variable;

public enum Selection {

    SELECTED(Variable.TRUE), UNSELECTED(Variable.FALSE), UNDEFINED(Variable.UNDEFINED);

    private final int value;

    private Selection(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return super.toString();
    }

}
