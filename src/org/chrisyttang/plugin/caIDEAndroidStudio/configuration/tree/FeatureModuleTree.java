package org.chrisyttang.plugin.caIDEAndroidStudio.configuration.tree;

import com.intellij.util.ui.Tree;

public class FeatureModuleTree extends Tree {

    public FeatureModuleTree(FeatureModelTreeModel treeModel){
        super(treeModel);
    }

    public FeatureModuleTree(LeafFeatureTreeNode root){
        super(root);
    }

}
