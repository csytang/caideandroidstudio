package org.chrisyttang.plugin.caIDEAndroidStudio.configuration.tree;

import org.chrisyttang.plugin.caIDEAndroidStudio.features.Feature;

import javax.swing.tree.TreeNode;
import java.util.Enumeration;

public class LeafFeatureTreeNode implements TreeNode {

    private TreeNode _parent;
    private Object _userObject;
    private boolean _checked;

    public LeafFeatureTreeNode(TreeNode parent, Object userObject){
        this._parent = parent;
        this._userObject = userObject;
        this._checked = false;
    }

    public void setParent(TreeNode parent){
        this._parent = parent;
    }
    public void setSelect(boolean selection){
        this._checked = selection;
    }

    public void click(){
        this._checked = !this._checked;
    }
    /**
     * Returns the child <code>TreeNode</code> at index
     * <code>childIndex</code>.
     *
     * @param childIndex
     */
    @Override
    public TreeNode getChildAt(int childIndex) {
        return null;
    }

    /**
     * Returns the number of children <code>TreeNode</code>s the receiver
     * contains.
     */
    @Override
    public int getChildCount() {
        return 0;
    }

    /**
     * Returns the parent <code>TreeNode</code> of the receiver.
     */
    @Override
    public TreeNode getParent() {
        return _parent;
    }

    /**
     * Returns the index of <code>node</code> in the receivers children.
     * If the receiver does not contain <code>node</code>, -1 will be
     * returned.
     *
     * @param node
     */
    @Override
    public int getIndex(TreeNode node) {
        return -1;
    }

    /**
     * Returns true if the receiver allows children.
     */
    @Override
    public boolean getAllowsChildren() {
        return false;
    }

    /**
     * Returns true if the receiver is a leaf.
     */
    @Override
    public boolean isLeaf() {
        return true;
    }

    /**
     * Returns the children of the receiver as an <code>Enumeration</code>.
     */
    @Override
    public Enumeration children() {
        return null;
    }

    public Object getUserObject() {
        return _userObject;
    }


    public boolean isCheck(){
        return _checked;
    }

    public void setChecked(final boolean checked){
        this._checked = checked;
    }

    /**
     * Returns a string representation of the object. In general, the
     * {@code toString} method returns a string that
     * "textually represents" this object. The result should
     * be a concise but informative representation that is easy for a
     * person to read.
     * It is recommended that all subclasses override this method.
     * <p>
     * The {@code toString} method for class {@code Object}
     * returns a string consisting of the name of the class of which the
     * object is an instance, the at-sign character `{@code @}', and
     * the unsigned hexadecimal representation of the hash code of the
     * object. In other words, this method returns a string equal to the
     * value of:
     * <blockquote>
     * <pre>
     * getClass().getName() + '@' + Integer.toHexString(hashCode())
     * </pre></blockquote>
     *
     * @return a string representation of the object.
     */
    @Override
    public String toString() {
        return ((Feature )_userObject).getName();
    }
}
