package org.chrisyttang.plugin.caIDEAndroidStudio.configuration.tree;

import org.chrisyttang.plugin.caIDEAndroidStudio.features.Feature;

import javax.swing.*;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.TreeCellRenderer;
import java.awt.*;

/**
 * this render enables
 *
 */
public class FeatureModelTreeCellRenderer implements TreeCellRenderer {

    JCheckBox defaultCheckBoxRenderer = new JCheckBox();
    DefaultTreeCellRenderer defaultRenderer = new DefaultTreeCellRenderer();
    public FeatureModelTreeCellRenderer(){

    }

    protected JCheckBox getCheckBoxRenderer() {
        return defaultCheckBoxRenderer;
    }

    public Component getTreeCellRendererComponent(JTree tree, Object value, boolean selected,
                                                  boolean expanded, boolean leaf, int row, boolean hasFocus) {
        Component returnValue = null;
        if (value != null /* && leaf */) {
            LeafFeatureTreeNode featureTreeNode = (LeafFeatureTreeNode)value;
            Feature feature = (Feature)featureTreeNode.getUserObject();
            defaultCheckBoxRenderer.setText(feature.getName());
            defaultCheckBoxRenderer.setEnabled(true);
            if(featureTreeNode.isCheck()){
                defaultCheckBoxRenderer.setSelected(true);
            }else{
                defaultCheckBoxRenderer.setSelected(false);
            }
            return defaultCheckBoxRenderer;
        }

        returnValue = defaultRenderer.getTreeCellRendererComponent(tree, value, selected, expanded,
                    leaf, row, hasFocus);

        return returnValue;
    }



}
