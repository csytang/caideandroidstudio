package org.chrisyttang.plugin.caIDEAndroidStudio.configuration.tree;

import javax.swing.*;
import javax.swing.event.CellEditorListener;
import javax.swing.tree.TreeCellEditor;
import java.awt.*;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.EventObject;

public class FeatureModelTreeCellEditor extends AbstractCellEditor implements TreeCellEditor {
    private FeatureModelTreeCellRenderer _featurenderer = new FeatureModelTreeCellRenderer();
    private FeatureModuleTree _featureModuleTree;

    public FeatureModelTreeCellEditor(FeatureModuleTree tree){
        _featureModuleTree = tree;
    }
    /**
     * Sets an initial <I>value</I> for the editor.  This will cause
     * the editor to stopEditing and lose any partially edited value
     * if the editor is editing when this method is called. <p>
     * <p>
     * Returns the component that should be added to the client's
     * Component hierarchy.  Once installed in the client's hierarchy
     * this component will then be able to draw and receive user input.
     *
     * @param tree       the JTree that is asking the editor to edit;
     *                   this parameter can be null
     * @param value      the value of the cell to be edited
     * @param isSelected true if the cell is to be rendered with
     *                   selection highlighting
     * @param expanded   true if the node is expanded
     * @param leaf       true if the node is a leaf node
     * @param row        the row index of the node being edited
     * @return the component for editing
     */
    @Override
    public Component getTreeCellEditorComponent(JTree tree, Object value, boolean isSelected, boolean expanded, boolean leaf, int row) {
        Component editor = _featurenderer.getTreeCellRendererComponent(tree, value, true, expanded, leaf, row, true);
        if (editor instanceof JCheckBox) {
            ((JCheckBox) editor).addItemListener(new ItemListener() {
                public void itemStateChanged(ItemEvent itemEvent) {
                    if (stopCellEditing()) {
                        fireEditingStopped();
                    }
                }
            });
        }
        return editor;
    }

    /**
     * Returns the value contained in the editor.
     *
     * @return the value contained in the editor
     */
    @Override
    public Object getCellEditorValue() {
        JCheckBox checkBox = _featurenderer.getCheckBoxRenderer();
        //JCheckBox checkBoxNode = new JCheckBox(checkBox.getText(), checkBox.isSelected());
        return checkBox;
    }

    /*
     * Returns true.
     *
     * @param e an event object
     * @return true
     */
    @Override
    public boolean isCellEditable(EventObject e) {
        return true;
    }



    /**
     * Calls <code>fireEditingStopped</code> and returns true.
     *
     * @return true
     */
    @Override
    public boolean stopCellEditing() {
        return super.stopCellEditing();
    }

    /**
     * Calls <code>fireEditingCanceled</code>.
     */
    @Override
    public void cancelCellEditing() {
        super.cancelCellEditing();
    }

    /**
     * Adds a <code>CellEditorListener</code> to the listener list.
     *
     * @param l the new listener to be added
     */
    @Override
    public void addCellEditorListener(CellEditorListener l) {
        super.addCellEditorListener(l);
    }

    /**
     * Removes a <code>CellEditorListener</code> from the listener list.
     *
     * @param l the listener to be removed
     */
    @Override
    public void removeCellEditorListener(CellEditorListener l) {
        super.removeCellEditorListener(l);
    }

    /**
     * Returns an array of all the <code>CellEditorListener</code>s added
     * to this AbstractCellEditor with addCellEditorListener().
     *
     * @return all of the <code>CellEditorListener</code>s added or an empty
     * array if no listeners have been added
     * @since 1.4
     */
    @Override
    public CellEditorListener[] getCellEditorListeners() {
        return super.getCellEditorListeners();
    }
}
