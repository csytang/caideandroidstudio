package org.chrisyttang.plugin.caIDEAndroidStudio.configuration.tree;

import com.intellij.openapi.project.Project;
import org.chrisyttang.plugin.caIDEAndroidStudio.featuremodel.FeatureModel;
import org.chrisyttang.plugin.caIDEAndroidStudio.features.Feature;

import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;
import java.util.*;

public class FeatureModelTreeModel implements TreeModel{


    private FeatureModel _featureModel;
    private Map<Feature,TreeNode> _featuretoTreeElements;
    private static FeatureModelTreeModel instance = null;//singleton

    /**
     * Singleton mode is provided for the TreeModel
     * @param project target project
     */
    private FeatureModelTreeModel(Project project){
        this._featuretoTreeElements  = new HashMap<Feature,TreeNode>();
        this._featureModel = FeatureModel.getInstance(project);
        createAllTreeNodeWithDFS();
    }

    /**
     *
     * @param project input project
     * @return singleton of feature model
     */
    public static FeatureModelTreeModel getInstance(Project project){
        if(instance == null){
            instance = new FeatureModelTreeModel(project);
        }
        return instance;
    }



    /**
     * create corresponding tree node for feature model
     *
     */
    private void createAllTreeNodeWithDFS(){
        Stack<Feature> tranversestack = new Stack<Feature>();
        Stack<Feature> seedstopstack = new Stack<Feature>();
        tranversestack.push(this._featureModel.getRoot());
        while(!tranversestack.empty()){
            Feature top = tranversestack.pop();
            if(!seedstopstack.contains(top))
                seedstopstack.add(top);
            if(top.getChildren().size()!=0){
                List<Feature> children = top.getChildren();
                tranversestack.addAll(children);
            }
        }

        // now, in the seedstopstack, leaf nodes must on the top of their parents
        // create the feature node
        while(!seedstopstack.empty()){
            Feature top = seedstopstack.pop();
            if(top.getChildren().size()==0){
                LeafFeatureTreeNode leafFeatureTreeNode = new LeafFeatureTreeNode(null,top);
                _featuretoTreeElements.put(top,leafFeatureTreeNode);
            }else{
                List<Feature> children = top.getChildren();
                List<TreeNode> childrenTreeNode = new LinkedList<TreeNode>();
                ParentFeatureTreeNode parentFeatureTreeNode = new ParentFeatureTreeNode(null,top,childrenTreeNode);
                for(Feature child:children){
                    LeafFeatureTreeNode treeNode = (LeafFeatureTreeNode)_featuretoTreeElements.get(child);
                    assert treeNode!=null;
                    treeNode.setParent(parentFeatureTreeNode);
                    _featuretoTreeElements.put(child,treeNode);
                    childrenTreeNode.add(treeNode);
                }
                _featuretoTreeElements.put(top,parentFeatureTreeNode);
            }
        }
    }

    /**
     * Returns the root of the tree.  Returns <code>null</code>
     * only if the tree has no nodes.
     *
     * @return the root of the tree
     */
    @Override
    public Object getRoot() {
        assert _featuretoTreeElements.containsKey(_featureModel.getRoot());
        return _featuretoTreeElements.get(_featureModel.getRoot());
    }

    /**
     * Returns the child of <code>parent</code> at index <code>index</code>
     * in the parent's
     * child array.  <code>parent</code> must be a node previously obtained
     * from this data source. This should not return <code>null</code>
     * if <code>index</code>
     * is a valid index for <code>parent</code> (that is <code>index &gt;= 0 &amp;&amp;
     * index &lt; getChildCount(parent</code>)).
     *
     * @param parent a node in the tree, obtained from this data source
     * @param index
     * @return the child of <code>parent</code> at index <code>index</code>
     */
    @Override
    public Object getChild(Object parent, int index) {
        if(parent instanceof ParentFeatureTreeNode){
            ParentFeatureTreeNode parentFeatureTreeNode = (ParentFeatureTreeNode)parent;
            return parentFeatureTreeNode.getChildAt(index);
        }else if(parent instanceof LeafFeatureTreeNode){
            return null;
        }
        return null;
    }

    /**
     * Returns the number of children of <code>parent</code>.
     * Returns 0 if the node
     * is a leaf or if it has no children.  <code>parent</code> must be a node
     * previously obtained from this data source.
     *
     * @param parent a node in the tree, obtained from this data source
     * @return the number of children of the node <code>parent</code>
     */
    @Override
    public int getChildCount(Object parent) {
        // parent is in the type of parentfeature tree node or leafFeature tree node
        if(parent instanceof ParentFeatureTreeNode){
            ParentFeatureTreeNode treeNode = (ParentFeatureTreeNode)parent;
            return treeNode.getChildCount();
        }else if(parent instanceof LeafFeatureTreeNode){
            return 0;
        }else{
            return 0;
        }

    }

    /**
     * Returns <code>true</code> if <code>node</code> is a leaf.
     * It is possible for this method to return <code>false</code>
     * even if <code>node</code> has no children.
     * A directory in a filesystem, for example,
     * may contain no files; the node representing
     * the directory is not a leaf, but it also has no children.
     *
     * @param node a node in the tree, obtained from this data source
     * @return true if <code>node</code> is a leaf
     */
    @Override
    public boolean isLeaf(Object node) {

        if(node instanceof ParentFeatureTreeNode){
            return false;
        }else if(node instanceof LeafFeatureTreeNode) {
            return true;
        }
        return false;
    }

    /**
     * Messaged when the user has altered the value for the item identified
     * by <code>path</code> to <code>newValue</code>.
     * If <code>newValue</code> signifies a truly new value
     * the model should post a <code>treeNodesChanged</code> event.
     *
     * @param path     path to the node that the user has altered
     * @param newValue the new value from the TreeCellEditor
     */
    @Override
    public void valueForPathChanged(TreePath path, Object newValue) {

    }

    /**
     * Returns the index of child in parent.  If either <code>parent</code>
     * or <code>child</code> is <code>null</code>, returns -1.
     * If either <code>parent</code> or <code>child</code> don't
     * belong to this tree model, returns -1.
     *
     * @param parent a node in the tree, obtained from this data source
     * @param child  the node we are interested in
     * @return the index of the child in the parent, or -1 if either
     * <code>child</code> or <code>parent</code> are <code>null</code>
     * or don't belong to this tree model
     */
    @Override
    public int getIndexOfChild(Object parent, Object child) {
        if(parent instanceof LeafFeatureTreeNode){
            return -1;

        }else if(parent instanceof ParentFeatureTreeNode){
            ParentFeatureTreeNode parentFeatureTreeNode = (ParentFeatureTreeNode)parent;
            Feature parFeature = (Feature)parentFeatureTreeNode.getUserObject();

            if(child instanceof LeafFeatureTreeNode){
                LeafFeatureTreeNode childFeatureTreeNode = (LeafFeatureTreeNode)child;
                Feature childFeature = (Feature)childFeatureTreeNode.getUserObject();
                return parFeature.getChildren().indexOf(childFeature);

            }else if(child instanceof ParentFeatureTreeNode){
                ParentFeatureTreeNode childFeatureTreeNode = (ParentFeatureTreeNode)child;
                Feature childFeature = (Feature)childFeatureTreeNode.getUserObject();
                return parFeature.getChildren().indexOf(childFeature);

            }else{
                return -1;
            }

        }
        return -1;
    }

    /**
     * Adds a listener for the <code>TreeModelEvent</code>
     * posted after the tree changes.
     *
     * @param l the listener to add
     * @see #removeTreeModelListener
     */
    @Override
    public void addTreeModelListener(TreeModelListener l) {

    }

    /**
     * Removes a listener previously added with
     * <code>addTreeModelListener</code>.
     *
     * @param l the listener to remove
     * @see #addTreeModelListener
     */
    @Override
    public void removeTreeModelListener(TreeModelListener l) {

    }
}
