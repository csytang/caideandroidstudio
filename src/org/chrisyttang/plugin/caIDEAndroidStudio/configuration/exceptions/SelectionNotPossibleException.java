package org.chrisyttang.plugin.caIDEAndroidStudio.configuration.exceptions;

import org.chrisyttang.plugin.caIDEAndroidStudio.configuration.model.Selection;

public class SelectionNotPossibleException extends RuntimeException {

    private static final long serialVersionUID = 1793844229871267311L;

    public SelectionNotPossibleException(String feature, Selection selection) {
        super("The feature \"" + feature + "\" cannot be " + (selection == Selection.SELECTED ? "selected" : "deselected"));
    }

}