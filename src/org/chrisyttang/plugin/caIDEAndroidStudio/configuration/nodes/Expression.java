package org.chrisyttang.plugin.caIDEAndroidStudio.configuration.nodes;

import java.util.Collection;
import java.util.TreeSet;

public abstract class Expression extends Variable {

    private static final long serialVersionUID = 3993773534104729866L;

    protected final Variable[] children;

    public Expression(Variable[] children) {
        super(-1);
        this.children = children;
    }

    protected abstract int computeValue();

    @Override
    public final int getValue() {
        return (value == UNDEFINED) ? value = computeValue() : value;
    }

    @Override
    public int getManualValue() {
        return getValue();
    }

    @Override
    public int getAutomaticValue() {
        return getValue();
    }

    @Override
    public final void setAutomaticValue(int value) {}

    @Override
    public final void setManualValue(int value) {}

    @Override
    public final void reset() {
        value = UNDEFINED;
        for (int i = 0; i < children.length; i++) {
            children[i].reset();
        }
    }

    public int updateValue() {
        reset();
        return value = computeValue();
    }

    public Collection<Integer> getVariables() {
        final TreeSet<Integer> idSet = new TreeSet<>();
        getVariables(idSet);
        return idSet;
    }

    @Override
    protected void getVariables(TreeSet<Integer> list) {
        for (final Variable variable : children) {
            variable.getVariables(list);
        }
    }
}
