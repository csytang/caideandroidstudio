package org.chrisyttang.plugin.caIDEAndroidStudio.configuration.nodes;

import java.io.Serializable;
import java.util.TreeSet;

public class Variable implements Serializable {

    private static final long serialVersionUID = 2253729345725413110L;

    public static final int TRUE = 2, FALSE = 1, UNDEFINED = 0;

    protected final int id;

    protected int value;

    public Variable(int id) {
        this(id, UNDEFINED);
    }

    public Variable(int id, int value) {
        this.id = id;
        setManualValue(value);
    }

    public int getId() {
        return id;
    }

    public boolean hasValue() {
        return value > UNDEFINED;
    }

    public int getValue() {
        return getManualValue() | getAutomaticValue();
    }

    public int getManualValue() {
        return value & 3;
    }

    public int getAutomaticValue() {
        return value >> 2;
    }

    void setManualValue(int value) {
        this.value = (this.value & 0xfffffffc) | value;
        assert (getValue() <= TRUE) && (getValue() >= UNDEFINED) : "Invalid Variable Configuration";
    }

    void setAutomaticValue(int value) {
        this.value = (this.value & 0xfffffff3) | (value << 2);
        assert (getValue() <= TRUE) && (getValue() >= UNDEFINED) : "Invalid Variable Configuration";
    }

    protected void reset() {}

    protected void getVariables(TreeSet<Integer> list) {
        list.add(id);
    }

    @Override
    public String toString() {
        switch (getValue()) {
            case TRUE:
                return id + " : true";
            case FALSE:
                return id + " : false";
            case UNDEFINED:
                return id + " : undefined";
            default:
                return "! " + id + " : invalid value!";
        }
    }
}
