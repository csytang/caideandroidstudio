package org.chrisyttang.plugin.caIDEAndroidStudio.actions;

import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.CommonDataKeys;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.diagnostic.Logger;
import com.intellij.openapi.editor.Document;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.editor.SelectionModel;
import com.intellij.openapi.project.Project;
import com.intellij.psi.*;
import org.chrisyttang.plugin.caIDEAndroidStudio.featuredcode.FeatureAnnotationDialog;
import org.chrisyttang.plugin.caIDEAndroidStudio.featuredcode.FeatureAnnotationReporter;
import org.chrisyttang.plugin.caIDEAndroidStudio.features.Feature;

import java.util.LinkedList;
import java.util.List;

public class ToggletextColorAction extends AnAction {

    private Project _project;
    private static final Logger LOG = Logger.getInstance("org.chrisyttang.plugin.caIDEAndroidStudio.action.ToggletextColorAction");
    private Feature _selectedfeature = null;
    private PsiFile _psiFileInEditor;
    private FeatureAnnotationReporter annotationReporter;

    /**
     * Actions that allow developers to bind features with code segments
     * @param e the input action event
     */
    @Override
    public void actionPerformed(final AnActionEvent e) {
        //Get all the required data from data keys
        final Editor editor = e.getRequiredData(CommonDataKeys.EDITOR);
        _project = e.getRequiredData(CommonDataKeys.PROJECT);

        //Access document, caret, and selection
        final Document document = editor.getDocument();
        final SelectionModel selectionModel = editor.getSelectionModel();
        //New instance of Runnable to make an action
        FeatureAnnotationDialog dialog = new FeatureAnnotationDialog(_project);
        dialog.show();
        _selectedfeature = FeatureAnnotationDialog.getSeletedFeature();

        // user close the panel and no feature is selected
        if(_selectedfeature==null){
            return;
        }


        LOG.info("selected feature is:"+_selectedfeature.getName());
        _psiFileInEditor = PsiDocumentManager.getInstance(_project).getPsiFile(document);


        ApplicationManager.getApplication().invokeLater(new Runnable() {
            public void run() {
                ApplicationManager.getApplication().runWriteAction(new Runnable() {
                    public void run() {
                        annotationReporter = FeatureAnnotationReporter.getInstance(_project);

                        // PsiFile file,PsiElement element, Document document, Feature feature
                        // start annotation

                        int startOffset = editor.getSelectionModel().getSelectionStart();


                        int endOffset = editor.getSelectionModel().getSelectionEnd();


                        List<PsiElement> visisted = new LinkedList<PsiElement>();
                        _psiFileInEditor.accept(new PsiRecursiveElementWalkingVisitor() {
                            @Override
                            public void visitElement(PsiElement element) {

                                if(visisted.contains(element.getParent())){
                                    return;
                                }
                                if(element.getTextRange().getStartOffset()>=startOffset &&
                                        element.getTextRange().getEndOffset()<=endOffset){
                                    if(element instanceof PsiWhiteSpace){
                                        return;
                                    }

                                    if(element instanceof PsiComment){
                                        return;
                                    }

                                    if(element instanceof PsiEmptyStatement){
                                        return;
                                    }

                                    if(element instanceof PsiWhileStatement){
                                        return;
                                    }

                                    visisted.add(element);
                                    try {
                                        annotationReporter.publishNewFeatureAnnotation(_psiFileInEditor, element, document, _selectedfeature,true);
                                    }catch (Exception e){
                                        e.printStackTrace();
                                    }

                                    return;
                                }else
                                    super.visitElement(element);
                            }
                        });

                        // FINISH
                        selectionModel.removeSelection();
                    }
                });
            }
        });

    }


    @Override
    public void update(final AnActionEvent e) {
        //Get required data keys
        final Project project = e.getData(CommonDataKeys.PROJECT);
        final Editor editor = e.getData(CommonDataKeys.EDITOR);
        //Set visibility only in case of existing project and editor and if some text in the editor is selected
        e.getPresentation().setVisible((project != null && editor != null
                && editor.getSelectionModel().hasSelection()));
    }

    /**
     *
     * @param message print the message to log file
     */
    private static void logMessage(String message) {
        LOG.info(message);
    }
}
