package org.chrisyttang.plugin.caIDEAndroidStudio.actions;

import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.editor.Document;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiDocumentManager;
import com.intellij.psi.PsiFile;
import org.chrisyttang.plugin.caIDEAndroidStudio.featuredcode.FeatureAnnotationCache;
import org.chrisyttang.plugin.caIDEAndroidStudio.featuredcode.FeatureAnnotationFile;
import org.chrisyttang.plugin.caIDEAndroidStudio.util.PsiFileUtil;

import java.util.Set;

public class InitAllFeatureFiles extends AnAction {

    private Project _project;
    @Override
    public void actionPerformed(AnActionEvent e) {
        this._project = e.getProject();

        Set<VirtualFile> allFiles = PsiFileUtil.exploreAllFileInProject(this._project);

        FeatureAnnotationCache cache = FeatureAnnotationCache.getInstance(this._project);

        for(VirtualFile file:allFiles) {
            if (!file.isDirectory()) {
                if (file.getExtension() == null)
                    continue;
                if (file.getExtension().toLowerCase().equals("java")) {
                    String fileName  = file.getName();
                    String xmlfileName = fileName.substring(0,fileName.length()-".java".length())+"_color.xml";
                    VirtualFile colorFile = file.getParent().findChild(xmlfileName);
                    PsiFile psiFile = PsiFileUtil.VirtualFileToPsiFileConverter(file,this._project);
                    Document javaDocument = PsiDocumentManager.getInstance(this._project).getDocument(psiFile);
                    if(colorFile==null) {
                        FeatureAnnotationFile annotationFile = new FeatureAnnotationFile(PsiFileUtil.VirtualFileToPsiFileConverter(file,this._project));
                        cache.addSourceToAnnotationXML(file,annotationFile);
                    }
                }
            }
        }

        // TODO: refresh local file system
    }
}
