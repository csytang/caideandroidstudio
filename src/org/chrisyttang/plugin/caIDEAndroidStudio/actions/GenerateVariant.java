package org.chrisyttang.plugin.caIDEAndroidStudio.actions;

import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.DataKeys;
import com.intellij.openapi.diagnostic.Logger;
import com.intellij.openapi.progress.ProgressManager;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.DialogWrapper;
import org.chrisyttang.plugin.caIDEAndroidStudio.configuration.gui.ConfigurationForm;
import org.chrisyttang.plugin.caIDEAndroidStudio.featuremodel.FeatureModel;
import org.chrisyttang.plugin.caIDEAndroidStudio.features.Feature;
import org.chrisyttang.plugin.caIDEAndroidStudio.util.VfsFileUtil;
import org.chrisyttang.plugin.caIDEAndroidStudio.variant.CreatingVariantTask;

import java.util.Set;

public class GenerateVariant extends AnAction {

    private Project _project = null;
    private ConfigurationForm configurationForm;
    private FeatureModel _featureModel;
    private Set<Feature> _alldiabledFeature;
    public static final Logger LOG = Logger.getInstance("org.chrisyttang.plugin.caIDE.actions.GenerateVariant");
    private CreatingVariantTask creatingVariantTask;

    @Override
    public void actionPerformed(AnActionEvent e) {

        // initialize
        _project = DataKeys.PROJECT.getData(e.getDataContext());


        configurationForm = new ConfigurationForm(_project);
        configurationForm.show();

        if(configurationForm.getExitCode()== DialogWrapper.OK_EXIT_CODE) {
            // get all disabled features and enabled features
            _alldiabledFeature = configurationForm.getDisabledFeatures();


            // create variant with variant creation task class
            if (_alldiabledFeature.size() != 0) {
                creatingVariantTask = new CreatingVariantTask(_project, _alldiabledFeature);
                ProgressManager.getInstance().run(creatingVariantTask);
            }
        }

    }


    /**
     *
     * @return whether the feature model file exists in the project repository
     */
    public boolean isFeatureFileExists(){
        return VfsFileUtil.FileExist(this._project,"featuremodel.afm");
    }

    @Override
    public void update(AnActionEvent e) {
        this._project = DataKeys.PROJECT.getData(e.getDataContext());
        if(this._project!=null){
            boolean visible = isFeatureFileExists();
            e.getPresentation().setEnabledAndVisible(visible);
        }
    }


}
