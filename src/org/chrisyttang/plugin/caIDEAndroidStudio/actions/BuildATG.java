package org.chrisyttang.plugin.caIDEAndroidStudio.actions;

import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.DataKeys;
import com.intellij.openapi.module.Module;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.DialogWrapper;
import org.chrisyttang.plugin.caIDEAndroidStudio.parser.ActivityTransactionGraph;
import org.chrisyttang.plugin.caIDEAndroidStudio.parser.GatorsetupDialog;

public class BuildATG extends AnAction {

    private Project _project = null;
    private Module _selectModule;
    private ActivityTransactionGraph _activityTransactionGraph;

    private GatorsetupDialog _gatorSetupDialog;


    @Override
    public void actionPerformed(AnActionEvent e) {

        // initialize
        _project = DataKeys.PROJECT.getData(e.getDataContext());
        _selectModule = DataKeys.MODULE.getData(e.getDataContext());

        // start a static analysis for the target Android project
        _gatorSetupDialog = new GatorsetupDialog(_project);
        _gatorSetupDialog.show();


        if(_gatorSetupDialog.getExitCode()== DialogWrapper.OK_EXIT_CODE){
            String androidLocation = _gatorSetupDialog.getAndroidSDKLocation();
            String gatorLocation = _gatorSetupDialog.getGatorLocation();
            _activityTransactionGraph = new ActivityTransactionGraph(androidLocation,
                    gatorLocation,_project,_selectModule,e);
            try {
                _activityTransactionGraph.parse();
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            }


        }else{
            return;
        }


    }
}
