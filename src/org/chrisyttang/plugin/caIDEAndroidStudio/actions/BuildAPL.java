package org.chrisyttang.plugin.caIDEAndroidStudio.actions;

import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.DataKeys;
import com.intellij.openapi.module.Module;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.DialogWrapper;
import com.intellij.openapi.ui.MessageType;
import com.intellij.openapi.ui.popup.Balloon;
import com.intellij.openapi.ui.popup.JBPopupFactory;
import com.intellij.openapi.wm.StatusBar;
import com.intellij.openapi.wm.WindowManager;
import com.intellij.ui.awt.RelativePoint;
import org.chrisyttang.plugin.caIDEAndroidStudio.recommendation.*;
import org.chrisyttang.plugin.caIDEAndroidStudio.recommendation.gui.RecommendationConfigDialog;

public class BuildAPL extends AnAction {


    private Project _project = null;
    private Module _selectModule;
    private RecommendationConfigDialog dialog;
    private APLRecommendationApps _approchtype;
    private IRecommendation recommendation;
    private String targetVariantName = "";


    @Override
    public void actionPerformed(AnActionEvent e) {

        // initialize
        _project = DataKeys.PROJECT.getData(e.getDataContext());
        _selectModule = DataKeys.MODULE.getData(e.getDataContext());

        // init the dialog to let users select
        dialog = new RecommendationConfigDialog(_project);
        dialog.show();

        if(dialog.getExitCode()== DialogWrapper.OK_EXIT_CODE) {
            _approchtype = dialog.getSelectedRecommendationApproach();


            switch (_approchtype) {
                case ACTIVITY_TRANS: {
                    recommendation =
                            new APLActivityTransRecommendation(_project, _selectModule, e);
                    targetVariantName = _project.getName()+"_activity_trans_variant";
                    break;
                }
                case FEATURE_MODEL: {
                    recommendation = new APLFeatureRecommendation(_project,_selectModule);
                    targetVariantName = _project.getName()+"_featuremodel_variant";
                    break;
                }
                case UI_DEP: {
                    recommendation = new APLUIDepRecommendation(_project,_selectModule);
                    targetVariantName = _project.getName()+"_ui_dep_variant";
                    break;
                }
                case PERMISSION_DEP: {
                    recommendation = new APLPermissionDepRecommendation(_project, _selectModule);
                    targetVariantName = _project.getName()+"_permission_dep_variant";
                    break;
                }
            }


            DialogWrapper recommendation_dialog = recommendation.generateRecommendations();
            recommendation_dialog.show();

            if (recommendation_dialog.getExitCode() == DialogWrapper.OK_EXIT_CODE) {
                recommendation.buildVariant();

                // send back a notification
                StatusBar statusBar = WindowManager.getInstance()
                        .getStatusBar(DataKeys.PROJECT.getData(e.getDataContext()));

                JBPopupFactory.getInstance()
                        .createHtmlTextBalloonBuilder("The variant is built, project "+targetVariantName+" is the APL created", MessageType.INFO, null)
                        .setFadeoutTime(7500)
                        .createBalloon()
                        .show(RelativePoint.getCenterOf(statusBar.getComponent()),
                                Balloon.Position.atRight);

            }
        }


    }
}
