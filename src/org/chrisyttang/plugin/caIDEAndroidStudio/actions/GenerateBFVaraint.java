package org.chrisyttang.plugin.caIDEAndroidStudio.actions;

import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.DataKeys;
import com.intellij.openapi.diagnostic.Logger;
import com.intellij.openapi.module.Module;
import com.intellij.openapi.progress.PerformInBackgroundOption;
import com.intellij.openapi.progress.ProgressManager;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.DialogWrapper;
import com.intellij.psi.search.FilenameIndex;
import com.intellij.psi.search.GlobalSearchScope;
import org.chrisyttang.androidmanifest.dom.ActivityNode;
import org.chrisyttang.plugin.caIDEAndroidStudio.featuremodel.FeatureModel;
import org.chrisyttang.plugin.caIDEAndroidStudio.features.Feature;
import org.chrisyttang.plugin.caIDEAndroidStudio.instantapp.configuration.InstantAppConfiguration;
import org.chrisyttang.plugin.caIDEAndroidStudio.instantapp.configuration.InstantAppConfigurationForm;
import org.chrisyttang.plugin.caIDEAndroidStudio.parser.AndroidManifestParserWrapper;
import org.chrisyttang.plugin.caIDEAndroidStudio.parser.ActivityTransactionGraph;
import org.chrisyttang.plugin.caIDEAndroidStudio.parser.GatorsetupDialog;
import org.chrisyttang.plugin.caIDEAndroidStudio.variant.CreateBFVariantTask;

import java.util.LinkedList;
import java.util.List;

public class GenerateBFVaraint extends AnAction {

    private Project _project = null;
    private InstantAppConfigurationForm _instantAppConfigurationForm;
    private GatorsetupDialog _gatorSetupDialog;
    private InstantAppConfiguration _instantAppConfiguration;
    private AndroidManifestParserWrapper _androidManifestParserWrapper;
    private List<ActivityNode> activityNodes;
    public static final Logger LOG = Logger.getInstance("org.chrisyttang.plugin.caIDEAndroidStudio.actions.GenerateBFVaraint");
    private CreateBFVariantTask createBFVariantTask;
    private Module _selectModule;
    private ActivityTransactionGraph _activityTransactionGraph;

    @Override
    public void actionPerformed(AnActionEvent e) {
        // if the the system is not open in Android view, close


        // initialize
        _project = DataKeys.PROJECT.getData(e.getDataContext());
        _selectModule = DataKeys.MODULE.getData(e.getDataContext());

        // AndroidManifest.xml parser
        _androidManifestParserWrapper = AndroidManifestParserWrapper.getInstant(_project,_selectModule);
        activityNodes = _androidManifestParserWrapper.getAllActivities();
        FeatureModel featureModel = FeatureModel.getInstance(_project);
        LinkedList<String> featureNames = new LinkedList<>();

        for(Feature feature:featureModel.getFeatures()){
            featureNames.add(feature.getName());
        }

        if(FilenameIndex.getFilesByName(_project,_selectModule.getName()+".atg", GlobalSearchScope.allScope(_project))==null){
            BuildATG buildATGaction = new BuildATG();
            buildATGaction.actionPerformed(e);
        }





        _instantAppConfigurationForm = new InstantAppConfigurationForm(_project, _androidManifestParserWrapper.getPackagename(), activityNodes, featureNames);
        _instantAppConfigurationForm.show();

        // the dialog is closed
        if (_instantAppConfigurationForm.getExitCode() == DialogWrapper.OK_EXIT_CODE) {
            // obtain the configuration data collected
            _instantAppConfiguration = _instantAppConfigurationForm.getInstantAppConfiguration();

            //
            createBFVariantTask = new CreateBFVariantTask(_project, "Create APL with Instant App template",
                        true, PerformInBackgroundOption.DEAF,
                        _instantAppConfiguration, activityNodes, _selectModule);
            // run the task for building the instant app
            ProgressManager.getInstance().run(createBFVariantTask);

        }






    }
}
