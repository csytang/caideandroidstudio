package org.chrisyttang.plugin.caIDEAndroidStudio.actions;

import com.intellij.lang.ASTNode;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.DataContext;
import com.intellij.openapi.actionSystem.DataKeys;
import com.intellij.openapi.application.AccessToken;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.diagnostic.Logger;
import com.intellij.openapi.editor.Document;
import com.intellij.openapi.module.Module;
import com.intellij.openapi.progress.ProcessCanceledException;
import com.intellij.openapi.progress.ProgressIndicator;
import com.intellij.openapi.progress.ProgressManager;
import com.intellij.openapi.progress.Task;
import com.intellij.openapi.progress.util.ProgressIndicatorUtils;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.MessageType;
import com.intellij.openapi.ui.popup.Balloon;
import com.intellij.openapi.ui.popup.JBPopupFactory;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.openapi.wm.StatusBar;
import com.intellij.openapi.wm.WindowManager;
import com.intellij.psi.*;
import com.intellij.psi.javadoc.*;
import com.intellij.psi.xml.XmlElement;
import com.intellij.psi.xml.XmlFile;
import com.intellij.psi.xml.XmlTag;
import com.intellij.ui.awt.RelativePoint;
import org.chrisyttang.plugin.caIDEAndroidStudio.featuredcode.FeatureAnnotationCache;
import org.chrisyttang.plugin.caIDEAndroidStudio.featuredcode.FeatureAnnotationFile;
import org.chrisyttang.plugin.caIDEAndroidStudio.featuredcode.FeatureAnnotationReporter;
import org.chrisyttang.plugin.caIDEAndroidStudio.featuredcode.FeaturedCodeDescriptor;
import org.chrisyttang.plugin.caIDEAndroidStudio.featuremodel.FeatureModel;
import org.chrisyttang.plugin.caIDEAndroidStudio.features.Feature;
import org.chrisyttang.plugin.caIDEAndroidStudio.util.PsiFileUtil;
import org.chrisyttang.plugin.caIDEAndroidStudio.util.PsiFormater;
import org.jetbrains.annotations.NotNull;
import com.intellij.openapi.progress.util.ReadTask;
import org.jetbrains.annotations.Nullable;

import java.util.HashSet;
import java.util.Set;

public class CollectAllFeaturesInModule extends AnAction {

    private Project _project;
    private Module _selectedmodule;
    private Logger LOG = Logger.getInstance("org.chrisyttang.plugin.caIDEAndroidStudio.actions.CollectAllFeaturesInModule");
    private FeatureAnnotationReporter annotationReporter;

    @Override
    public void actionPerformed(AnActionEvent e) {
        DataContext dataContext = e.getDataContext();
        _project = DataKeys.PROJECT.getData(dataContext);
        _selectedmodule = DataKeys.MODULE.getData(dataContext);

        Set<VirtualFile> allFiles = PsiFileUtil.exploreAllFileInModule(_selectedmodule);

        FeatureAnnotationCache cache = FeatureAnnotationCache.getInstance(_project);
        annotationReporter = FeatureAnnotationReporter.getInstance(_project);

        ApplicationManager.getApplication().executeOnPooledThread(new Runnable() {
            public void run() {
                ApplicationManager.getApplication().runReadAction(new Runnable() {
                    public void run() {
                        for (VirtualFile file : allFiles) {
                            if (!file.isDirectory()) {
                                if (file.getExtension() == null)
                                    continue;
                                if (file.getExtension().toLowerCase().equals("java")) {
                                    String fileName = file.getName();
                                    String xmlfileName = fileName.substring(0, fileName.length() - ".java".length()) + "_color.xml";
                                    VirtualFile colorFile = file.getParent().findChild(xmlfileName);
                                    PsiFile psiFile = PsiFileUtil.VirtualFileToPsiFileConverter(file, _project);
                                    Document javaDocument = PsiDocumentManager.getInstance(_project).getDocument(psiFile);
                                    LOG.info("File:"+file.getName());

                                    if (colorFile != null) {
                                        FeatureAnnotationFile annotationFile = new FeatureAnnotationFile(psiFile);

                                        // add the mapping between source code file and annotation xml
                                        cache.addSourceToAnnotationXML(file, annotationFile);

                                        // now let's extract all annotation from the xml file
                                        PsiFile psiColorFile = PsiFileUtil.VirtualFileToPsiFileConverter(colorFile, _project);

                                        Set<FeaturedCodeDescriptor> annotationset = new HashSet<FeaturedCodeDescriptor>();

                                        assert psiColorFile instanceof XmlFile;

                                        XmlFile colorFileXml = (XmlFile) psiColorFile;

                                        // get all element in the tag
                                        XmlTag rootTag = colorFileXml.getRootTag();
                                        XmlTag[] itemTags = rootTag.getSubTags();

                                        for (XmlTag itemTag : itemTags) {// item tag
                                            itemTag.accept(new XmlElementVisitor() {

                                                @Override
                                                public void visitXmlElement(XmlElement element) {

                                                    // annotation tag
                                                    XmlTag annotationTag = itemTag.getSubTags()[0];
                                                    String itemName = itemTag.getAttributeValue("name");
                                                    String className = itemName.substring(0, itemName.indexOf("[")).split(" ")[0];

                                                    XmlTag elementTag = annotationTag.getSubTags()[0];


                                                    String featureName = annotationTag.getAttributeValue("name");
                                                    String ASTID = elementTag.getAttributeValue("val");
                                                    FeatureModel featureModel = FeatureModel.getInstance(_project);
                                                    Feature feature = featureModel.getFeature(featureName);
                                                    assert feature != null;

                                                    ((PsiJavaFile)psiFile).getNode().getPsi().accept(new JavaRecursiveElementWalkingVisitor() {

                                                        @Override
                                                        public void visitElement(PsiElement element) {
                                                            String externalName = PsiFormater.getExternalName(element, true).trim();
                                                            if (ASTID.trim().equals(externalName)||
                                                                    ASTID.trim().endsWith("/"+externalName)) {
                                                                FeaturedCodeDescriptor descriptor = new FeaturedCodeDescriptor(element, javaDocument, feature);
                                                                annotationset.add(descriptor);
                                                                LOG.info("Publish a new element:"+ASTID);
                                                                annotationReporter.publishNewFeatureAnnotation(psiFile, element, javaDocument, feature,false);
                                                            }
                                                            super.visitElement(element);
                                                        }


                                                    });

                                                }

                                            });
                                        }



                                    }else{
                                        LOG.error("Cannot find the color file");
                                    }
                                }
                            }
                        }

                        // send back a notification
                        StatusBar statusBar = WindowManager.getInstance()
                                .getStatusBar(DataKeys.PROJECT.getData(e.getDataContext()));

                        JBPopupFactory.getInstance()
                                .createHtmlTextBalloonBuilder("Collection has been completed", MessageType.INFO, null)
                                .setFadeoutTime(7500)
                                .createBalloon()
                                .show(RelativePoint.getCenterOf(statusBar.getComponent()),
                                        Balloon.Position.atRight);

                    }
                });
            }
        });



    }

}
