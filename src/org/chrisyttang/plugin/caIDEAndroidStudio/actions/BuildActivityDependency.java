package org.chrisyttang.plugin.caIDEAndroidStudio.actions;

import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.DataConstants;
import com.intellij.openapi.actionSystem.DataKeys;
import com.intellij.openapi.module.Module;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import org.chrisyttang.plugin.caIDEAndroidStudio.parser.ActivityDependency;
import org.chrisyttang.plugin.caIDEAndroidStudio.util.DialogUtil;

public class BuildActivityDependency extends AnAction {

    private VirtualFile _selectedFile;
    private ActivityDependency _activityDependency;
    private Project _project;
    private Module _selectModule;

    @Override
    public void actionPerformed(AnActionEvent e) {
        _project = e.getProject();
        _selectedFile = (VirtualFile) e.getDataContext().getData(DataConstants.VIRTUAL_FILE);
        _selectModule = DataKeys.MODULE.getData(e.getDataContext());

        if(_selectedFile==null){
            DialogUtil.createDialog("The selected file is not a java file!");
            return;
        }else if(_selectedFile.getExtension()==null){
            DialogUtil.createDialog("The selected file is not a java file!");
            return;
        }else if(!_selectedFile.getExtension().endsWith("java")){
            DialogUtil.createDialog("The selected file is not a java file!");
            return;
        }

        _activityDependency = new ActivityDependency(_selectedFile,_selectModule);

    }
}
