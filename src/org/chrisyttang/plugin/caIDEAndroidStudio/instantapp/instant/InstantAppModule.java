package org.chrisyttang.plugin.caIDEAndroidStudio.instantapp.instant;

import com.android.tools.idea.gradle.parser.GradleBuildFile;
import com.intellij.openapi.command.WriteCommandAction;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import org.chrisyttang.androidmanifest.dom.ActivityNode;
import org.chrisyttang.androidmanifest.dom.ManifestNode;
import org.chrisyttang.plugin.caIDEAndroidStudio.instantapp.Module;
import org.chrisyttang.plugin.caIDEAndroidStudio.instantapp.gradle.FeatureModuleBuildGradle;
import org.chrisyttang.plugin.caIDEAndroidStudio.util.FileUtil;

import java.io.File;
import java.util.Collection;
import java.util.List;

public class InstantAppModule implements Module {

    public String _packageName;
    public String _moduleName;
    public String _instantappProjectPath;
    public Project _project;
    private File _targetBuildgradleFile;

    public InstantAppModule(Project pproject, String pinstantappProjectPath,String ppackageName){
        _project = pproject;
        _instantappProjectPath = pinstantappProjectPath;
        _moduleName = "instant";
        _packageName = ppackageName;
    }
    /**
     * @return build.gradle file from the module
     */
    @Override
    public File getBuildGradle() {
        return _targetBuildgradleFile;
    }

    /**
     * @return null no AndroidManifest file for instant app module
     */
    @Override
    public File getAndroidManifest() {
        return null;
    }

    /**
     * this is function must be called at runtime for init the module
     *
     * @param _buildGradleSource
     * @param _androidManifestSource
     * @param _assetSource
     * @param _resSource
     * @param _featuremodules
     */
    @Override
    public void initModule(GradleBuildFile _buildGradleSource, ManifestNode _androidManifestSource, Collection<File> _assetSource, Collection<File> _resSource, List<String> _featuremodules) {
        initModuleDirectory();
        initBuildGradleFile(_buildGradleSource,_featuremodules);
        initAndroidManifestXMLFile(_androidManifestSource);
    }

    public boolean initBuildGradleFile(GradleBuildFile buildGradleSource, List<String> featuremodules) {
        String targetBuildGradlePath = _instantappProjectPath+ File.separator +_moduleName + File.separator+"build.gradle";
        FeatureModuleBuildGradle featureModuleBuildGradle = new FeatureModuleBuildGradle(buildGradleSource,this,_moduleName,featuremodules);

        _targetBuildgradleFile = new File(targetBuildGradlePath);
        FileUtil.writeFile(_targetBuildgradleFile,featureModuleBuildGradle.getContent());
        return true;
    }

    /**
     * @param _assetSource
     * @param _resSource
     * @return
     * No need resource clone
     */
    @Override
    public boolean cloneResources(Collection<File> _assetSource, Collection<File> _resSource) {
        // do nothing
        return true;
    }

    /**
     * @param srcRoots
     * @return
     */
    @Override
    public boolean customizeCloneSourceCode(List<VirtualFile>srcRoots,List<VirtualFile> moduleActivities,
                                            List<VirtualFile>featureModuleActivities) {
        return false;
    }

    /**
     * @return the activity binds to this module
     */
    @Override
    public List<ActivityNode> getActivities() {
        return null;
    }

    /**
     * @param _androidManifestSource
     * @return
     */
    @Override
    public boolean initAndroidManifestXMLFile(ManifestNode _androidManifestSource) {
        // do nothing, no AndroidManifest.xml for instant app module
        return true;
    }

    /**
     * @return the name of this module
     */
    @Override
    public String getModuleName() {
        return "instant";
    }


    /**
     * @return the package name of this module
     */
    @Override
    public String getPackageName() {
        return _packageName;
    }

    /**
     * @return whether this is a module for installed app
     */
    @Override
    public boolean isInstalledModule() {
        return false;
    }

    /**
     * @return init the module directory
     */
    @Override
    public boolean initModuleDirectory() {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                File _targetModuleDir = new File(_instantappProjectPath+File.separator+_moduleName);
                _targetModuleDir.mkdir();
            }
        };
        WriteCommandAction.runWriteCommandAction(_project, runnable);
        return true;
    }
}
