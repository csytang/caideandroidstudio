package org.chrisyttang.plugin.caIDEAndroidStudio.instantapp.instant;

import com.android.tools.idea.gradle.parser.GradleBuildFile;
import com.intellij.openapi.command.WriteCommandAction;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import org.chrisyttang.androidmanifest.dom.ActivityNode;
import org.chrisyttang.androidmanifest.dom.ManifestNode;
import org.chrisyttang.plugin.caIDEAndroidStudio.instantapp.Module;
import org.chrisyttang.plugin.caIDEAndroidStudio.util.PsiFileUtil;

import java.io.File;
import java.net.URL;
import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 *  - module
 *    | - <build>
 *    | - build.gradle
 *    | - <src>
 *        | - <main>
 *            | - AndroidManifest.xml
 *            | - <java>
 *            | - <res>
 */
public abstract class InstantFeatureModule implements Module {

    public List<ActivityNode> _activityNodes;
    public String _packageName;
    public String _moduleName;
    public String _instantappProjectPath;
    public Project _project;
    public InstantFeatureModule(Project pproject, String pinstantappProjectPath, List<ActivityNode> pactivityNodes, String ppackageName, String pmoduleName){
        _project = pproject;
        _instantappProjectPath = pinstantappProjectPath;
        _activityNodes = pactivityNodes;
        _packageName = ppackageName;
        _moduleName = pmoduleName;
    }


    /**
     *
     * @return build.gradle file from the module
     */
    public abstract File getBuildGradle();

    /**
     *
     * @return whether this module is base module
     */
    public abstract boolean isBaseModule();

    /**
     *
     * @return <br>implementation</br> dependencies of this module
     */
    public abstract Set<InstantFeatureModule> getImplementationDependencies();

    /**
     *
     * @return get default URL of this module
     */
    public abstract URL getdefultURL();

    /**
     *
     * @return the AndroidManifest.xml of this module
     */
    public abstract File getAndroidManifest();

    /**
     * this is function must be called at runtime for init the module
     */
    public void initModule(GradleBuildFile _buildGradleSource,ManifestNode _androidManifestSource, Collection<File> _assetSource,
                           Collection<File> _resSource, List<String> _featuremodules){
        initModuleDirectory();
        initBuildGradleFile(_buildGradleSource,_featuremodules);
        initAndroidManifestXMLFile(_androidManifestSource);
        cloneResources(_assetSource,_resSource);
    };

    /**
     *
     * @return init the module directory
     */
    public boolean initModuleDirectory(){
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                File _targetModuleDir = new File(_instantappProjectPath+File.separator+_moduleName);
                _targetModuleDir.mkdir();
            }
        };
        WriteCommandAction.runWriteCommandAction(_project, runnable);
        return true;
    }

    /**
     *
     * @return whether successfully create the build.gradle file
     */
    public abstract boolean initBuildGradleFile(GradleBuildFile _buildGradleSource, List<String> _featuremodules);

    /**
     *
     * @return whether successfully create the AndroidManifest.xml file
     */
    public abstract boolean initAndroidManifestXMLFile(ManifestNode _androidManifestSource);


    /**
     * @return clone resources
     */
    public boolean cloneResources(Collection<File> _assetSource,
                                  Collection<File> _resSource){

        // target Res directory path
        for(File file:_resSource){
            PsiFileUtil.cloneFile(file,_instantappProjectPath,_project);
        }

        for(File file:_resSource){
            PsiFileUtil.cloneFile(file,_instantappProjectPath,_project);
        }

        return true;
    }

    /**
     *
     * @return the package name of this module
     */
    public String getPackageName(){
        return _packageName;
    }


    /**
     * @return whether this is a module for installed app
     */
    @Override
    public boolean isInstalledModule() {
        return false;
    }


    /**
     * @return the activity binds to this module
     */
    @Override
    public List<ActivityNode> getActivities() {
        return _activityNodes;
    }


}
