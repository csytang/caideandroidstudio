package org.chrisyttang.plugin.caIDEAndroidStudio.instantapp.instant;

import com.android.tools.idea.gradle.parser.GradleBuildFile;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import org.chrisyttang.androidmanifest.dom.ActivityNode;
import org.chrisyttang.androidmanifest.dom.ManifestNode;
import org.chrisyttang.plugin.caIDEAndroidStudio.instantapp.androidmanifest.FeatureModuleAndroidManifest;
import org.chrisyttang.plugin.caIDEAndroidStudio.instantapp.gradle.FeatureModuleBuildGradle;
import org.chrisyttang.plugin.caIDEAndroidStudio.util.FileUtil;
import org.chrisyttang.plugin.caIDEAndroidStudio.util.PsiFileUtil;

import java.io.File;
import java.net.URL;
import java.util.List;
import java.util.Set;

public class FeatureModule extends InstantFeatureModule {

    private File _targetBuildgradleFile;
    private File _targetAndroidManifestFile;

    public FeatureModule(Project pproject, String pinstantappProjectPath, List<ActivityNode> pactivityNodes, String ppackageName, String pmoduleName) {
        super(pproject,pinstantappProjectPath,pactivityNodes, ppackageName, pmoduleName);
    }

    /**
     * @return build.gradle file from the module
     */
    @Override
    public File getBuildGradle() {
        return _targetBuildgradleFile;
    }

    /**
     * @return whether this module is base module
     */
    @Override
    public boolean isBaseModule() {
        return false;
    }

    /**
     * @return <br>implementation</br> dependencies of this module
     */
    @Override
    public Set<InstantFeatureModule> getImplementationDependencies() {
        return null;
    }

    /**
     * @return get default URL of this module
     */
    @Override
    public URL getdefultURL() {
        return null;
    }


    /**
     * @return the AndroidManifest.xml of this module
     */
    @Override
    public File getAndroidManifest() {
        return _targetAndroidManifestFile;
    }

    /**
     * @param _buildGradleSource
     * @param _featuremodules
     * @return whether successfully create the build.gradle file
     */
    @Override
    public boolean initBuildGradleFile(GradleBuildFile _buildGradleSource, List<String> _featuremodules) {
        String targetBuildGradlePath = _instantappProjectPath+ File.separator +super._moduleName + File.separator+"build.gradle";
        FeatureModuleBuildGradle featureModuleBuildGradle = new FeatureModuleBuildGradle(_buildGradleSource,this,super._moduleName,_featuremodules);

        _targetBuildgradleFile = new File(targetBuildGradlePath);
        FileUtil.writeFile(_targetBuildgradleFile,featureModuleBuildGradle.getContent());
        return true;
    }

    /**
     * @param srcRoots
     * @return
     */
    @Override
    public boolean customizeCloneSourceCode(List<VirtualFile>srcRoots,List<VirtualFile> moduleActivities,
                                            List<VirtualFile>featureModuleActivities) {
        for(VirtualFile file:srcRoots){
            cloneSourceCodefile(file,moduleActivities);
        }
        return true;

    }



    private void cloneSourceCodefile(VirtualFile file,List<VirtualFile> moduleActivities){
        if(file.isDirectory()){
            for(VirtualFile child:file.getChildren()){
                cloneSourceCodefile(child,moduleActivities);
            }
        }else{
            if(moduleActivities.contains(file)){
                // if the activity file is not feature module activity, then clone
                PsiFileUtil.cloneFile(file,_instantappProjectPath+ File.separator +super._moduleName,_project);
            }
        }
    }

    /**
     * @param _androidManifestSource
     * @return whether successfully create the AndroidManifest.xml file
     */
    @Override
    public boolean initAndroidManifestXMLFile(ManifestNode _androidManifestSource) {
        FeatureModuleAndroidManifest featureModuleAndroidManifest = new FeatureModuleAndroidManifest(this,
                _androidManifestSource,_activityNodes,_packageName);

        // the content of AndroidManifest is created inside @code BaseModuleAndroidManifest class
        _targetAndroidManifestFile = new File(_instantappProjectPath+File.separator+_moduleName
                +File.separator+"main"+File.separator+"AndroidManifext.xml");


        FileUtil.writeFile(_targetAndroidManifestFile,featureModuleAndroidManifest.getContent());
        return true;
    }

    /**
     * @return the name of this module
     */
    @Override
    public String getModuleName() {
        return _moduleName;
    }

}
