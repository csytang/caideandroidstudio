package org.chrisyttang.plugin.caIDEAndroidStudio.instantapp.installed;

import com.android.tools.idea.gradle.parser.GradleBuildFile;
import com.intellij.openapi.command.WriteCommandAction;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import org.chrisyttang.androidmanifest.dom.ActivityNode;
import org.chrisyttang.androidmanifest.dom.ManifestNode;
import org.chrisyttang.plugin.caIDEAndroidStudio.instantapp.Module;
import org.chrisyttang.plugin.caIDEAndroidStudio.instantapp.androidmanifest.InstalledAndroidManifest;
import org.chrisyttang.plugin.caIDEAndroidStudio.instantapp.gradle.InstalledModuleBuildGradle;
import org.chrisyttang.plugin.caIDEAndroidStudio.util.FileUtil;

import java.io.File;
import java.util.Collection;
import java.util.List;

public class InstalledAppModule implements Module{
    private File _buildGradle;
    private File _androidManifest;
    private String _packageName;
    public String _moduleName;
    public String _instantappProjectPath;
    public Project _project;
    private InstalledAndroidManifest _installedAndroidManifest;
    public InstalledAppModule(Project pproject, String pinstantappProjectPath,String ppackageName){
        _project = pproject;
        _instantappProjectPath = pinstantappProjectPath;
        _packageName = ppackageName;
        _moduleName = "installed";
        _installedAndroidManifest = new InstalledAndroidManifest(this);
    }


    /**
     * @return build.gradle file from the module
     */
    @Override
    public File getBuildGradle() {
        return this._buildGradle;
    }

    /**
     * @return the AndroidManifest.xml of this module
     */
    @Override
    public File getAndroidManifest() {
        return this._androidManifest;
    }

    /**
     * this is function must be called at runtime for init the module
     *
     * @param _buildGradleSource
     * @param _androidManifestSource
     * @param _assetSource
     * @param _resSource
     * @param _featuremodules
     */
    @Override
    public void initModule(GradleBuildFile _buildGradleSource, ManifestNode _androidManifestSource, Collection<File> _assetSource, Collection<File> _resSource, List<String> _featuremodules) {
        initModuleDirectory();
        initBuildGradleFile(_buildGradleSource,_featuremodules);
        initAndroidManifestXMLFile(_androidManifestSource);
    }




    /**
     * @return the package name of this module
     */
    @Override
    public String getPackageName() {
        return _packageName;
    }

    /**
     * @return whether this is a module for installed app
     */
    @Override
    public boolean isInstalledModule() {
        return true;
    }

    /**
     * @return init the module directory
     */
    @Override
    public boolean initModuleDirectory() {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                File _targetModuleDir = new File(_instantappProjectPath+File.separator+_moduleName);
                _targetModuleDir.mkdir();
            }
        };
        WriteCommandAction.runWriteCommandAction(_project, runnable);
        return true;
    }

    /**
     * @param buildGradleSource
     * @param featuremodules
     * @return
     */
    @Override
    public boolean initBuildGradleFile(GradleBuildFile buildGradleSource, List<String> featuremodules) {
        String targetBuildGradlePath = _instantappProjectPath+ File.separator +"installed" + File.separator+"build.gradle";
        InstalledModuleBuildGradle installedModuleBuildGradle = new InstalledModuleBuildGradle(buildGradleSource,this,featuremodules);

        _buildGradle = new File(targetBuildGradlePath);
        FileUtil.writeFile(_buildGradle,installedModuleBuildGradle.getContent());
        return true;
    }

    /**
     * @param _assetSource
     * @param _resSource
     * @return
     */
    @Override
    public boolean cloneResources(Collection<File> _assetSource, Collection<File> _resSource) {
        // do nothing
        return true;
    }

    /**
     * @param srcRoots
     * @return
     */
    @Override
    public boolean customizeCloneSourceCode(List<VirtualFile>srcRoots,List<VirtualFile> baseModuleActivities,
                                            List<VirtualFile>featureModuleActivities) {
        // do nothing
        return true;
    }

    /**
     * @return the activity binds to this module
     */
    @Override
    public List<ActivityNode> getActivities() {
        return null;
    }

    /**
     * @param _androidManifestSource
     * @return
     */
    @Override
    public boolean initAndroidManifestXMLFile(ManifestNode _androidManifestSource) {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                File _targetModuleDir = new File(_instantappProjectPath+File.separator+
                        _moduleName+File.separator+"src");
                _targetModuleDir.mkdir();
                _targetModuleDir = new File(_instantappProjectPath+File.separator+
                        _moduleName+File.separator+"src"+File.separator+"main");
                _targetModuleDir.mkdir();

                _androidManifest = new File(_instantappProjectPath+File.separator+
                        _moduleName+File.separator+"src"+File.separator+"main"+File.separator+"AndroidManifest.xml");
                FileUtil.writeFile(_androidManifest,_installedAndroidManifest.getContent());
            }
        };
        WriteCommandAction.runWriteCommandAction(_project, runnable);

        return false;
    }

    /**
     * @return the name of this module
     */
    @Override
    public String getModuleName() {
        return "installed";
    }


}
