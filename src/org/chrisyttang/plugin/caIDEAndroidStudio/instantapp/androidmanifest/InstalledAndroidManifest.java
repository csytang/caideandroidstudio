package org.chrisyttang.plugin.caIDEAndroidStudio.instantapp.androidmanifest;

import org.chrisyttang.plugin.caIDEAndroidStudio.instantapp.Module;
import org.chrisyttang.plugin.caIDEAndroidStudio.instantapp.installed.InstalledAppModule;

import java.util.List;
import java.util.Map;

public class InstalledAndroidManifest extends AndroidManifestTemplate {

    private InstalledAppModule _installedAppModule;

    public InstalledAndroidManifest(InstalledAppModule installedAppModule){
        _installedAppModule = (InstalledAppModule) installedAppModule;
    }
    /**
     * return the module of this AndroidManifest.xml belongs to;
     */
    @Override
    public Module getModule() {
        return _installedAppModule;
    }


    /**
     * apply all changes
     */
    @Override
    public void applyChanges() {
        // do nothing
        return;
    }

    /**
     * @return content of this AndroidManifest.xml file
     */
    @Override
    public String getContent() {
        return "<manifest xmlns:android=\"http://schemas.android.com/apk/res/android\" package=\""+getModule().getPackageName()
                +"\">\n </manifest>";
    }
}
