package org.chrisyttang.plugin.caIDEAndroidStudio.instantapp.androidmanifest;

import com.intellij.openapi.diagnostic.Logger;
import org.chrisyttang.androidmanifest.dom.ActivityNode;
import org.chrisyttang.androidmanifest.dom.ApplicationNode;
import org.chrisyttang.androidmanifest.dom.ManifestNode;
import org.chrisyttang.plugin.caIDEAndroidStudio.instantapp.Module;

import java.util.*;

public class BaseModuleAndroidManifest implements AndroidManifest{

    private ManifestNode _orimanifestNode;
    private Module _module;
    private String _content;
    private List<ActivityNode> _activityNodes;
    private String _packageName;

    private Logger LOG = Logger.getInstance("org.chrisyttang.plugin.caIDEAndroidStudio.instantapp.androidmanifest.FeatureModuleAndroidManifest");

    public BaseModuleAndroidManifest(Module pmodule, ManifestNode porimanifestNode, List<ActivityNode> pactivityNodes, String ppackageName){
        this._module = pmodule;
        this._orimanifestNode = porimanifestNode;
        this._activityNodes = pactivityNodes;
        this._packageName = ppackageName;
        applyChanges();
    }
    /**
     * return the module of this AndroidManifest.xml belongs to;
     */
    @Override
    public Module getModule() {
        return _module;
    }



    /**
     * apply all changes
     */
    @Override
    public void applyChanges() {
        // change the package name to the target packagename
        _orimanifestNode.setPackage(this._packageName);
        ApplicationNode applicationNode =  _orimanifestNode.getApplicationNode();

        // remove all activies in feature module
        Set<ActivityNode> toRemoveSet = new HashSet<ActivityNode>();

        for(ActivityNode activityNode:applicationNode.getActivityNodes()){
            if(!_activityNodes.contains(activityNode)){
                toRemoveSet.add(activityNode);
            }
        }

        for(ActivityNode activityNode:toRemoveSet){
            applicationNode.removeActivityNode(activityNode);
        }

        _content = _orimanifestNode.toString();

        LOG.info(_content);
    }


    /**
     * @return the content of this AndroidManifest.xml file
     */
    @Override
    public String getContent() {
        return _content;
    }
}
