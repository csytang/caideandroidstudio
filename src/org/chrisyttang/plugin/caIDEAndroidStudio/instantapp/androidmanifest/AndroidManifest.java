package org.chrisyttang.plugin.caIDEAndroidStudio.instantapp.androidmanifest;

import org.chrisyttang.plugin.caIDEAndroidStudio.instantapp.Module;

import java.util.List;
import java.util.Map;

public interface AndroidManifest {
    /**
     * return the module of this AndroidManifest.xml belongs to;
     */
    public Module getModule();



    /**
     * apply all changes
     */
    public void applyChanges();



    /**
     *
     * @return the content of this AndroidManifest.xml file
     */
    public String getContent();



}
