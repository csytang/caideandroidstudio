package org.chrisyttang.plugin.caIDEAndroidStudio.instantapp.androidmanifest;

import org.chrisyttang.plugin.caIDEAndroidStudio.instantapp.Module;

public abstract class AndroidManifestTemplate implements AndroidManifest {
    /**
     * return the module of this AndroidManifest.xml belongs to;
     */
    @Override
    public abstract Module getModule();

    /**
     *
     * @return content of this AndroidManifest.xml file
     */
    public abstract String getContent();
}
