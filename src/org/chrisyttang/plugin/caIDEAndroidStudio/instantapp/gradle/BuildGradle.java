package org.chrisyttang.plugin.caIDEAndroidStudio.instantapp.gradle;

import com.intellij.openapi.vfs.VirtualFile;
import org.chrisyttang.plugin.caIDEAndroidStudio.instantapp.Module;

import java.util.List;
import java.util.Map;

public interface BuildGradle {
    /**
     *
     * @return 'com.android.feature'
     */
    public String getAppliedPlugin();


    /**
     *
     * @return the module contain this build.gradle
     */
    public Module getModule();


    /**
     * apply the changes on the build.gradle file
     */
    public void applyChanges();

    /**
     *
     * @return the content of this build.gradle
     */
    public String getContent();

}
