package org.chrisyttang.plugin.caIDEAndroidStudio.instantapp.gradle;

import com.android.tools.idea.gradle.dsl.api.GradleBuildModel;
import com.android.tools.idea.gradle.dsl.api.dependencies.DependencyModel;
import com.android.tools.idea.gradle.parser.GradleBuildFile;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiElement;
import org.chrisyttang.plugin.caIDEAndroidStudio.instantapp.Module;
import org.chrisyttang.plugin.caIDEAndroidStudio.util.BuildGradleUtil;
import org.chrisyttang.plugin.caIDEAndroidStudio.util.RunnableHelper;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class InstantAppModuleBuildGradle implements BuildGradle {
    private GradleBuildFile _gradleBuildFile;
    private Module _module;
    private String _moduleName;
    private GradleBuildModel _originalModel;
    private List<String> _featureModules;
    private String _targetContent = "";

    public InstantAppModuleBuildGradle(GradleBuildFile pgradleBuildFile, Module pmodule, String pbasemodule,List<String> pfeatureModules){
        _gradleBuildFile = pgradleBuildFile;
        _module = pmodule;
        _moduleName = pbasemodule;
        _featureModules = pfeatureModules;
        applyChanges();
    }
    /**
     * @return 'com.android.instantapp'
     */
    @Override
    public String getAppliedPlugin() {
        return "com.android.instantapp";
    }



    /**
     * @return the module contain this build.gradle
     */
    @Override
    public Module getModule() {
        return null;
    }



    /**
     * apply the changes on the build.gradle file
     */
    @Override
    public void applyChanges() {
        if(this._gradleBuildFile!=null){


            Runnable runnable = new Runnable() {
                @Override
                public void run() {

                        //String backupContent = _gradleBuildFile.getPsiFile().getNode().getText();

                        _originalModel = GradleBuildModel.parseBuildFile(_gradleBuildFile.getFile(),_gradleBuildFile.getProject());
                        _targetContent = _gradleBuildFile.getPsiFile().getNode().getText();



                        // remove application plugin
                        _targetContent = _targetContent.replaceAll("apply\\s*plugin\\s*:\\s*'\\s*com.android.application\\s*'",
                                "apply plugin: 'com.android.instantapp'");

                        // remove the application Id
                        _targetContent = _targetContent.replaceAll("applicationId\\s*\"[0-9a-zA-Z.]+\"","");


                        for(DependencyModel dependencyModel:_originalModel.dependencies().all()){
                            //_originalModel.dependencies().remove(dependencyModel);
                            _targetContent = _targetContent.replaceAll(dependencyModel.getPsiElement().getText(),"");
                        }


                        String defaultConfig = BuildGradleUtil.getDependencies(_targetContent);;
                        String updatedefaultConfig = defaultConfig;

                        // change on dependences
                        for(String featuremodule:_featureModules) {
                            updatedefaultConfig = updatedefaultConfig.trim().
                                    substring(0,updatedefaultConfig.length()-2)+
                                    "implementation project(':"+featuremodule+"')\n}";
                        }


                        updatedefaultConfig = updatedefaultConfig.trim().substring(0,updatedefaultConfig.length()-2)+
                                "implementation project(':base')\n}";

                        _targetContent = _targetContent.replace(defaultConfig,updatedefaultConfig);

                        //_originalModel.applyChanges();


                        //_targetContent = _gradleBuildFile.getPsiFile().getNode().getText();
                        //_originalModel.resetState();

                        //_gradleBuildFile.getFile().setBinaryContent(backupContent.getBytes());

                        //_gradleBuildFile.getFile().refresh(false,false);
                }
            };

            RunnableHelper.runWriteCommand(_gradleBuildFile.getProject(),runnable);


        }else{
            //TODO:
        }
    }

    /**
     * @return the content of this build.gradle
     */
    @Override
    public String getContent() {
        return null;
    }
}
