package org.chrisyttang.plugin.caIDEAndroidStudio.instantapp.gradle;




import com.android.tools.idea.gradle.dsl.api.GradleBuildModel;
import com.android.tools.idea.gradle.dsl.parser.android.AndroidDslElement;
import com.android.tools.idea.gradle.dsl.parser.android.ProductFlavorDslElement;
import com.android.tools.idea.gradle.parser.GradleBuildFile;
import com.intellij.lang.ASTNode;
import com.intellij.openapi.diagnostic.Logger;
import com.intellij.openapi.editor.Document;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiDocumentManager;
import com.intellij.psi.PsiElement;
import com.intellij.psi.impl.PsiDocumentManagerBase;
import com.oracle.tools.packager.Log;
import org.chrisyttang.plugin.caIDEAndroidStudio.instantapp.Module;
import org.chrisyttang.plugin.caIDEAndroidStudio.instantapp.instant.BaseModule;
import org.chrisyttang.plugin.caIDEAndroidStudio.util.BuildGradleUtil;
import org.chrisyttang.plugin.caIDEAndroidStudio.util.RunnableHelper;
import org.jetbrains.annotations.NotNull;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class BaseModuleBuildGradle implements BuildGradle {
    private GradleBuildFile _gradleBuildFile;
    private Module _module;
    private String _moduleName;
    private GradleBuildModel _originalModel;
    private List<String> _featureModules;
    private String _targetContent = "";
    public static final Logger LOG = Logger.getInstance("org.chrisyttang.plugin.caIDEAndroidStudio.instantapp.gradle.BaseModuleBuildGradle");

    public BaseModuleBuildGradle(GradleBuildFile pgradleBuildFile, Module pmodule, String pbasemodule,List<String> pfeatureModules){
        _gradleBuildFile = pgradleBuildFile;
        _module = pmodule;
        _moduleName = pbasemodule;
        _featureModules = pfeatureModules;
        applyChanges();
    }
    /**
     * @return 'com.android.feature'
     */
    @Override
    public String getAppliedPlugin() {
        return "com.android.feature";
    }



    /**
     * @return the module contain this build.gradle
     */
    @Override
    public BaseModule getModule() {
        return (BaseModule)_module;
    }



    /**
     * apply the changes on the build.gradle file
     */
    @Override
    public void applyChanges() {
        if(this._gradleBuildFile!=null){

            this._originalModel = GradleBuildModel.parseBuildFile(_gradleBuildFile.getFile(),_gradleBuildFile.getProject());
            _targetContent = _gradleBuildFile.getPsiFile().getNode().getText();

            LOG.info(_targetContent);
            // remove application plugin
            _targetContent = _targetContent.replaceAll("apply\\s*plugin\\s*:\\s*'com.android.application'",
                    "apply plugin: 'com.android.feature'");

            LOG.info(_targetContent);
            // remove the application Id
            _targetContent = _targetContent.replaceAll("applicationId\\s*\"[0-9a-zA-Z.]+\"","");


            // change on dependences

            LOG.info(_targetContent);


            String defaultConfig = BuildGradleUtil.getDependencies(_targetContent);

            String updatedefaultConfig = defaultConfig;


            // change on dependences
            for(String featuremodule:_featureModules) {
                updatedefaultConfig = updatedefaultConfig.trim().
                        substring(0,updatedefaultConfig.length()-2)+
                        "feature project(':"+featuremodule+"')\n}";
            }

            updatedefaultConfig = updatedefaultConfig.trim().
                    substring(0,updatedefaultConfig.length()-2)+
                    "feature project(':installed')\n}";


            _targetContent = _targetContent.replace(defaultConfig,updatedefaultConfig);


            // add baseFeature true

            String androidConfig = BuildGradleUtil.getAndroid(_targetContent);
            String updatedeandroidConfig = androidConfig;

            updatedeandroidConfig = updatedeandroidConfig.trim().
                    substring(0,updatedeandroidConfig.length()-2)+"baseFeature true\n}";


            _targetContent = _targetContent.replace(androidConfig,updatedeandroidConfig);


            /*
            _originalModel.applyChanges();

            _targetContent = _gradleBuildFile.getPsiFile().getNode().getText();
            _originalModel.resetState();


            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    try {
                        _gradleBuildFile.getFile().setBinaryContent(backupContent.getBytes());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    _gradleBuildFile.getFile().refresh(false,false);
                }
            };

            RunnableHelper.runWriteCommand(_gradleBuildFile.getProject(),runnable);

            */




        }else{
            //TODO:
        }
    }

    /**
     * @return the content of this build.gradle
     */
    @Override
    public String getContent() {
        return _targetContent;
    }


}
