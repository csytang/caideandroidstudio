package org.chrisyttang.plugin.caIDEAndroidStudio.instantapp.gradle;

import com.android.tools.idea.gradle.dsl.api.GradleBuildModel;
import com.android.tools.idea.gradle.parser.GradleBuildFile;
import com.intellij.openapi.command.WriteCommandAction;
import com.intellij.openapi.editor.Document;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiDocumentManager;
import com.intellij.psi.PsiElement;
import com.intellij.psi.impl.PsiDocumentManagerBase;
import org.chrisyttang.plugin.caIDEAndroidStudio.instantapp.Module;
import org.chrisyttang.plugin.caIDEAndroidStudio.instantapp.instant.FeatureModule;
import org.chrisyttang.plugin.caIDEAndroidStudio.util.BuildGradleUtil;
import org.chrisyttang.plugin.caIDEAndroidStudio.util.RunnableHelper;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FeatureModuleBuildGradle implements BuildGradle {

    private GradleBuildFile _gradleBuildFile;
    private Module _module;
    private String _moduleName;
    private GradleBuildModel _originalModel;
    private List<String> _featureModules;
    private String _targetContent = "";
    private PsiDocumentManagerBase _docmanager = null;

    public FeatureModuleBuildGradle(GradleBuildFile pgradleBuildFile, Module pmodule, String pbasemodule, List<String> pfeatureModules){
        _gradleBuildFile = pgradleBuildFile;
        _module = pmodule;
        _moduleName = pbasemodule;
        _featureModules = pfeatureModules;
        _docmanager  = (PsiDocumentManagerBase) PsiDocumentManager.getInstance(_gradleBuildFile.getProject());;
        applyChanges();
    }

    /**
     * @return 'com.android.feature'
     */
    @Override
    public String getAppliedPlugin() {
        return "com.android.feature";
    }


    /**
     * @return the module contain this build.gradle
     */
    @Override
    public FeatureModule getModule() {
        return (FeatureModule)_module;
    }



    /**
     * apply the changes on the build.gradle file
     */
    @Override
    public void applyChanges() {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                if (_gradleBuildFile != null) {
                    // remove application plugin
                    _targetContent = _gradleBuildFile.getPsiFile().getNode().getText();

                    _targetContent = _targetContent.replaceAll("apply\\s*plugin\\s*:\\s*'\\s*com.android.application\\s*'",
                            "apply plugin: 'com.android.instantapp'");


                    // remove the application Id
                    _targetContent = _targetContent.replaceAll("applicationId\\s*\"[0-9a-zA-Z.]+\"","");


                    // change on dependences

                    String defaultConfig = BuildGradleUtil.getDependencies(_targetContent);
                    String updatedefaultConfig = defaultConfig;

                    updatedefaultConfig = updatedefaultConfig.replace("}","implementation project(':base')\n}");


                    _targetContent = _targetContent.replace(defaultConfig,updatedefaultConfig);

                    /*
                    try {
                        _originalModel.applyChanges();
                    }catch (Exception e){

                    }


                    _targetContent = _gradleBuildFile.getPsiFile().getNode().getText();
                    */





                } else {
                    //TODO:

                }
            }
        };

        WriteCommandAction.runWriteCommandAction(_gradleBuildFile.getProject(), runnable);
    }

    /**
     * @return the content of this build.gradle
     */
    @Override
    public String getContent() {
        return _targetContent;
    }


}
