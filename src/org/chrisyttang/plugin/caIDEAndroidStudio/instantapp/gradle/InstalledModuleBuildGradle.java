package org.chrisyttang.plugin.caIDEAndroidStudio.instantapp.gradle;

import com.android.tools.idea.gradle.dsl.api.GradleBuildModel;
import com.android.tools.idea.gradle.dsl.api.dependencies.DependencyModel;
import com.android.tools.idea.gradle.dsl.model.android.AndroidModelImpl;
import com.android.tools.idea.gradle.parser.GradleBuildFile;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiElement;
import org.chrisyttang.plugin.caIDEAndroidStudio.instantapp.Module;
import org.chrisyttang.plugin.caIDEAndroidStudio.instantapp.installed.InstalledAppModule;
import org.chrisyttang.plugin.caIDEAndroidStudio.util.BuildGradleUtil;
import org.chrisyttang.plugin.caIDEAndroidStudio.util.RunnableHelper;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class InstalledModuleBuildGradle implements BuildGradle{

    private GradleBuildFile _gradleBuildFile;
    private Module _module;
    private String _moduleName;
    private GradleBuildModel _originalModel;
    private List<String> _featureModules;
    private String _targetContent = "";

    public InstalledModuleBuildGradle(GradleBuildFile pgradleBuildFile, Module pmodule, List<String> pfeatureModules){
        _gradleBuildFile = pgradleBuildFile;
        _module = pmodule;
        _moduleName = "installed";
        _featureModules = pfeatureModules;
        applyChanges();
    }
    /**
     * @return 'com.android.application'
     */
    @Override
    public String getAppliedPlugin() {
        return "com.android.application";
    }



    /**
     * @return the module contain this build.gradle
     */
    @Override
    public InstalledAppModule getModule() {
        return (InstalledAppModule)_module;
    }



    /**
     * apply the changes on the build.gradle file
     */
    @Override
    public void applyChanges() {
        if(this._gradleBuildFile!=null){

            this._originalModel = GradleBuildModel.parseBuildFile(_gradleBuildFile.getFile(),_gradleBuildFile.getProject());
            _targetContent = _gradleBuildFile.getPsiFile().getNode().getText();


            // remove application plugin

            for(DependencyModel dependencyModel:_originalModel.dependencies().all()){
                _targetContent = _targetContent.replace(dependencyModel.getPsiElement().getText(),"");

            }

            // change on dependences

            String defaultConfig = BuildGradleUtil.getDependencies(_targetContent);
            String updatedefaultConfig = defaultConfig;

            // change on dependences
            for(String featuremodule:_featureModules) {
                updatedefaultConfig = updatedefaultConfig.trim().
                        substring(0,updatedefaultConfig.length()-2)+
                        "implementation project(':"+featuremodule+"')\n}";
            }

            updatedefaultConfig = updatedefaultConfig.trim().
                    substring(0,updatedefaultConfig.length()-2)+
                    "implementation project(':base')\n}";

            _targetContent = _targetContent.replace(defaultConfig,updatedefaultConfig);

            /*

            try {
                _originalModel.applyChanges();
            }catch (Exception e){

            }

            _targetContent = _gradleBuildFile.getPsiFile().getNode().getText();

            */





        }else{
            //TODO:
        }
    }

    /**
     * @return the content of this build.gradle
     */
    @Override
    public String getContent() {
        return _targetContent;
    }
}
