package org.chrisyttang.plugin.caIDEAndroidStudio.instantapp.configuration;

import org.chrisyttang.androidmanifest.dom.ActivityNode;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class InstantAppConfiguration {

    private List<ActivityNode> _baseModuleActivity;
    private List<ActivityNode> _featureModuleActivities;
    private Map<ActivityNode,String> _activityNodePackageNameMap = new HashMap<ActivityNode,String>();
    private Map<ActivityNode,String> _activityNodeHostMap = new HashMap<ActivityNode,String>();
    private Map<ActivityNode,String> _activityModuleNameMapping = new HashMap<ActivityNode,String>();

    public InstantAppConfiguration(List<ActivityNode> pbaseModuleActivity,List<ActivityNode> pfeatureModuleActivities){
        this._baseModuleActivity = pbaseModuleActivity;
        this._featureModuleActivities = pfeatureModuleActivities;
    }

    public void addActivityNodePackgeNameMapping(ActivityNode activityNode,String packageName){
        _activityNodePackageNameMap.put(activityNode,packageName);
    }

    public Map<ActivityNode,String> getActivityNodePackgeNameMapping(){
        return _activityNodePackageNameMap;
    }

    public void addActivityNodeURLMapping(ActivityNode activityNode,String url){
        _activityNodeHostMap.put(activityNode, url);
    }

    public Map<ActivityNode,String> getActivityNodeURLMapping(){
        return _activityNodeHostMap;
    }

    public void addActivityNodeModuleNameMapping(ActivityNode activityNode,String moduleName){
        _activityModuleNameMapping.put(activityNode, moduleName);
    }

    public Map<ActivityNode,String> getActivityNodeModuleNameMapping(){
        return _activityModuleNameMapping;
    }

    public List<ActivityNode> getBaseModuleActivity() {
        return _baseModuleActivity;
    }

    public List<ActivityNode> getFeatureModuleActivities() {
        return _featureModuleActivities;
    }

    public List<String> getModules(){
        return new LinkedList<>(_activityModuleNameMapping.values());
    }
}
