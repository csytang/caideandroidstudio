package org.chrisyttang.plugin.caIDEAndroidStudio.instantapp.configuration;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.DialogWrapper;
import com.intellij.psi.search.FilenameIndex;
import com.intellij.psi.search.GlobalSearchScope;
import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;
import com.intellij.uiDesigner.core.Spacer;
import org.chrisyttang.androidmanifest.dom.ActivityNode;
import org.chrisyttang.plugin.caIDEAndroidStudio.instantapp.ModuleType;
import org.jetbrains.annotations.Nullable;

import java.awt.event.*;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.util.Map;

public class InstantAppConfigurationForm extends DialogWrapper {
    private JComboBox activity_comboBox;
    private JButton addButton;
    private JList baseActivityUIList;
    private DefaultListModel baseActivityUIListModel = new DefaultListModel();
    private JList featureActivityUIList;
    private DefaultListModel featureActivitiesUIListModel = new DefaultListModel();
    private JRadioButton baseRadioButton;
    private JRadioButton featureRadioButton;
    private JPanel rootPanel;
    private JPanel activityselectionpanel;
    private JPanel displayPanel;
    private JButton backButton;
    private JButton nextButton;
    private JPanel controlPanel;
    private List<ActivityNode> activityNodeList;
    private List<String> activityNodeNameList = new LinkedList<>();
    private int currentPageIndex = 0;
    private int MAXPAGEINDEX = 3;

    private InstantAppLinkAssistant instantAppLinkAssistantPage = null;
    private InstantAppPackageNameSetup instantAppPackageNameSetup = null;
    private InstantAppModuleNameConfiguration instantAppModuleNameConfiguration = null;

    private ModuleType moduleType;
    private ActivityNode selectedActivity;
    private List<ActivityNode> baseModuleActivity = new LinkedList<>();
    private List<ActivityNode> featureModuleActivities = new LinkedList<>();

    // the name of activities in feature modules
    private List<String> featurebindActivities = new LinkedList<>();

    private Project _project;
    private List<String> _features;
    private String _packageName;
    private InstantAppConfiguration instantAppConfiguration;
    private Map<String, ActivityNode> nameToActivityNode = new HashMap<>();

    public InstantAppConfigurationForm(Project project, String ppackageName, List<ActivityNode> pactivityNodeList, List<String> pfeatures) {
        super(project);
        _project = project;
        _features = pfeatures;
        _packageName = ppackageName;
        this.activityNodeList = pactivityNodeList;
        for (int i = 0; i < this.activityNodeList.size(); i++) {
            activityNodeNameList.add(this.activityNodeList.get(i).getName());
            nameToActivityNode.put(this.activityNodeList.get(i).getName(), this.activityNodeList.get(i));
        }

        $$$setupUI$$$();


        setResizable(true);
        init();

        activity_comboBox.addItemListener(new ItemListener() {
            /**
             * Invoked when an item has been selected or deselected by the user.
             * The code written for this method performs the operations
             * that need to occur when an item is selected (or deselected).
             *
             * @param e
             */
            @Override
            public void itemStateChanged(ItemEvent e) {
                String selectedActivityName = (String) e.getItem();
                for (ActivityNode activityNode : activityNodeList) {
                    if (activityNode.getName().equals(selectedActivityName)) {
                        selectedActivity = activityNode;
                    }
                }
            }
        });

        // add a activity to a mapping
        addButton.addActionListener(new ActionListener() {
            /**
             * Invoked when an action occurs.
             *
             * @param e
             */
            @Override
            public void actionPerformed(ActionEvent e) {
                int selectedIndex = activity_comboBox.getSelectedIndex();
                String selectedactivityNodeName = activityNodeNameList.get(selectedIndex);
                if (baseActivityUIListModel.contains(selectedactivityNodeName) ||
                        featureActivitiesUIListModel.contains(selectedactivityNodeName)) {
                    if (baseActivityUIListModel.contains(selectedactivityNodeName)) {
                        JOptionPane.showMessageDialog(null, "The activity " + selectedactivityNodeName + " is already mapped to a base module", "Error", JOptionPane.INFORMATION_MESSAGE);
                    } else {
                        JOptionPane.showMessageDialog(null, "The activity " + selectedactivityNodeName + " is already mapped to a feature module", "Error", JOptionPane.INFORMATION_MESSAGE);
                    }
                } else {

                    if (moduleType == ModuleType.BASE) {
                        // base module
                        if (!baseActivityUIListModel.contains(selectedactivityNodeName)) {
                            baseActivityUIListModel.add(baseActivityUIListModel.size(), selectedactivityNodeName);

                            // add to base module activity list
                            baseModuleActivity.add(nameToActivityNode.get(selectedactivityNodeName));
                            if (baseActivityUIListModel.size() == 1) {
                                baseRadioButton.setEnabled(false);
                            }
                        }
                    } else if (moduleType == ModuleType.FEATURE) {
                        //feature module
                        if (!featureActivitiesUIListModel.contains(selectedactivityNodeName)) {
                            featureActivitiesUIListModel.add(featureActivitiesUIListModel.size(), selectedactivityNodeName);

                            // add the activity to the feature activity list
                            featureModuleActivities.add(nameToActivityNode.get(selectedactivityNodeName));

                        }
                    }

                    featurebindActivities.add(selectedactivityNodeName);
                }
            }
        });

        nextButton.addActionListener(new ActionListener() {
            /**
             * Invoked when an action occurs.
             *
             * @param e
             */
            @Override
            public void actionPerformed(ActionEvent e) {
                if (currentPageIndex != MAXPAGEINDEX) {

                    currentPageIndex++;
                    if (currentPageIndex == MAXPAGEINDEX) {
                        nextButton.setEnabled(false);
                    } else {
                        nextButton.setEnabled(true);
                    }
                    if (!backButton.isEnabled()) {
                        backButton.setEnabled(true);
                    }
                    loadTargetPage();
                }
            }
        });
        backButton.addActionListener(new ActionListener() {
            /**
             * Invoked when an action occurs.
             *
             * @param e
             */
            @Override
            public void actionPerformed(ActionEvent e) {
                if (currentPageIndex != 0) {

                    currentPageIndex--;
                    if (currentPageIndex != 0) {
                        backButton.setEnabled(true);
                    } else {
                        backButton.setEnabled(false);
                    }
                    if (!nextButton.isEnabled()) {
                        nextButton.setEnabled(true);
                    }
                }
                loadTargetPage();
            }
        });
        ChangeListener listener = new ChangeListener() {
            /**
             * Invoked when the target of the listener has changed its state.
             *
             * @param e a ChangeEvent object
             */
            @Override
            public void stateChanged(ChangeEvent e) {
                AbstractButton button = (AbstractButton) e.getSource();
                String selected = button.getText();
                if (selected.equals("Feature")) {
                    //base module
                    moduleType = ModuleType.FEATURE;
                } else if (selected.equals("Base")) {
                    //feature module
                    moduleType = ModuleType.BASE;
                }
            }
        };

        featureRadioButton.addChangeListener(listener);
        baseRadioButton.addChangeListener(listener);
        baseActivityUIList.addMouseListener(new MouseAdapter() {
            /**
             * {@inheritDoc}
             *
             * @param e
             */
            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    int index = baseActivityUIList.locationToIndex(e.getPoint());
                    baseActivityUIListModel.remove(index);
                    if (baseActivityUIListModel.isEmpty()) {
                        baseRadioButton.setEnabled(true);
                    }
                }
            }
        });

        featureActivityUIList.addMouseListener(new MouseAdapter() {
            /**
             * {@inheritDoc}
             *
             * @param e
             */
            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    int index = featureActivityUIList.locationToIndex(e.getPoint());
                    featureActivitiesUIListModel.remove(index);
                }
            }
        });


    }

    /**
     * this will load the target page to the foreground
     */
    private void loadTargetPage() {
        displayPanel.removeAll();
        displayPanel.revalidate();
        displayPanel.repaint();
        List<String> baseActivityString = new LinkedList<String>();

        switch (currentPageIndex) {
            case 0: {
                displayPanel.add(activityselectionpanel, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
                break;
            }
            case 1: {
                instantAppLinkAssistantPage = new InstantAppLinkAssistant(_project, featurebindActivities);
                displayPanel.add(instantAppLinkAssistantPage.$$$getRootComponent$$$(), new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
                break;
            }
            case 2: {
                baseActivityString.clear();
                baseActivityString.add(baseModuleActivity.get(0).getName());
                instantAppPackageNameSetup = new InstantAppPackageNameSetup(_project, featurebindActivities, _packageName, baseActivityString);
                displayPanel.add(instantAppPackageNameSetup.$$$getRootComponent$$$(), new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
                break;
            }
            case 3: {
                baseActivityString.clear();
                baseActivityString.add(baseModuleActivity.get(0).getName());
                instantAppModuleNameConfiguration = new InstantAppModuleNameConfiguration(_project, featurebindActivities, baseActivityString);
                displayPanel.add(instantAppModuleNameConfiguration.$$$getRootComponent$$$(), new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
                break;
            }
        }
        rootPanel.updateUI();
    }

    private void createUIComponents() {
        // combox binding with the activity list
        activity_comboBox = new JComboBox(this.activityNodeNameList.toArray());
        rootPanel = new JPanel();
        displayPanel = new JPanel();
        baseActivityUIList = new JList(baseActivityUIListModel);
        featureActivityUIList = new JList(featureActivitiesUIListModel);
    }

    @Override
    protected void doOKAction() {
        // save the result collected
        if (validChecking()) {
            // collect from all pages;
            // collect from page 0(instant app configuration pages)
            // save in the baseModuleActivity and featureModuleActivities
            instantAppConfiguration = new InstantAppConfiguration(this.baseModuleActivity, this.featureModuleActivities);

            // save the result in page 1;
            Map<String, String> activityURLMapping = instantAppLinkAssistantPage.getActivityURLMapping();
            for (Map.Entry<String, String> entry : activityURLMapping.entrySet()) {
                instantAppConfiguration.addActivityNodeURLMapping(this.nameToActivityNode.get(entry.getKey()), entry.getValue());
            }

            // save the result in page 2;
            Map<String, String> activityPacakgeMapping = instantAppPackageNameSetup.getActivityPackageMapping();
            for (Map.Entry<String, String> entry : activityPacakgeMapping.entrySet()) {
                instantAppConfiguration.addActivityNodePackgeNameMapping(this.nameToActivityNode.get(entry.getKey()), entry.getValue());
            }

            // save the result in page 3;
            Map<String, String> activityModuleMapping = instantAppModuleNameConfiguration.getActivityModuleMapping();
            for (Map.Entry<String, String> entry : activityModuleMapping.entrySet()) {
                instantAppConfiguration.addActivityNodeModuleNameMapping(this.nameToActivityNode.get(entry.getKey()), entry.getValue());
            }


            super.doOKAction();
        }
    }

    public InstantAppConfiguration getInstantAppConfiguration() {
        return instantAppConfiguration;
    }

    protected boolean validChecking() {
        if (currentPageIndex != 3) {
            return false;
        }
        return true;
    }

    @Override
    public void doCancelAction() {
        super.doCancelAction();
    }

    @Nullable
    @Override
    protected JComponent createCenterPanel() {
        return rootPanel;
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$() {
        createUIComponents();
        rootPanel.setLayout(new GridLayoutManager(2, 1, new Insets(0, 0, 0, 0), -1, -1));
        displayPanel.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        displayPanel.setEnabled(false);
        rootPanel.add(displayPanel, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        activityselectionpanel = new JPanel();
        activityselectionpanel.setLayout(new GridLayoutManager(2, 1, new Insets(0, 0, 0, 0), -1, -1));
        displayPanel.add(activityselectionpanel, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        final JPanel panel1 = new JPanel();
        panel1.setLayout(new GridLayoutManager(1, 5, new Insets(0, 0, 0, 0), -1, -1));
        activityselectionpanel.add(panel1, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        panel1.add(activity_comboBox, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        addButton = new JButton();
        addButton.setText("Add");
        panel1.add(addButton, new GridConstraints(0, 4, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        featureRadioButton = new JRadioButton();
        featureRadioButton.setText("Feature");
        panel1.add(featureRadioButton, new GridConstraints(0, 3, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        baseRadioButton = new JRadioButton();
        baseRadioButton.setSelected(false);
        baseRadioButton.setText("Base");
        panel1.add(baseRadioButton, new GridConstraints(0, 2, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final Spacer spacer1 = new Spacer();
        panel1.add(spacer1, new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, 1, null, null, null, 0, false));
        final JPanel panel2 = new JPanel();
        panel2.setLayout(new GridLayoutManager(2, 2, new Insets(0, 0, 0, 0), -1, -1));
        activityselectionpanel.add(panel2, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        final JScrollPane scrollPane1 = new JScrollPane();
        panel2.add(scrollPane1, new GridConstraints(1, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        scrollPane1.setViewportView(featureActivityUIList);
        final JLabel label1 = new JLabel();
        label1.setText("Activities in Feature Module");
        panel2.add(label1, new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label2 = new JLabel();
        label2.setText("Activities in Base Module");
        panel2.add(label2, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JScrollPane scrollPane2 = new JScrollPane();
        panel2.add(scrollPane2, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        scrollPane2.setViewportView(baseActivityUIList);
        controlPanel = new JPanel();
        controlPanel.setLayout(new GridLayoutManager(1, 3, new Insets(0, 0, 0, 0), -1, -1));
        rootPanel.add(controlPanel, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        backButton = new JButton();
        backButton.setEnabled(false);
        backButton.setText("Back");
        controlPanel.add(backButton, new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        nextButton = new JButton();
        nextButton.setText("Next");
        controlPanel.add(nextButton, new GridConstraints(0, 2, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final Spacer spacer2 = new Spacer();
        controlPanel.add(spacer2, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, 1, null, null, null, 0, false));
        ButtonGroup buttonGroup;
        buttonGroup = new ButtonGroup();
        buttonGroup.add(baseRadioButton);
        buttonGroup.add(featureRadioButton);
    }

    /**
     * @noinspection ALL
     */
    public JComponent $$$getRootComponent$$$() {
        return rootPanel;
    }
}
