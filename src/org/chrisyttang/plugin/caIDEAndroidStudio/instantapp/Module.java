package org.chrisyttang.plugin.caIDEAndroidStudio.instantapp;

import com.android.tools.idea.gradle.parser.GradleBuildFile;
import com.intellij.openapi.vfs.VirtualFile;
import org.chrisyttang.androidmanifest.dom.ActivityNode;
import org.chrisyttang.androidmanifest.dom.ManifestNode;

import java.io.File;
import java.util.Collection;
import java.util.List;

public interface Module {


    /**
     *
     * @return build.gradle file from the module
     */
    public File getBuildGradle();

    /**
     *
     * @return the AndroidManifest.xml of this module
     */
    public abstract File getAndroidManifest();

    /**
     * this is function must be called at runtime for init the module
     */
    public void initModule(GradleBuildFile _buildGradleSource, ManifestNode _androidManifestSource, Collection<File> _assetSource,
                           Collection<File> _resSource, List<String> _featuremodules);


    /**
     *
     * @return the package name of this module
     */
    public String getPackageName();

    /**
     *
     * @return whether this is a module for installed app
     */
    public boolean isInstalledModule();

    /**
     *
     * @return init the module directory
     */
    public boolean initModuleDirectory();

    /**
     *
     * @param buildGradleSource
     * @param featuremodules
     * @return
     */
    public boolean initBuildGradleFile(GradleBuildFile buildGradleSource, List<String> featuremodules);

    /**
     *
     * @param _assetSource
     * @param _resSource
     * @return
     */
    public boolean cloneResources(Collection<File> _assetSource,
                                  Collection<File> _resSource);


    /**
     *
     * @return
     */
    public boolean customizeCloneSourceCode(List<VirtualFile>srcRoots,List<VirtualFile> moduleActivities,
                                            List<VirtualFile>featureModuleActivities);


    /**
     *
     * @return the activity binds to this module
     */
    public List<ActivityNode> getActivities();


    /**
     *
     * @param _androidManifestSource
     * @return
     */
    public boolean initAndroidManifestXMLFile(ManifestNode _androidManifestSource);

    /**
     *
     * @return the name of this module
     */
    public String getModuleName();

}
