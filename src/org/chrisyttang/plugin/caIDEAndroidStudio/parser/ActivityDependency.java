package org.chrisyttang.plugin.caIDEAndroidStudio.parser;

import com.intellij.ide.DataManager;
import com.intellij.ide.highlighter.JavaFileType;
import com.intellij.openapi.actionSystem.ActionManager;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.diagnostic.Logger;
import com.intellij.openapi.fileEditor.impl.LoadTextUtil;
import com.intellij.openapi.fileTypes.StdFileTypes;
import com.intellij.openapi.module.Module;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiFile;
import com.intellij.psi.PsiJavaFile;
import org.chrisyttang.plugin.caIDEAndroidStudio.util.DialogUtil;
import org.chrisyttang.plugin.caIDEAndroidStudio.util.PsiFileUtil;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ActivityDependency {

    private VirtualFile _activity;
    private List<VirtualFile> _allViewsInUse = new LinkedList<>();
    private List<VirtualFile> _allFragementsInUse = new LinkedList<>();
    private List<String> _allViewsIdInUseStr = new LinkedList<>();
    private List<String> _allFragmentsIdInUseStr = new LinkedList<>();
    public static final Logger LOG = Logger.getInstance("org.chrisyttang.plugin.caIDEAndroidStudio.parser.ActivityDependency");

    private String _findViewByIdPattern = "findViewById[(]R.id.\\w+[)]";
    private String _findFragmentByIdPattern = "findFragmentById[(]R.id.\\w+[)]";
    private String _content = "";
    private Module _module;

    public ActivityDependency(VirtualFile pactivity,Module pmodule){
        _activity = pactivity;
        _module = pmodule;
        _content = LoadTextUtil.loadText(_activity).toString();

        parse();
    }

    private void parse(){

        // from findViewById
        Pattern viewPattern = Pattern.compile(_findViewByIdPattern);

        Matcher viewMatcher = viewPattern.matcher(_content);
        extractIdInUse(viewMatcher,_allViewsIdInUseStr);

        // from findFragmentById
        Pattern fragmentPattern = Pattern.compile(_findFragmentByIdPattern);

        Matcher fragmentMatcher = fragmentPattern.matcher(_content);
        extractIdInUse(fragmentMatcher,_allFragmentsIdInUseStr);

        // find views' class from id
        Set<VirtualFile> allFileInModule = PsiFileUtil.exploreAllFileInModule(_module);

        for(VirtualFile virtualFile: allFileInModule){
            PsiFile psiFile = PsiFileUtil.VirtualFileToPsiFileConverter(virtualFile,_module.getProject());
            if(psiFile.getFileType()== StdFileTypes.JAVA){
                PsiJavaFile psiJavaFile = (PsiJavaFile)psiFile;
                PsiClass[] psiClasses = psiJavaFile.getClasses();
                for(PsiClass psiClass:psiClasses){
                    PsiClass superClass = psiClass.getSuperClass();
                    while(superClass!=null) {
                        String name = superClass.getQualifiedName();
                        if (name.equals("android.view.View")) {
                            if(isViewBindWithId(virtualFile)){
                                this._allViewsInUse.add(virtualFile);
                            }
                        }else if(name.endsWith("app.Fragment") &&
                                name.startsWith("android")){
                            if(isFragmentBindWithId(virtualFile)){
                                this._allFragementsInUse.add(virtualFile);
                            }
                        }
                        if(name.equals("java.lang.Object")){
                            break;
                        }
                        superClass = superClass.getSuperClass();
                    }
                }
            }
        }

     //   DialogUtil.createDialog("The possible depenencies have been analyzed");

    }

    private boolean isViewBindWithId(VirtualFile viewFile){
        String content = LoadTextUtil.loadText(viewFile).toString();
        boolean containid = false;
        for(String id:this._allViewsIdInUseStr){
            if(content.contains(id)){
                containid = true;
            }
        }

        if(!containid){
            return false;
        }
        return true;
    }


    private boolean isFragmentBindWithId(VirtualFile fragmentFile){
        String content = LoadTextUtil.loadText(fragmentFile).toString();
        boolean containid = false;
        for(String id:this._allFragmentsIdInUseStr){
            if(content.contains(id)){
                containid = true;
            }
        }

        if(!containid){
            return false;
        }

        return true;
    }




    private void extractIdInUse(Matcher matcher,List<String> idInUse){
        while (matcher.find()) {
            String findId = matcher.group(0);
            String id = findId.substring(1+findId.lastIndexOf("."),findId.indexOf(")"));
            LOG.info(id);
            if(!idInUse.contains(id)){
                idInUse.add(id);
            }
        }
    }

    public List<VirtualFile> getAllRelated(){
        List<VirtualFile> all_related_files = new LinkedList<VirtualFile>();
        all_related_files.addAll(_allFragementsInUse);
        all_related_files.addAll(_allViewsInUse);
        all_related_files.add(_activity);
        return all_related_files;
    }



}
