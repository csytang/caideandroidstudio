package org.chrisyttang.plugin.caIDEAndroidStudio.parser;

import com.intellij.openapi.actionSystem.*;
import com.intellij.openapi.command.WriteCommandAction;
import com.intellij.openapi.diagnostic.Logger;
import com.intellij.openapi.module.Module;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import org.chrisyttang.androidmanifest.dom.ActivityNode;
import org.chrisyttang.plugin.caIDEAndroidStudio.util.CommandLineUtil;

import java.io.*;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static org.chrisyttang.plugin.caIDEAndroidStudio.util.CommandLineUtil.executCommand;

public class ActivityTransactionGraph {

    private String _AndroidSDKLocation;
    private String _GatorLocation;
    private Project _project;
    private Module _module;
    private AnActionEvent _anActionEvent;
    private String _targetApkPath;
    private Logger LOG = Logger.getInstance("org.chrisyttang.plugin.caIDEAndroidStudio.parser.ActivityTransactionGraph");
    private VirtualFile _viratgFile = null;
    private ATGParser _atgParser = null;
    private AndroidManifestParserWrapper _androidManifestParserWrapper;
    private List<ActivityNode> _activities;
    private Map<String,ActivityNode> _nameToActivityNodes = new HashMap<String, ActivityNode>();

    public ActivityTransactionGraph(String pAndroidSDKLocation, String pGatorLocation,Project pproject, Module ppmodule,
                                    AnActionEvent panActionEvent){
        if(pAndroidSDKLocation.endsWith("\\")) {
            this._AndroidSDKLocation = pAndroidSDKLocation.substring(0,pAndroidSDKLocation.length()-1);
        }else{
            this._AndroidSDKLocation = pAndroidSDKLocation;
        }

        if(pGatorLocation.endsWith("\\")){
            this._GatorLocation = pGatorLocation.substring(0,pGatorLocation.length()-1);
        }else{
            this._GatorLocation = pGatorLocation;
        }

        this._project = pproject;
        this._module = ppmodule;
        this._anActionEvent = panActionEvent;
        this._targetApkPath = this._module.getModuleFilePath().substring(0,
        this._module.getModuleFilePath().lastIndexOf("/"))+ File.separator+"build"+File.separator
        +"outputs"+File.separator+"apk"+File.separator+_module.getName()+"-debug.apk";
        this._androidManifestParserWrapper = AndroidManifestParserWrapper.getInstant(this._project,this._module);
        this._activities = _androidManifestParserWrapper.getAllActivities();

        for(ActivityNode _activity:this._activities){
            _nameToActivityNodes.put(_activity.getName(),_activity);
        }
    }

    public void parse() throws InterruptedException {


        // check the apk is created;
        // module/build/outputs/apks/?.apk
        /*
        if(!new File(this._targetApkPath).exists()){
            // 1. build the android project into an apk;
            ActionManager am = ActionManager.getInstance();//Android.BuildApk
            AnAction buildAction = am.getActionOrStub("Android.BuildApk");
            buildAction.actionPerformed(_anActionEvent);
            buildAction.wait();
        }



        // 2. export the environment variable
        if(!this._AndroidSDKLocation.equals("")){
            if(this._AndroidSDKLocation.endsWith("\\")){
                this._AndroidSDKLocation = this._AndroidSDKLocation.substring(0,this._AndroidSDKLocation.length()-1);
            }
        }
        if(!this._GatorLocation.equals("")){
            if(this._GatorLocation.endsWith("\\")){
                this._GatorLocation = this._GatorLocation.substring(0,this._GatorLocation.length()-1);
            }
        }




        // 3. run the gator to generate an <modulename>.atg
        // move to target directory
        List<String>gatorArguments = new LinkedList<>();
        gatorArguments.add("python2");
        gatorArguments.add("runGatorOnApk.py");
        gatorArguments.add(_targetApkPath);
        gatorArguments.add("-client");
        gatorArguments.add("WTGDemoClient");
        //gatorArguments.add(">");
        //gatorArguments.add(_project.getBaseDir()+File.separator+_module.getName()+".atg");



        Map<String,String> environmentVariables = new HashMap<String,String>();


        environmentVariables.put("GatorRoot",this._GatorLocation);
        environmentVariables.put("ADT",this._AndroidSDKLocation);
        */
        boolean viratgCreated = false;
        for(VirtualFile virturalFile : _project.getBaseDir().getChildren()){
            if(virturalFile.getPath().endsWith(_module.getName()+".atg")){
                _viratgFile = virturalFile;
                viratgCreated = true;
                break;
            }
        }

        /*
        if(!viratgCreated) {

            Runnable createNewFileRunnable = new Runnable() {
                @Override
                public void run() {
                    // create the feature model file

                    try {
                        _viratgFile = _project.getBaseDir().findOrCreateChildData(this, _module.getName() + ".atg");
                        String outputPath = _viratgFile.getPath();
                        CommandLineUtil.executCommand(new File(_GatorLocation + File.separator + "AndroidBench"),
                                gatorArguments, environmentVariables, outputPath);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            };

            WriteCommandAction.runWriteCommandAction(this._project, createNewFileRunnable);

        }
        */
    }

    public VirtualFile getResultVirtualFile(){
        return _viratgFile;
    }


    public ATGraph getATGraph() {
        _atgParser = new ATGParser(_viratgFile,_nameToActivityNodes);
        return _atgParser.getATGraph();
    }
}
