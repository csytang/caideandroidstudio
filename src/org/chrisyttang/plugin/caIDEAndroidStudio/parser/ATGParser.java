package org.chrisyttang.plugin.caIDEAndroidStudio.parser;

import com.intellij.openapi.fileEditor.impl.LoadTextUtil;
import com.intellij.openapi.vfs.VirtualFile;
import org.chrisyttang.androidmanifest.dom.ActivityNode;

import java.util.Map;

public class ATGParser {

    private VirtualFile _atgfile;
    private String _content;
    private Map<String,ActivityNode> _nameToActivityNode;
    private ATGraph _atGraph = new ATGraph();

    public ATGParser(VirtualFile patgfile, Map<String,ActivityNode> pnameToActivityNode){
        this._atgfile = patgfile;
        this._nameToActivityNode = pnameToActivityNode;
        this._content = LoadTextUtil.loadText(_atgfile).toString();
        parse(this._content);
    }
    private void parse(String content){
        String[]lines = content.split("\n");
        int index = lines.length;
        for(int i = 0; i < lines.length; i++){
            if(lines[i].startsWith("[WTGBuilder]: stage 6 finishes")){
                index = i;
                break;
            }
        }

        boolean withinEdge = false;
        int edgeStartIndex = index;
        int edgeEndIndex = index;
        for(int i = index;i < lines.length;i++){


            if(!lines[i].startsWith("[DEMO]: Current Edge ID:")){
                continue;
            }else if(withinEdge && lines[i].startsWith("[WTGDemoClient] End:")){
                edgeEndIndex = i;
                processEdge(edgeStartIndex,edgeEndIndex,lines);
                break;
            } else{
                if(!withinEdge){
                    edgeStartIndex = i;
                    withinEdge = true;
                }else{
                    edgeEndIndex = i-1;
                    withinEdge = false;
                    i = edgeEndIndex;
                    processEdge(edgeStartIndex,edgeEndIndex,lines);
                }
            }
        }


        for(Map.Entry<String,ActivityNode>entry:_nameToActivityNode.entrySet()){
            ActivityNode activityNode = entry.getValue();
            _atGraph.addVertex(activityNode);
        }


    }

    protected void processEdge(int startIndex,int endIndex,String[]lines){
        String sourceActivityName = "";
        String targetActivityName = "";
        for(int i = startIndex;i <= endIndex;i++){
            String content = lines[i];
            if(content.contains("Source Window:")){
                String mainContent = content.substring(content.indexOf("Source Window:")+"Source Window:".length(), content.length());
                sourceActivityName = mainContent.substring(mainContent.indexOf("[")+1,mainContent.indexOf("]"));
            }else if(content.contains("Target Window:")){
                String mainContent = content.substring(content.indexOf("Target Window:")+"Target Window:".length(), content.length());
                targetActivityName = mainContent.substring(mainContent.indexOf("[")+1,mainContent.indexOf("]"));
            }
        }

        if(sourceActivityName.isEmpty()||targetActivityName.isEmpty()){
            return;
        }

        if(_nameToActivityNode.containsKey(sourceActivityName) &&
                _nameToActivityNode.containsKey(targetActivityName)){
            ActivityNode activitysource = _nameToActivityNode.get(sourceActivityName);
            ActivityNode activitytarget = _nameToActivityNode.get(targetActivityName);
            _atGraph.addEdge(activitysource, activitytarget);
        }

    }

    public ATGraph getATGraph(){
        return _atGraph;
    }

}
