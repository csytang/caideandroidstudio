package org.chrisyttang.plugin.caIDEAndroidStudio.parser;

import org.chrisyttang.androidmanifest.dom.ActivityNode;
import org.chrisyttang.androidmanifest.dom.AndroidManifestNode;
import org.jgrapht.Graph;
import org.jgrapht.GraphPath;
import org.jgrapht.alg.connectivity.KosarajuStrongConnectivityInspector;
import org.jgrapht.alg.interfaces.StrongConnectivityAlgorithm;
import org.jgrapht.alg.shortestpath.DijkstraShortestPath;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.graph.DefaultEdge;

public class ATGraph {

    private Graph<ActivityNode,DefaultEdge> atgraph = null;

    public ATGraph(){
        atgraph = new DefaultDirectedGraph<ActivityNode, DefaultEdge>(DefaultEdge.class);
    }

    public void addVertex(ActivityNode activityNode){
        if(!atgraph.containsVertex(activityNode))
            atgraph.addVertex(activityNode);
    }

    public void addEdge(ActivityNode source,ActivityNode sink){
        if(!atgraph.containsVertex(source)){
            atgraph.addVertex(source);
        }
        if(!atgraph.containsVertex(sink)){
            atgraph.addVertex(sink);
        }
        atgraph.addEdge(source,sink);
    }

    public boolean isReachable(ActivityNode source,ActivityNode node){
        DijkstraShortestPath<ActivityNode, DefaultEdge> dijkstraAlg =
                new DijkstraShortestPath<>(atgraph);

        GraphPath path = dijkstraAlg.getPath(source,node);
        if(path==null){
            return false;
        }else{
            return true;
        }
    }


    public boolean isStrongConnected(ActivityNode source,ActivityNode node){
        DijkstraShortestPath<ActivityNode, DefaultEdge> dijkstraAlg =
                new DijkstraShortestPath<>(atgraph);

        GraphPath path = dijkstraAlg.getPath(source,node);
        if(path!=null){
            if(dijkstraAlg.getPath(node,source)!=null){
                return true;
            }else{
                return false;
            }
        }else {
            return false;
        }
    }





}
