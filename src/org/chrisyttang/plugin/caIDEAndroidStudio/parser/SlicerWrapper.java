package org.chrisyttang.plugin.caIDEAndroidStudio.parser;

import com.intellij.analysis.AnalysisScope;
import com.intellij.openapi.components.ServiceManager;
import com.intellij.openapi.project.Project;
import com.intellij.psi.PsiElement;
import com.intellij.slicer.SliceAnalysisParams;
import com.intellij.slicer.SliceManager;
import com.intellij.slicer.SliceUsage;
import com.intellij.util.Processor;
import org.jetbrains.annotations.NotNull;

public class SlicerWrapper {

    private SliceManager _sliceManager;
    private static SlicerWrapper instance;
    private Project _project;
    private SliceAnalysisParams _sliceAnalysisParams = new SliceAnalysisParams();
    private SliceUsage _sliceUsage;

    private SlicerWrapper(Project pproject){
        this._project = pproject;
        this._sliceManager = ServiceManager.getService(_project,SliceManager.class);
        this._sliceAnalysisParams.scope = new AnalysisScope(this._project);
    }

    public static SlicerWrapper getInstance(Project pproject){
        if(instance==null){
            instance = new SlicerWrapper(pproject);
        }
        return instance;
    }

    public void doSlice(PsiElement psiElement, boolean dataFlowToThis){
        _sliceAnalysisParams.dataFlowToThis = dataFlowToThis;

    }


}
