package org.chrisyttang.plugin.caIDEAndroidStudio.parser;

import com.intellij.openapi.diagnostic.Logger;
import com.intellij.openapi.module.Module;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiFile;
import com.intellij.psi.search.FilenameIndex;
import com.intellij.psi.search.GlobalSearchScope;
import org.chrisyttang.androidmanifest.dom.*;
import org.chrisyttang.plugin.caIDEAndroidStudio.util.VfsFileUtil;

import javax.swing.*;
import java.io.File;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static org.chrisyttang.androidmanifest.dom.AndroidManifestNode.NodeType.Activity;

public class AndroidManifestParserWrapper {

    private static AndroidManifestParserWrapper instance;
    private Project _project;
    public static final Logger LOG = Logger.getInstance("org.chrisyttang.plugin.caIDEAndroidStudio.parser.AndroidManifestParser");
    private VirtualFile _androidManifestVirtualFile;
    private File _androidManifestFile;
    private AndroidManifestParser _parser;

    private List<AndroidManifestNode> permissions = new LinkedList<AndroidManifestNode>();
    private List<ActivityNode> activities = new LinkedList<ActivityNode>();
    private List<IntentFilterNode> intentFilterNodes = new LinkedList<IntentFilterNode>();
    private Map<IntentFilterNode,ActivityNode> intentFilterNodeActivityNodeMap = new HashMap<IntentFilterNode,ActivityNode>();
    private Module _selectedModule;

    private List<AndroidManifestNode> roots;
    private String packagename;
    private String minSDKVersion="";
    private String maxSDKVersion = "";
    private String _modulePath = "";

    private AndroidManifestParserWrapper(Project pproject,Module pmodule){
        this._project = pproject;
        this._selectedModule = pmodule;
        this._modulePath = this._selectedModule.getModuleFilePath().substring(0,this._selectedModule.getModuleFilePath().lastIndexOf("/"));
        parse();
    }

    public static AndroidManifestParserWrapper getInstant(Project pproject,Module pmodule){
        if (instance==null){
            instance = new AndroidManifestParserWrapper(pproject,pmodule);
        }
        return instance;
    }

    protected void parse(){
        if(!VfsFileUtil.FileExist(this._project,"AndroidManifest.xml")){
            LOG.error("Cannot find the AndroidManifest.xml file in the target project!");
            //System.exit(-1);
        }

        if(FilenameIndex.getFilesByName(this._project,"AndroidManifest.xml", GlobalSearchScope.allScope(this._project)).length>=1){
            PsiFile[]targetAndroidManifests = FilenameIndex.getFilesByName(this._project,"AndroidManifest.xml", GlobalSearchScope.allScope(this._project));

            for(PsiFile androidmenifest:targetAndroidManifests){
                String filePath = androidmenifest.getVirtualFile().getPath();
                if(filePath.contains(this._modulePath.substring(0,this._modulePath.lastIndexOf("/")))){
                    this._androidManifestVirtualFile =  androidmenifest.getVirtualFile();
                    break;
                }

            }

        }else
            this._androidManifestVirtualFile =  VfsFileUtil.getFile(this._project,"AndroidManifest.xml");

        this._androidManifestFile = new File(this._androidManifestVirtualFile.getPath());

        this._parser = new AndroidManifestParser(this._androidManifestFile);

        this.roots = this._parser.getRoots();


        for(AndroidManifestNode node:this.roots){
            processAndroidManifestNode(node);
        }

        for(IntentFilterNode intentFilterNode:this.intentFilterNodes){
            AndroidManifestNode parentNode = intentFilterNode.getParent();
            // here, we only obtain the intent filter for activity
            if(parentNode.getNodeType().equals(Activity)){
                this.intentFilterNodeActivityNodeMap.put(intentFilterNode,(ActivityNode) parentNode);
            }
        }

    }


    protected void processAndroidManifestNode(AndroidManifestNode root){

        // check current node
        switch(root.getNodeType()){

            case Action:{
                break;
            }
            case Activity:{//launchmode, scren orientation, name,
                ActivityNode activityNode = (ActivityNode)root;
                activities.add(activityNode);
                break;
            }
            case ActivityAlias:{
                break;

            }
            case Application:{
                break;
            }
            case Category:{
                break;
            }
            case Data:{
                break;
            }
            case GrantUriPermission:{
                break;
            }
            case Instrumentation:{
                break;
            }
            case IntentFilter:{
                IntentFilterNode intentFilterNode = (IntentFilterNode)root;
                intentFilterNodes.add(intentFilterNode);
                break;
            }
            case Manifest:{
                ManifestNode manifestNode = (ManifestNode)root;
                this.packagename = manifestNode.getPackageName();
                break;
            }
            case MetaData:{
                break;
            }
            case Permission:{
                PermissionNode permissionNode = (PermissionNode)root;
                this.permissions.add(permissionNode);
                break;
            }
            case PermissionGroup:{
                break;
            }
            case PermissionTree:{

                break;
            }
            case Provider:{
                break;
            }
            case Receiver:{
                break;
            }
            case Service:{
                break;
            }
            case UsesLibrary:{
                break;
            }
            case UsesPermission:{
                UsesPermissionNode usesPermissionNode = (UsesPermissionNode)root;
                this.permissions.add(usesPermissionNode);// processed in properties skip
                break;
            }
            case UsesSdk:{
                UsesSDKNode usesSDKNode = (UsesSDKNode)root;
                maxSDKVersion = usesSDKNode.getPropMaxSdkVersion();
                minSDKVersion = usesSDKNode.getPropMinSdkVersion();
                break;
            }
            case Comment:{
                break;
            }
            case Unknown:{
                break;
            }
            case UsesFeature:{
                break;
            }

        }

        AndroidManifestNode[] children = root.getChildren();
        if(children!=null){
            if(children.length>0){
                for(AndroidManifestNode child:children){
                    processAndroidManifestNode(child);
                }
            }
        }
        AndroidManifestNode[] unkownChildren = root.getUnkownChildren();
        if(unkownChildren!=null){
            if(unkownChildren.length>0){
                for(AndroidManifestNode child:unkownChildren){
                    processAndroidManifestNode(child);
                }
            }
        }
    }

    /** getters **/
    public String getPackagename(){
        return packagename;
    }

    public List<ActivityNode> getAllActivities(){
        return activities;
    }

    public Map<IntentFilterNode, ActivityNode> getIntentFilterNodeActivityNodeMap() {
        return intentFilterNodeActivityNodeMap;
    }


    public List<AndroidManifestNode> getAllPermission() {
        return permissions;
    }
}
