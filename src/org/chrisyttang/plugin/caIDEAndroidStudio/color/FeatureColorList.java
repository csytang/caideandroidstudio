package org.chrisyttang.plugin.caIDEAndroidStudio.color;

import com.intellij.openapi.command.WriteCommandAction;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.roots.ProjectRootManager;
import com.intellij.openapi.vfs.LocalFileSystem;
import com.intellij.openapi.vfs.VfsUtil;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.search.FilenameIndex;
import com.intellij.psi.search.GlobalSearchScope;
import org.chrisyttang.plugin.caIDEAndroidStudio.featuremodel.FeatureModel;
import org.chrisyttang.plugin.caIDEAndroidStudio.features.Feature;
import org.chrisyttang.plugin.caIDEAndroidStudio.util.JsonHelper;
import org.chrisyttang.plugin.caIDEAndroidStudio.util.RunnableHelper;
import org.chrisyttang.plugin.caIDEAndroidStudio.util.VfsFileUtil;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class FeatureColorList {

    private static FeatureColorList instance = null;
    private static Map<Feature,Color> featureToColor = new HashMap<Feature,Color>();
    private Project _project;
    private static VirtualFile _colorFile = null;

    /**
     * this function will initialize the feature-color map
     * @param project target project
     */
    private FeatureColorList(Project project){
        this._project = project;
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                loadFromColoList();
            }
        };
        RunnableHelper.runWriteCommand(project,runnable);
    }

    /**
     * this function will initialize the feature color list from color.json file in the file system
     */
    public void loadFromColoList(){
        String readstr = "";
        if(isColorFileExists()){
            VirtualFile _colorFile = FilenameIndex.getFilesByName(_project,"color.json", GlobalSearchScope.allScope(_project))[0].getVirtualFile();
            try {
                byte[] content = _colorFile.contentsToByteArray();
                readstr = new String(content);
            } catch (IOException e) {
                e.printStackTrace();
            }

            Map<String, String> featuretocolorStr = JsonHelper.JsonToString(readstr);

            // get the feature model
            FeatureModel featureModel = FeatureModel.getInstance(_project);
            Collection<Feature> allfeatures =  featureModel.getFeatures();

            // set up the feature list from the color.json and feature model file(featuremodel.afm)
            for(Feature feature:allfeatures){
                String featureName = feature.getName();
                if(featuretocolorStr.containsKey(featureName)){
                    String strcolor = featuretocolorStr.get(featureName);
                    Color color = ColorUtil.HexToColor(strcolor);
                    featureToColor.put(feature, color);
                }
            }
        }
    }

    public static Color getRandomColor(){
        Random rand = new Random();
        // Java 'Color' class takes 3 floats, from 0 to 1.
        float r = rand.nextFloat();
        float g = rand.nextFloat();
        float b = rand.nextFloat();
        Color randomColor = new Color(r, g, b);
        while(featureToColor.values().contains(randomColor)){
            rand = new Random();
            // Java 'Color' class takes 3 floats, from 0 to 1.
            r = rand.nextFloat();
            g = rand.nextFloat();
            b = rand.nextFloat();
            randomColor = new Color(r, g, b);
        }
        return randomColor;
    }

    /**
     * this action will allow developer set the project for feature color list
     * @param project
     */
    public static void SetProject(Project project){
        if (instance==null){
            instance = new FeatureColorList(project);
        }
    }

    public static FeatureColorList getInstance(Project project){
        if (instance==null){
            instance = new FeatureColorList(project);
        }
        return instance;
    }

    public static FeatureColorList getInstance(){
        return instance;
    }

    /**
     * set the color to the feature
     * @param feature
     * @param color the color of the feature
     */
    public void updateColor(Feature feature,Color color){
        featureToColor.put(feature,color);
        synchronizeFeatureColorList();
    }


    public void renameFeature(){
        synchronizeFeatureColorList();
    }

    /**
     *
     */
    private void synchronizeFeatureColorList(){

        // refresh the color.json file
        Runnable refreshcolorjsonRunnable = new Runnable() {
            @Override
            public void run() {
                try {

                    ProjectRootManager _projectManager = ProjectRootManager.getInstance(_project);

                    if(isColorFileExists()){
                        _colorFile = FilenameIndex.getFilesByName(_project,"color.json", GlobalSearchScope.allScope(_project))[0].getVirtualFile();
                    }else{

                        // get selected module
                        // _colorFile = _projectManager.getContentRoots()[0].createChildData(null,"color.json");
                        _colorFile = LocalFileSystem.getInstance().createChildFile(this,_project.getBaseDir(),"color.json");
                    }

                    VfsUtil.saveText(_colorFile, JsonHelper.StringMapToJson(FeatureColortoStringMap()));

                }catch (IOException e1) {
                    e1.printStackTrace();
                }

            }
        };

        WriteCommandAction.runWriteCommandAction(_project, refreshcolorjsonRunnable);
    }

    /**
     *
     * @return map from feature name to color(hex)
     */
    public Map<String,String> FeatureColortoStringMap(){
        Map<String,String> strfeaturelist = new HashMap<String,String>();

        for(Map.Entry<Feature,Color>entry:this.featureToColor.entrySet()){
            Feature feature = entry.getKey();
            Color color = entry.getValue();
            strfeaturelist.put(feature.getName(),ColorUtil.ColorToHex(color));
        }

        return strfeaturelist;
    }



    /**
     *
     * @return whether the feature model file exists in the project repository
     */
    public boolean isColorFileExists(){
        return FilenameIndex.getFilesByName(_project,"color.json", GlobalSearchScope.allScope(_project))!=null;

    }


    public Color getColor(Feature node) {
        if(featureToColor.containsKey(node)){
            return featureToColor.get(node);
        }else{
            return null;
        }
    }

    public void removeFeature(Feature feature) {
        featureToColor.remove(feature);
        synchronizeFeatureColorList();
    }
}
