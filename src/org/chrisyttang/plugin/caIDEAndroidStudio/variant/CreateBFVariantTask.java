package org.chrisyttang.plugin.caIDEAndroidStudio.variant;

import com.android.tools.idea.gradle.parser.GradleBuildFile;
import com.android.tools.idea.gradle.util.GradleUtil;
import com.intellij.openapi.command.WriteCommandAction;
import com.intellij.openapi.diagnostic.Logger;
import com.intellij.openapi.progress.PerformInBackgroundOption;
import com.intellij.openapi.progress.ProgressIndicator;
import com.intellij.openapi.progress.Task;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.project.ProjectManager;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiDocumentManager;
import com.intellij.psi.impl.PsiDocumentManagerBase;
import com.intellij.psi.search.FilenameIndex;
import com.intellij.psi.search.GlobalSearchScope;
import org.chrisyttang.androidmanifest.dom.ActivityNode;
import org.chrisyttang.androidmanifest.dom.AndroidManifestNode;
import org.chrisyttang.androidmanifest.dom.AndroidManifestParser;
import org.chrisyttang.androidmanifest.dom.ManifestNode;
import org.chrisyttang.plugin.caIDEAndroidStudio.instantapp.Module;
import org.chrisyttang.plugin.caIDEAndroidStudio.instantapp.configuration.InstantAppConfiguration;
import org.chrisyttang.plugin.caIDEAndroidStudio.instantapp.installed.InstalledAppModule;
import org.chrisyttang.plugin.caIDEAndroidStudio.instantapp.instant.BaseModule;
import org.chrisyttang.plugin.caIDEAndroidStudio.instantapp.instant.FeatureModule;
import org.chrisyttang.plugin.caIDEAndroidStudio.instantapp.instant.InstantAppModule;
import org.chrisyttang.plugin.caIDEAndroidStudio.parser.AndroidManifestParserWrapper;
import org.chrisyttang.plugin.caIDEAndroidStudio.recommendation.IRecommendation;
import org.chrisyttang.plugin.caIDEAndroidStudio.util.AndroidResourceUtils;
import org.chrisyttang.plugin.caIDEAndroidStudio.util.CommonUtils;
import org.chrisyttang.plugin.caIDEAndroidStudio.util.DialogUtil;
import org.chrisyttang.plugin.caIDEAndroidStudio.util.PsiFileUtil;
import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;

import java.util.*;

public class CreateBFVariantTask extends Task.Backgroundable {

    private InstantAppConfiguration _instantAppConfiguration;
    private List<String> modules;

    private String _basePath;
    private String _pathSeperate;

    private Project _project;
    private String _projectName;
    private String _targetVariantName;
    private String _targetVariantPath;
    private String _basePackgeName = "";

    private List<ActivityNode> _baseActivityNodes;
    private List<ActivityNode> _featureActivityNodes;
    private List<ActivityNode> _allActivityNodes;
    private Map<ActivityNode,String> _activityNodePackageNameMap = new HashMap<ActivityNode,String>();
    private Map<ActivityNode,String> _activityNodeHostMap = new HashMap<ActivityNode,String>();
    private Map<ActivityNode,String> _activityModuleNameMapping = new HashMap<ActivityNode,String>();
    private List<Module> _allmodules = new LinkedList<>();
    private List<String> _featuremoduleNames = new LinkedList<String>();
    private com.intellij.openapi.module.Module _selectedModule;
    private VirtualFile _projectAndroidManifestFile;
    private Collection<File> _projectAssets;
    private Collection<File> _projectRes;
    private GradleBuildFile _gradleBuildFile;
    private Map<ActivityNode,VirtualFile> _activityNodeVirtualFileMap = new HashMap<>();
    private AndroidManifestParser androidManifestParser;

    public static final Logger LOG = Logger.getInstance("org.chrisyttang.plugin.caIDEAndroidStudio.variant.CreateBFVariantTask");

    private Project _targetVariant = null;
    private IRecommendation _recommendation;

    public CreateBFVariantTask(@Nullable Project project, @Nls @NotNull String title,
                               boolean canBeCancelled, @Nullable PerformInBackgroundOption backgroundOption,
                               InstantAppConfiguration pinstantAppConfiguration,List<ActivityNode> pallActivityNodes, com.intellij.openapi.module.Module pselectedModule) {
        super(project, title, canBeCancelled, backgroundOption);

        this._project = project;
        this._instantAppConfiguration = pinstantAppConfiguration;
        this._selectedModule = pselectedModule;
        this._allActivityNodes = pallActivityNodes;

        _basePath = _project.getBasePath();
        _pathSeperate = File.separator;
        _projectName = _project.getName();

        _targetVariantName = _projectName+"_featuremodel_variant";
        _targetVariantPath = _basePath.substring(0,_basePath.lastIndexOf(_pathSeperate))+_pathSeperate+_targetVariantName;

    }

    public CreateBFVariantTask(@Nullable Project project,
                               @Nls @NotNull String title,
                               boolean canBeCancelled,
                               @Nullable PerformInBackgroundOption backgroundOption,
                               InstantAppConfiguration pinstantAppConfiguration,
                               List<ActivityNode> pallActivityNodes,
                               com.intellij.openapi.module.Module pselectedModule,
                               IRecommendation precommendation) {
        super(project, title, canBeCancelled, backgroundOption);
        this._project = project;
        this._instantAppConfiguration = pinstantAppConfiguration;
        this._selectedModule = pselectedModule;
        this._allActivityNodes = pallActivityNodes;

        _basePath = _project.getBasePath();
        _pathSeperate = File.separator;
        _projectName = _project.getName();

        _targetVariantName = _projectName+precommendation.getRecommendationName().toString();
        _targetVariantPath = _basePath.substring(0,_basePath.lastIndexOf(_pathSeperate))+_pathSeperate+_targetVariantName;
        _recommendation = precommendation;
    }


    @Override
    public void run(@NotNull ProgressIndicator progressIndicator) {
        File _targetVariantDir = new File(_targetVariantPath);
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                // 0. Create the variant project directory
                if(!_targetVariantDir.exists()) {
                    _targetVariantDir.mkdir();
                }

                _targetVariant = ProjectManager.getInstance().createProject(_targetVariantName,_targetVariantDir.getPath());
                createInstantAppVariant();
            }

        };
        WriteCommandAction.runWriteCommandAction(_project, runnable);


    }

    /**
     * this action will enable create variant project based on feature model
     */
    private void createInstantAppVariant(){
          // 1. project analysis
        // 1.1 locate the AndroidManfest.xml
        // 1.2 locate the src/res directory
        // 1.3 locate the build.gradle (if it is build with gradle)
        // 1.4 locate all Activity
        // 1.5 find all fragments with each Activity
        // 1.6 find all views with each Activity

        // ======================1.1===============================


        GlobalSearchScope moduleContentScope = _selectedModule.getModuleContentScope();
        List<VirtualFile> androidManifestList = new LinkedList<>(FilenameIndex.getVirtualFilesByName(_project,"AndroidManifest.xml",true,moduleContentScope));

        _projectAndroidManifestFile = androidManifestList.get(0);

        // =====================1.2===============================
        AndroidResourceUtils.setProject(_project);
        _projectRes = AndroidResourceUtils.getAndroidResFolders(_selectedModule);


        _projectAssets = AndroidResourceUtils.getAndroidAssetsFolders(_selectedModule);

        // remove all not in the module


        // ======================1.3===============================

        final VirtualFile buildFile = GradleUtil.getGradleBuildFile(_selectedModule);
        _gradleBuildFile = new GradleBuildFile(buildFile,_project);

        // ======================1.4==============================
        for(ActivityNode activityNode:this._allActivityNodes){
            String[] fullNames = activityNode.getName().split("\\.");
            String activityName = fullNames[fullNames.length-1];
            if(FilenameIndex.getFilesByName(_project,activityName+".java",GlobalSearchScope.moduleScope(_selectedModule))==null||
                    FilenameIndex.getFilesByName(_project,activityName+".java",GlobalSearchScope.moduleScope(_selectedModule)).length==0){
                LOG.error("Cannot find the activity: "+activityName+" in the project!!!");
                System.exit(-1);
            }
            VirtualFile activityFile = FilenameIndex.getFilesByName(_project,activityName+".java",GlobalSearchScope.moduleScope(_selectedModule))[0].getVirtualFile();
            _activityNodeVirtualFileMap.put(activityNode,activityFile);
        }

        _basePackgeName = AndroidManifestParserWrapper.getInstant(_project,_selectedModule).getPackagename();

        // ==========1.5======================





        // =========1.6=====================


        // 2. mark all unspecial files and clone them directly



        // 3. We get number of modules and module names
        modules = _instantAppConfiguration.getModules();
        _baseActivityNodes = _instantAppConfiguration.getBaseModuleActivity();
        _featureActivityNodes = _instantAppConfiguration.getFeatureModuleActivities();
        _activityNodePackageNameMap = _instantAppConfiguration.getActivityNodePackgeNameMapping();
        _activityModuleNameMapping = _instantAppConfiguration.getActivityNodeModuleNameMapping();
        _activityNodeHostMap = _instantAppConfiguration.getActivityNodeURLMapping();

        // 4. Create each module;
        BaseModule baseModule = new BaseModule(_project,_targetVariantPath,_baseActivityNodes,_basePackgeName,
                "base");
        this._allmodules.add(baseModule);

        for(ActivityNode activityNode:this._featureActivityNodes){
            FeatureModule featureModule = new FeatureModule(_project,_targetVariantPath,_featureActivityNodes,_activityNodePackageNameMap.get(activityNode),
                    _activityModuleNameMapping.get(activityNode));
            this._allmodules.add(featureModule);
            this._featuremoduleNames.add(featureModule._moduleName);
        }


        InstalledAppModule installedAppModule = new InstalledAppModule(_project,_targetVariantPath,_basePackgeName);

        this._allmodules.add(installedAppModule);

        InstantAppModule instantAppModule = new InstantAppModule(_project,_targetVariantPath,_basePackgeName);
        this._allmodules.add(instantAppModule);


        androidManifestParser = new AndroidManifestParser(new File(_projectAndroidManifestFile.getPath()));
        ManifestNode manifestNode = null;
        for(AndroidManifestNode root:androidManifestParser.getRoots()){
            if(root instanceof ManifestNode){
                manifestNode = (ManifestNode)root;
            }
        }

        // 5. Create build.gradle in each module;
        for(Module module:this._allmodules){
            module.initModule(_gradleBuildFile,manifestNode,_projectAssets,_projectRes,_featuremoduleNames);
        }


        // 6. In each module, build the src folder and copy the files;
        VirtualFile[] srcRoots = CommonUtils.getSrcRoot(_selectedModule);
        List<VirtualFile> featureActivityFiles = new LinkedList<VirtualFile>();
        for(ActivityNode featureActivity:_featureActivityNodes){
            featureActivityFiles.add(_activityNodeVirtualFileMap.get(featureActivity));
        }

        for(Module module:this._allmodules){
            List<VirtualFile> activityVirtualFiles = new LinkedList<>();
            if(module instanceof InstantAppModule||
                    module instanceof InstalledAppModule){
                continue;
            }
            for(ActivityNode node:module.getActivities()){
                activityVirtualFiles.add(_activityNodeVirtualFileMap.get(node));
            }
            module.customizeCloneSourceCode(new LinkedList<VirtualFile>(Arrays.asList(srcRoots)),
                    activityVirtualFiles,
                    featureActivityFiles);
        }


        // 7. Copy all other files
        Set<VirtualFile> otherfiles = PsiFileUtil.exploreAllNonModuleFile(_project);
        for(VirtualFile file:otherfiles){
            // copy to base
            PsiFileUtil.cloneFile(file,_targetVariantPath+File.separator+"base",_project);
            // copyt to all feature modules

            for(Module module:this._allmodules){
                if(module instanceof FeatureModule){
                    FeatureModule featureModule = (FeatureModule)module;
                    String moduleName = featureModule.getModuleName();
                    PsiFileUtil.cloneFile(file,_targetVariantPath+File.separator+moduleName,_project);
                }
            }
        }

        // 8. finish popup dialog to show this finish
        // DialogUtil.createDialog("The base-feature instant app based APL is created!");

    }





}
