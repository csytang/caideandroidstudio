package org.chrisyttang.plugin.caIDEAndroidStudio.variant;

import com.intellij.lang.ASTNode;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.command.CommandProcessor;
import com.intellij.openapi.command.WriteCommandAction;
import com.intellij.openapi.diagnostic.Logger;
import com.intellij.openapi.progress.ProgressIndicator;
import com.intellij.openapi.progress.Task;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.project.ProjectManager;
import com.intellij.openapi.roots.ProjectFileIndex;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.openapi.vfs.VirtualFileManager;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;
import com.intellij.psi.PsiWhiteSpace;
import com.intellij.psi.util.PsiTreeUtil;
import org.chrisyttang.plugin.caIDEAndroidStudio.featuredcode.FeatureAnnotationCache;
import org.chrisyttang.plugin.caIDEAndroidStudio.featuredcode.FeaturedCodeDescriptor;
import org.chrisyttang.plugin.caIDEAndroidStudio.features.Feature;
import org.chrisyttang.plugin.caIDEAndroidStudio.util.FileUtil;
import org.chrisyttang.plugin.caIDEAndroidStudio.util.PsiFileUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class CreatingVariantTask extends Task.Backgroundable {

    private static final String TAG = CreatingVariantTask.class.getSimpleName();
    private static final Logger LOG = Logger.getInstance(TAG);
    private Project _project;
    private Project _targetVariant = null;

    private Set<Feature> _alldiabledFeature;
    private String _basePath;
    private String _pathSeperate;

    // this set contains all files in the source project
    private Set<VirtualFile> allFilesSrc = new HashSet<VirtualFile>();


    private String _projectName;
    private String _targetVariantName;
    private String _targetVariantPath;
    private int _selection;


    // feature annotation cache
    private FeatureAnnotationCache _cache = null;

    // annotation status
    private Map<PsiFile, Set<FeaturedCodeDescriptor>> _annotations = null;

    // project file system
    private ProjectFileIndex _srcprojectFileIndex;

    public CreatingVariantTask(@Nullable Project project, Set<Feature> alldiabledFeature) {
        super(project, "Creating instant app",true);
        this._project = project;
        this._alldiabledFeature = alldiabledFeature;
        _basePath = _project.getBasePath();
        _pathSeperate = File.separator;
        _projectName = _project.getName();
        // target project name
        _targetVariantName = _projectName+"_featuremodel_variant";
        _targetVariantPath = _basePath.substring(0,_basePath.lastIndexOf(_pathSeperate))+_pathSeperate+_targetVariantName;

        // initialize the annotation cache
        _cache = FeatureAnnotationCache.getInstance(project);

        _annotations =  _cache.getAnnotations();

    }

    @Override
    public void run(@NotNull ProgressIndicator progressIndicator) {
        File _targetVariantDir = new File(_targetVariantPath);
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                if(!_targetVariantDir.exists()) {
                    _targetVariantDir.mkdir();
                }

                _targetVariant = ProjectManager.getInstance().createProject(_targetVariantName,_targetVariantDir.getPath());

                creatingVariant();
            }
        };
        WriteCommandAction.runWriteCommandAction(_project, runnable);

    }

    /**
     * this action will enable create variant project
     */
    private void creatingVariant(){
        // for all psifile, create the corresponding psifile in the target project
        _srcprojectFileIndex = ProjectFileIndex.SERVICE.getInstance(_project);

        allFilesSrc.addAll(PsiFileUtil.exploreAllFileInProject(_project));


        // clone the source psifile to the target project
        for(VirtualFile sourcePsiFile:allFilesSrc){

            if(associatedWithDisabledFeatures(sourcePsiFile)){
                // the source psi file is associated with at least one disabled feature
                // to cope with the source psi file
                rewrite(sourcePsiFile,_project);
            }else{
                // the source psi file is not associated with any disabled feature
                PsiFileUtil.cloneFile(sourcePsiFile,_targetVariantPath,_project);
            }

        }

    }

    /**
     * rewrite the file and output to the target project
     * @param source_File source file to be rewritten
     */
    private void rewrite(VirtualFile source_File, Project source_Project){
        // 1. get the source path of sourcefile
        String source_FilePath = source_File.getPath();

        // 2. build the target file path of target file
        String source_projectPath = source_Project.getBasePath();
        String source_FileRelativePath = source_FilePath.substring(source_projectPath.length(),source_FilePath.length());

        // 3. check whether the parent directory is created
        String target_FilePath = _targetVariantPath+source_FileRelativePath;

        // 4. check whether the target file exists
        File target_File = new File(target_FilePath);

        // 4.1 already created
        if(target_File.exists()){
            return;
        }

        // 4.2 not created, get its parents
        VirtualFile source_ParentDir = source_File.getParent();

        // source directory path
        String source_DirPath = source_ParentDir.getPath();
        String source_DirRelativePath = source_DirPath.substring(source_projectPath.length(),source_DirPath.length());

        // the relative file path of its parent
        String target_ParentDirPath = _targetVariantPath+source_DirRelativePath;

        File parentDir = new File(target_ParentDirPath);
        if(!parentDir.exists()){
            // 4.3 create the target folder first
            PsiFileUtil.cloneFile(source_ParentDir, _targetVariantPath, source_Project);
        }


        // 4.4 rewrite the PsiFile and redirect to target file
        PsiFile sourcePsiFile = PsiFileUtil.VirtualFileToPsiFileConverter(source_File,source_Project);

        // 4.5 delete code fragments with disabled features
        assert _annotations.containsKey(sourcePsiFile);
        Set<FeaturedCodeDescriptor> descriptorSet = _annotations.get(sourcePsiFile);

        ASTNode fileASTNode = sourcePsiFile.getNode();
        String backupContent = fileASTNode.getChars().toString();
        VirtualFileManager fm = VirtualFileManager.getInstance();

        for(FeaturedCodeDescriptor descriptor:descriptorSet){
            Feature feature = descriptor.getFeature();
            if(_alldiabledFeature.contains(feature)){
                // only process the current feature is in the disabled feature set
                PsiElement unabledelement = descriptor.getPsiElement();
                fileASTNode.removeChild(unabledelement.getNode());
            }
        }

        String rewritedFileContent = fileASTNode.getChars().toString();

        // 4.6 rewrite the copyedSourcePsiElement to file

        FileUtil.writeFile(target_File,rewritedFileContent);



        Runnable filereserve = new Runnable() {
            @Override
            public void run() {
                try {
                    source_File.setBinaryContent(backupContent.getBytes());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };


        ApplicationManager.getApplication().invokeLater(new Runnable() {
            @Override
            public void run() {
                CommandProcessor.getInstance().executeCommand(_project, new Runnable() {
                    @Override
                    public void run() {
                        ApplicationManager.getApplication().runWriteAction(filereserve);
                    }
                }, "content recovery from backup", null);
            }
        });


    }

    private static PsiElement findElementInRange(@NotNull PsiElement psiElement,
                                                 int startOffset,
                                                 int endOffset) {
        PsiElement element1 = psiElement.findElementAt(startOffset);
        PsiElement element2 = psiElement.findElementAt(endOffset - 1);
        if (element1 instanceof PsiWhiteSpace) {
            startOffset = element1.getTextRange().getEndOffset();
            element1 = psiElement.findElementAt(startOffset);
        }
        if (element2 instanceof PsiWhiteSpace) {
            endOffset = element2.getTextRange().getStartOffset();
            element2 = psiElement.findElementAt(endOffset - 1);
        }
        if (element2 == null || element1 == null) return null;
        final PsiElement element = PsiTreeUtil.findCommonParent(element1, element2);

        if (element == null || element.getTextRange().getStartOffset() != startOffset || element.getTextRange().getEndOffset() != endOffset) {
            return null;
        }
        return element;
    }

    /**
     *
     * @param virtualFile source java file
     * @return
     */
    private boolean associatedWithDisabledFeatures(VirtualFile virtualFile) {
        // if it is not a java file, then it will not associated with disabled feature
        if(!virtualFile.getFileType().getName().toLowerCase().equals("java")){
            return false;
        }

        // if there is not a feature related to the file, it cannot be associated with disabled feature
        PsiFile sourcePsiFile = PsiFileUtil.VirtualFileToPsiFileConverter(virtualFile,_project);
        if(sourcePsiFile==null){
            return false;
        }

        if(!_annotations.containsKey(sourcePsiFile)){
            return false;
        }

        Set<FeaturedCodeDescriptor> featuredCodeDescriptorSet = _annotations.get(sourcePsiFile);

        // iterate the featured code descriptor
        for(FeaturedCodeDescriptor descriptor:featuredCodeDescriptorSet){
            Feature feature = descriptor.getFeature();
            if(_alldiabledFeature.contains(feature)){
                return true;
            }
        }

        return false;
    }



}
