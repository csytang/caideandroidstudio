package org.chrisyttang.plugin.caIDEAndroidStudio.recommendation.recommendationconfig;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.DialogWrapper;
import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;
import com.intellij.uiDesigner.core.Spacer;
import org.chrisyttang.androidmanifest.dom.ActivityNode;
import org.chrisyttang.plugin.caIDEAndroidStudio.instantapp.ModuleType;
import org.chrisyttang.plugin.caIDEAndroidStudio.instantapp.configuration.InstantAppConfiguration;
import org.chrisyttang.plugin.caIDEAndroidStudio.instantapp.configuration.InstantAppLinkAssistant;
import org.chrisyttang.plugin.caIDEAndroidStudio.instantapp.configuration.InstantAppModuleNameConfiguration;
import org.chrisyttang.plugin.caIDEAndroidStudio.instantapp.configuration.InstantAppPackageNameSetup;
import org.chrisyttang.plugin.caIDEAndroidStudio.recommendation.ActivityGroup;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;
import java.util.List;

public class RecommendationBaseIAConfigurationForm extends DialogWrapper {
    private JPanel rootPanel;
    private JPanel displayPanel;
    private JPanel controlPanel;
    private JButton backButton;
    private JButton nextButton;
    private JLabel hintLabel;
    private JComboBox activityListJComboBox;
    private JRadioButton Base;
    private JRadioButton Feature;
    private JButton add;
    private JPanel activitySelectionPanel;
    private JList baseActivityList;
    private JList featureActivityList;

    private Project _project;
    private List<Object> _solution;
    private Map<ActivityNode, ActivityGroup> _activityNodeToGroup;
    private InstantAppConfiguration instantAppConfiguration;

    private int currentPageIndex = 0;
    private InstantAppLinkAssistant instantAppLinkAssistantPage = null;
    private InstantAppPackageNameSetup instantAppPackageNameSetup = null;
    private InstantAppModuleNameConfiguration instantAppModuleNameConfiguration = null;

    // name of activity group in feature module
    private List<String> featurebindActivityGroups = new LinkedList<>();
    // name of activity group in base module
    private List<String> basemodulebindActivityGroup = new LinkedList<>();

    private int MAXPAGEINDEX = 3;

    private List<ActivityNode> baseModuleActivity = new LinkedList<>();
    private List<ActivityNode> featureModuleActivities = new LinkedList<>();

    // list of activities in list
    private DefaultListModel baseActivityUIListModel = new DefaultListModel();
    private DefaultListModel featureActivitiesUIListModel = new DefaultListModel();


    private String _packageName;
    private List<String> activitygroups = new LinkedList<>();
    private Map<String, ActivityGroup> stringActivityGroupMap = new HashMap<>();
    private List<ActivityNode> _allactivities = new LinkedList<>();
    private Set<ActivityNode> _activitiesInGroup = new HashSet<ActivityNode>();
    private ModuleType moduleType;
    private Map<String, ActivityNode> nameToActivityNode = new HashMap<>();

    public RecommendationBaseIAConfigurationForm(Project project, String ppackageName, List<Object> psolution, Map<ActivityNode, ActivityGroup> pactivityNodeToGroup) {
        super(project);

        this._project = project;
        this._packageName = ppackageName;
        this._solution = psolution;
        this._activityNodeToGroup = pactivityNodeToGroup;
        this._allactivities = new LinkedList<>(pactivityNodeToGroup.keySet());
        for (int i = 0; i < this._allactivities.size(); i++) {
            nameToActivityNode.put(this._allactivities.get(i).getName(), this._allactivities.get(i));
        }

        $$$setupUI$$$();
        setResizable(true);

        init();

        for (Object obj : this._solution) {
            ActivityGroup activityGroup = (ActivityGroup) obj;
            String activityGroup_Content = activityGroup.toString();
            activitygroups.add(activityGroup_Content);
            stringActivityGroupMap.put(activityGroup_Content, activityGroup);

            // init the combobox with the activity groups
            activityListJComboBox.addItem(activityGroup_Content);

            _activitiesInGroup.addAll(activityGroup.getActivities());
        }

        for (ActivityNode activityNode : _allactivities) {
            if (_activitiesInGroup.contains(activityNode)) {
                continue;
            }

            // add activity to base module
            baseActivityUIListModel.add(baseActivityUIListModel.size(), activityNode.getName());
            baseModuleActivity.add(activityNode);
        }

        add.addActionListener(new ActionListener() {
            /**
             * Invoked when an action occurs.
             *
             * @param e
             */
            @Override
            public void actionPerformed(ActionEvent e) {

                int selectedIndex = activityListJComboBox.getSelectedIndex();
                String selectedactivityNodeName = activitygroups.get(selectedIndex);
                if (baseActivityUIListModel.contains(selectedactivityNodeName) ||
                        featureActivitiesUIListModel.contains(selectedactivityNodeName)) {
                    if (baseActivityUIListModel.contains(selectedactivityNodeName)) {
                        JOptionPane.showMessageDialog(null, "The activity group" + selectedactivityNodeName + " is already mapped to a base module", "Error", JOptionPane.INFORMATION_MESSAGE);
                    } else {
                        JOptionPane.showMessageDialog(null, "The activity group" + selectedactivityNodeName + " is already mapped to a feature module", "Error", JOptionPane.INFORMATION_MESSAGE);
                    }
                } else {
                    ActivityGroup selectedGroup = stringActivityGroupMap.get(selectedactivityNodeName);
                    if (moduleType == ModuleType.BASE) {
                        baseActivityUIListModel.add(baseActivityUIListModel.size(), selectedactivityNodeName);
                        baseModuleActivity.addAll(selectedGroup.getActivities());
                        for (ActivityNode featureactivityNode : selectedGroup.getActivities()) {
                            basemodulebindActivityGroup.add(featureactivityNode.getName());
                            featurebindActivityGroups.add(featureactivityNode.getName());
                        }
                    } else {
                        featureActivitiesUIListModel.add(featureActivitiesUIListModel.size(), selectedactivityNodeName);
                        featureModuleActivities.addAll(selectedGroup.getActivities());
                        for (ActivityNode featureactivityNode : selectedGroup.getActivities()) {
                            featurebindActivityGroups.add(featureactivityNode.getName());
                        }
                    }
                }

            }
        });


        createHint();

        nextButton.addActionListener(new ActionListener() {
            /**
             * Invoked when an action occurs.
             *
             * @param e
             */
            @Override
            public void actionPerformed(ActionEvent e) {
                if (currentPageIndex != MAXPAGEINDEX) {

                    currentPageIndex++;
                    if (currentPageIndex == MAXPAGEINDEX) {
                        nextButton.setEnabled(false);
                    } else {
                        nextButton.setEnabled(true);
                    }
                    if (!backButton.isEnabled()) {
                        backButton.setEnabled(true);
                    }
                    loadTargetPage();
                }

            }
        });


        backButton.addActionListener(new ActionListener() {
            /**
             * Invoked when an action occurs.
             *
             * @param e
             */
            @Override
            public void actionPerformed(ActionEvent e) {
                if (currentPageIndex != 0) {

                    currentPageIndex--;
                    if (currentPageIndex != 0) {
                        backButton.setEnabled(true);
                    } else {
                        backButton.setEnabled(false);
                    }
                    if (!nextButton.isEnabled()) {
                        nextButton.setEnabled(true);
                    }
                }
                loadTargetPage();

            }
        });

        ChangeListener listener = new ChangeListener() {
            /**
             * Invoked when the target of the listener has changed its state.
             *
             * @param e a ChangeEvent object
             */
            @Override
            public void stateChanged(ChangeEvent e) {
                AbstractButton button = (AbstractButton) e.getSource();
                String selected = button.getText();
                if (selected.equals("Feature")) {
                    //base module
                    moduleType = ModuleType.FEATURE;
                } else if (selected.equals("Base")) {
                    //feature module
                    moduleType = ModuleType.BASE;
                }
            }
        };


        Base.addChangeListener(listener);
        Feature.addChangeListener(listener);


    }


    private void createHint() {
        // hintLabel
        String labelContent = "";
        labelContent += "<html>";
        labelContent += "Other activities, include ";

        for (ActivityNode activityNode : _allactivities) {

            if (_activitiesInGroup.contains(activityNode)) {
                continue;
            }

            labelContent += activityNode.getName();
            labelContent += "<br>";

        }

        labelContent += "will be redirect to base module";
        labelContent += "<br>";

        labelContent += "</html>";
        hintLabel.setText(labelContent);
    }

    private void createUIComponents() {
        // create the JComboBox,
        rootPanel = new JPanel();
        displayPanel = new JPanel();
        activityListJComboBox = new JComboBox();
        activitySelectionPanel = new JPanel();
        hintLabel = new JLabel();
        controlPanel = new JPanel();
        baseActivityList = new JList(baseActivityUIListModel);
        featureActivityList = new JList(featureActivitiesUIListModel);

    }


    @Nullable
    @Override
    protected JComponent createCenterPanel() {
        return rootPanel;
    }


    /*
     * reserve the result to instantappconfiguration
     */
    @Override
    protected void doOKAction() {
        // save the result collected
        if (validChecking()) {
            // collect from all pages;
            // collect from page 0(instant app configuration pages)
            // save in the baseModuleActivity and featureModuleActivities
            instantAppConfiguration = new InstantAppConfiguration(this.baseModuleActivity, this.featureModuleActivities);

            // save the result in page 1;
            Map<String, String> activityURLMapping = instantAppLinkAssistantPage.getActivityURLMapping();
            for (Map.Entry<String, String> entry : activityURLMapping.entrySet()) {
                instantAppConfiguration.addActivityNodeURLMapping(this.nameToActivityNode.get(entry.getKey()), entry.getValue());
            }


            List<ActivityGroup> visitedactivityGroups = new LinkedList<>();
            // save the result in page 2;
            Map<String, String> activityPackgeMapping = instantAppPackageNameSetup.getActivityPackageMapping();
            for (Map.Entry<String, String> entry : activityPackgeMapping.entrySet()) {
                ActivityNode activityNode = this.nameToActivityNode.get(entry.getKey());
                ActivityGroup activityGroup = _activityNodeToGroup.get(activityNode);
                if (!visitedactivityGroups.contains(activityGroup)) {
                    visitedactivityGroups.add(activityGroup);
                    instantAppConfiguration.addActivityNodePackgeNameMapping(activityNode, entry.getValue());
                } else {
                    Set<ActivityNode> bindedActivities = activityGroup.getActivities();
                    for (ActivityNode relatedActivity : bindedActivities) {
                        if (!relatedActivity.equals(activityNode))
                            instantAppConfiguration.addActivityNodePackgeNameMapping(relatedActivity, entry.getValue());
                    }
                }
            }

            // 2. here is all activities not customized
            for (ActivityNode activityNode : _allactivities) {

                if (_activitiesInGroup.contains(activityNode)) {
                    continue;
                }

                instantAppConfiguration.addActivityNodePackgeNameMapping(activityNode, _packageName);
            }


            // clear all visited activities
            visitedactivityGroups.clear();

            // save the result in page 3;
            Map<String, String> activityModuleMapping = instantAppModuleNameConfiguration.getActivityModuleMapping();
            for (Map.Entry<String, String> entry : activityModuleMapping.entrySet()) {
                ActivityNode activityNode = this.nameToActivityNode.get(entry.getKey());
                ActivityGroup activityGroup = _activityNodeToGroup.get(activityNode);
                if (!visitedactivityGroups.contains(activityGroup)) {
                    visitedactivityGroups.add(activityGroup);
                    instantAppConfiguration.addActivityNodeModuleNameMapping(activityNode, entry.getValue());
                } else {
                    Set<ActivityNode> bindedActivities = activityGroup.getActivities();
                    for (ActivityNode relatedActivity : bindedActivities) {
                        if (!relatedActivity.equals(activityNode))
                            instantAppConfiguration.addActivityNodeModuleNameMapping(relatedActivity, entry.getValue());
                    }
                }
            }


            // 2. here is all activities not customized
            for (ActivityNode activityNode : _allactivities) {

                if (_activitiesInGroup.contains(activityNode)) {
                    continue;
                }

                instantAppConfiguration.addActivityNodeModuleNameMapping(activityNode, "base");
            }


            super.doOKAction();
        }

    }


    protected boolean validChecking() {
        if (currentPageIndex != 3) {
            return false;
        }
        return true;
    }

    public InstantAppConfiguration getInstantAppConfiguration() {
        return instantAppConfiguration;
    }

    /**
     * this will load the target page to the foreground
     */
    private void loadTargetPage() {
        displayPanel.removeAll();
        displayPanel.revalidate();
        displayPanel.repaint();
        switch (currentPageIndex) {
            case 0: {
                displayPanel.add(activitySelectionPanel, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
                break;
            }
            case 1: {
                instantAppLinkAssistantPage = new InstantAppLinkAssistant(_project, featurebindActivityGroups);
                displayPanel.add(instantAppLinkAssistantPage.$$$getRootComponent$$$(), new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
                break;
            }
            case 2: {
                instantAppPackageNameSetup = new InstantAppPackageNameSetup(_project, featurebindActivityGroups, _packageName, basemodulebindActivityGroup);
                displayPanel.add(instantAppPackageNameSetup.$$$getRootComponent$$$(), new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
                break;
            }
            case 3: {
                instantAppModuleNameConfiguration = new InstantAppModuleNameConfiguration(_project, featurebindActivityGroups, basemodulebindActivityGroup);
                displayPanel.add(instantAppModuleNameConfiguration.$$$getRootComponent$$$(), new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
                break;
            }
        }
        rootPanel.updateUI();

    }

    @Override
    public void doCancelAction() {
        super.doCancelAction();
    }


    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$() {
        createUIComponents();
        rootPanel.setLayout(new GridLayoutManager(3, 1, new Insets(0, 0, 0, 0), -1, -1));
        displayPanel.setLayout(new GridLayoutManager(2, 5, new Insets(0, 0, 0, 0), -1, -1));
        rootPanel.add(displayPanel, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        activitySelectionPanel.setLayout(new GridLayoutManager(2, 1, new Insets(0, 0, 0, 0), -1, -1));
        displayPanel.add(activitySelectionPanel, new GridConstraints(0, 0, 2, 5, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        final JPanel panel1 = new JPanel();
        panel1.setLayout(new GridLayoutManager(2, 2, new Insets(0, 0, 0, 0), -1, -1));
        activitySelectionPanel.add(panel1, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        final JLabel label1 = new JLabel();
        label1.setText("Activities in Base Module");
        panel1.add(label1, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label2 = new JLabel();
        label2.setText("Activities in Feature Module");
        panel1.add(label2, new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JScrollPane scrollPane1 = new JScrollPane();
        panel1.add(scrollPane1, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        scrollPane1.setViewportView(baseActivityList);
        final JScrollPane scrollPane2 = new JScrollPane();
        panel1.add(scrollPane2, new GridConstraints(1, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        scrollPane2.setViewportView(featureActivityList);
        final JPanel panel2 = new JPanel();
        panel2.setLayout(new GridLayoutManager(1, 5, new Insets(0, 0, 0, 0), -1, -1));
        activitySelectionPanel.add(panel2, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        panel2.add(activityListJComboBox, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        Base = new JRadioButton();
        Base.setText("Base");
        panel2.add(Base, new GridConstraints(0, 2, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        Feature = new JRadioButton();
        Feature.setText("Feature");
        panel2.add(Feature, new GridConstraints(0, 3, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        add = new JButton();
        add.setText("Add");
        panel2.add(add, new GridConstraints(0, 4, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final Spacer spacer1 = new Spacer();
        panel2.add(spacer1, new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, 1, null, null, null, 0, false));
        controlPanel.setLayout(new GridLayoutManager(1, 3, new Insets(0, 0, 0, 0), -1, -1));
        rootPanel.add(controlPanel, new GridConstraints(2, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        final Spacer spacer2 = new Spacer();
        controlPanel.add(spacer2, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, 1, null, null, null, 0, false));
        backButton = new JButton();
        backButton.setEnabled(false);
        backButton.setText("Back");
        controlPanel.add(backButton, new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        nextButton = new JButton();
        nextButton.setText("Next");
        controlPanel.add(nextButton, new GridConstraints(0, 2, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JScrollPane scrollPane3 = new JScrollPane();
        rootPanel.add(scrollPane3, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        hintLabel.setText("Hint");
        scrollPane3.setViewportView(hintLabel);
        ButtonGroup buttonGroup;
        buttonGroup = new ButtonGroup();
        buttonGroup.add(Base);
        buttonGroup.add(Feature);
    }

    /**
     * @noinspection ALL
     */
    public JComponent $$$getRootComponent$$$() {
        return rootPanel;
    }
}
