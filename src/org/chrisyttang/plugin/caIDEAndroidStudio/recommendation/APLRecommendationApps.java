package org.chrisyttang.plugin.caIDEAndroidStudio.recommendation;

public enum  APLRecommendationApps {
    ACTIVITY_TRANS,
    UI_DEP,
    UI_TRANS,
    FEATURE_MODEL,
    PERMISSION_DEP;

    /**
     * Returns the name of this enum constant, as contained in the
     * declaration.  This method may be overridden, though it typically
     * isn't necessary or desirable.  An enum type should override this
     * method when a more "programmer-friendly" string form exists.
     *
     * @return the name of this enum constant
     */
    @Override
    public String toString() {
        switch (this){
            case ACTIVITY_TRANS:{
                return "activity_trans";
            }
            case UI_DEP:{
                return "ui_dep";
            }
            case FEATURE_MODEL:{
                return "feature_model";
            }
            case PERMISSION_DEP:{
                return "permission_dep";
            }
        }
        return super.toString();
    }
}
