package org.chrisyttang.plugin.caIDEAndroidStudio.recommendation;

import com.intellij.openapi.progress.PerformInBackgroundOption;
import com.intellij.openapi.progress.ProgressManager;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.DialogWrapper;
import org.chrisyttang.androidmanifest.dom.ActivityNode;
import org.chrisyttang.plugin.caIDEAndroidStudio.instantapp.Module;
import org.chrisyttang.plugin.caIDEAndroidStudio.instantapp.configuration.InstantAppConfiguration;
import org.chrisyttang.plugin.caIDEAndroidStudio.parser.AndroidManifestParserWrapper;
import org.chrisyttang.plugin.caIDEAndroidStudio.recommendation.gui.RecommendationSolutionTempDialog;
import org.chrisyttang.plugin.caIDEAndroidStudio.recommendation.recommendationconfig.RecommendationBaseIAConfigurationForm;
import org.chrisyttang.plugin.caIDEAndroidStudio.util.RecommendationSolutionUtil;
import org.chrisyttang.plugin.caIDEAndroidStudio.variant.CreateBFVariantTask;

import javax.swing.*;
import java.util.*;

public abstract class IRecommendation {

    protected List<ActivityNode> _activityNodes;
    protected RecommendationSolutionTempDialog _solutiontempdialog = null;
    protected Map<String,List<Object>> _solutions;
    protected RecommendationBaseIAConfigurationForm _configurationForm;
    protected Project _project;
    protected com.intellij.openapi.module.Module _module;
    protected CreateBFVariantTask _createBFVariantTask;
    protected InstantAppConfiguration _instantAppConfiguration;
    protected Set<ActivityGroup> _activitySet = new HashSet<ActivityGroup>();
    protected Map<ActivityNode,ActivityGroup> _activityNodeToGroup = new HashMap<ActivityNode, ActivityGroup>();
    protected AndroidManifestParserWrapper _androidManifestParserWrapper;

    public IRecommendation(Project pproject, com.intellij.openapi.module.Module pseletedModule){
        _project = pproject;
        _module = pseletedModule;
    }

    public void buildVariant(){
        if(_solutiontempdialog.getExitCode()==DialogWrapper.OK_EXIT_CODE){
            String selected_solution_key = _solutiontempdialog.getSelectedSolution();
            assert _solutions.containsKey(selected_solution_key);
            List<Object> solution = _solutions.get(selected_solution_key);
            // init the representative

            _configurationForm = new RecommendationBaseIAConfigurationForm(_project,_androidManifestParserWrapper.getPackagename()
                    ,solution,_activityNodeToGroup);
            _configurationForm.show();

            if(_configurationForm.getExitCode()==DialogWrapper.OK_EXIT_CODE){
                _instantAppConfiguration = _configurationForm.getInstantAppConfiguration();

                _createBFVariantTask = new CreateBFVariantTask(_project, "Create APL with Instant App template",
                        true, PerformInBackgroundOption.DEAF,
                        _instantAppConfiguration, _activityNodes, _module,this);
                // run the task for building the instant app
                ProgressManager.getInstance().run(_createBFVariantTask);

            }

        }
    }

    public abstract DialogWrapper generateRecommendations();

    public abstract APLRecommendationApps getRecommendationName();

    public String[] getPossibleSolution(int numberfeatures_required){
        _solutions = RecommendationSolutionUtil.getPossibleSolutions(_activitySet,
                _activityNodeToGroup,numberfeatures_required);

        return (String[])_solutions.keySet().toArray(new String[_solutions.keySet().size()]);
    }


}
