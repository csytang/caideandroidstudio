package org.chrisyttang.plugin.caIDEAndroidStudio.recommendation;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.DialogWrapper;
import org.chrisyttang.androidmanifest.dom.ActivityNode;
import org.chrisyttang.plugin.caIDEAndroidStudio.instantapp.Module;
import org.chrisyttang.plugin.caIDEAndroidStudio.util.RecommendationSolutionUtil;

import java.util.List;
import java.util.Set;

/**
 * data flow based approach UI
 */
public class APLUIDepRecommendation extends IRecommendation {

    public APLUIDepRecommendation(Project pproject, com.intellij.openapi.module.Module pseletedModule){
        super(pproject,pseletedModule);
    }


    @Override
    public DialogWrapper generateRecommendations() {
        


        return null;
    }

    @Override
    public APLRecommendationApps getRecommendationName() {
        return APLRecommendationApps.UI_DEP;
    }

}
