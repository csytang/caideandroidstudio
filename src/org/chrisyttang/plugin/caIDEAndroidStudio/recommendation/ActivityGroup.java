package org.chrisyttang.plugin.caIDEAndroidStudio.recommendation;

import org.chrisyttang.androidmanifest.dom.ActivityNode;

import java.util.HashSet;
import java.util.Set;

public class ActivityGroup {
    private Set<ActivityNode> _activityNodeSet;

    public ActivityGroup(ActivityNode activityNode){
        this._activityNodeSet = new HashSet<ActivityNode>();
        this._activityNodeSet.add(activityNode);
    }

    public Set<ActivityNode> getActivities(){
        return _activityNodeSet;
    }

    public void merge(ActivityGroup activityGroup){
        _activityNodeSet.addAll(activityGroup.getActivities());
    }

    public String toString(){
        String content = "[";
        for(ActivityNode node:_activityNodeSet){
            content += node.getName();
            content += ";";
        }
        content+="]";
        return content;
    }
}
