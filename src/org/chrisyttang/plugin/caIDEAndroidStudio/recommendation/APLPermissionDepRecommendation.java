package org.chrisyttang.plugin.caIDEAndroidStudio.recommendation;

import com.android.xml.AndroidManifest;
import com.intellij.openapi.module.Module;
import com.intellij.openapi.progress.PerformInBackgroundOption;
import com.intellij.openapi.progress.ProgressManager;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.DialogWrapper;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiFile;
import com.intellij.psi.search.FilenameIndex;
import com.intellij.psi.search.GlobalSearchScope;
import org.chrisyttang.androidmanifest.dom.ActivityNode;
import org.chrisyttang.androidmanifest.dom.AndroidManifestNode;
import org.chrisyttang.plugin.caIDEAndroidStudio.instantapp.configuration.InstantAppConfiguration;
import org.chrisyttang.plugin.caIDEAndroidStudio.parser.ActivityDependency;
import org.chrisyttang.plugin.caIDEAndroidStudio.parser.AndroidManifestParserWrapper;
import org.chrisyttang.plugin.caIDEAndroidStudio.recommendation.gui.RecommendationSolutionTempDialog;
import org.chrisyttang.plugin.caIDEAndroidStudio.recommendation.recommendationconfig.RecommendationBaseIAConfigurationForm;
import org.chrisyttang.plugin.caIDEAndroidStudio.util.*;
import org.chrisyttang.plugin.caIDEAndroidStudio.variant.CreateBFVariantTask;

import javax.swing.*;
import java.io.File;
import java.util.*;

public class APLPermissionDepRecommendation extends IRecommendation {

    /**
     * We consider activity use the same permission directly or indirectly
     *
     */
    private List<AndroidManifestNode> _allPermissions;
    private Map<AndroidManifestNode,List<String>> _permissionMethodMapping = new HashMap<AndroidManifestNode,List<String>>();
    private PermissionClassReflectionUtil permissionClassReflectionUtil = new PermissionClassReflectionUtil();
    private Map<ActivityNode,List<AndroidPermission>> _activityPermissions = new HashMap<ActivityNode, List<AndroidPermission>>();

    public APLPermissionDepRecommendation(Project pproject, Module pseletedModule) {
        super(pproject, pseletedModule);
    }


    @Override
    public DialogWrapper generateRecommendations() {
        // 1. get all permissions
        _androidManifestParserWrapper = AndroidManifestParserWrapper.getInstant(super._project,super._module);

        _allPermissions = _androidManifestParserWrapper.getAllPermission();

        for(AndroidManifestNode permission:_allPermissions) {
            if(permissionClassReflectionUtil.getMethodsfromPermission(permission.getNodeName())!=null){
                _permissionMethodMapping.put(permission,
                        permissionClassReflectionUtil.getMethodsfromPermission(permission.getNodeName()));
            }
        }

        _activityNodes = _androidManifestParserWrapper.getAllActivities();

        for(int i = 0;i < _activityNodes.size();i++){
            ActivityNode activityNode = _activityNodes.get(i);
            ActivityGroup activityGroup = new ActivityGroup(activityNode);
            _activitySet.add(activityGroup);
            _activityNodeToGroup.put(activityNode,activityGroup);
        }


        // 2. loop all activities
        for(ActivityNode activity:_androidManifestParserWrapper.getAllActivities()){
            String activityFullName = activity.getName();
            String dirPath = "";
            if (activityFullName.contains(".")) {
                dirPath = activityFullName.substring(0, activityFullName.lastIndexOf(".")).replace(".", "/");
            }else{

            }
            String activityName = activityFullName.substring(activityFullName.lastIndexOf(".")+1,
                    activityFullName.length());


            PsiFile targetActivityFile = null;
            //PsiFile[] activityFiles = FilenameIndex.getFilesByName(_project,activityName+".java", GlobalSearchScope.moduleScope(super._module));
            PsiFile[] activityFiles = FilenameIndex.getFilesByName(_project,activityName+".java", GlobalSearchScope.allScope(_project));

            if (activityFiles.length==0){
                _activityPermissions.put(activity,new LinkedList<AndroidPermission>());
                continue;
            }

            if(!dirPath.isEmpty()) {
                for (PsiFile activityfile : activityFiles) {
                    if (activityfile.getVirtualFile().getPath().contains(dirPath)) {
                        targetActivityFile = activityfile;
                        break;
                    }
                }
            }else{
                targetActivityFile = activityFiles[0];
            }



            ActivityDependency activityDependency = new ActivityDependency(targetActivityFile.getVirtualFile(),super._module);

            List<VirtualFile> allrelatedFiles = activityDependency.getAllRelated();


            List<PsiFile> relatedpsiFiles = FileLevelDataFlow.getDataFlow(targetActivityFile,this._project);
            for(PsiFile file:relatedpsiFiles){

                    if (!allrelatedFiles.contains(file.getVirtualFile())) {
                        allrelatedFiles.add(file.getVirtualFile());
                    }

            }

            for(VirtualFile related_file:allrelatedFiles){
                List<AndroidPermission> permissions = VirtualFilePermissionFinder.getPermissionsFromVirtualFile(related_file);
                if(_activityPermissions.containsKey(activity)){
                    for(AndroidPermission permission:permissions){
                        if(!_activityPermissions.get(activity).contains(permission)){
                            _activityPermissions.get(activity).add(permission);
                        }
                    }
                }else{
                    _activityPermissions.put(activity,permissions);
                }
            }
        }

        // Write a temp result
        String content = "";

        // Write a temp result Map<ActivityNode,ActivityGroup> _activityNodeToGroup
        for(ActivityNode activity:_androidManifestParserWrapper.getAllActivities()){
            content += activity.getName();
            content += "\t";
            content += "[";
            List<AndroidPermission> permissions = _activityPermissions.get(activity);
            for(AndroidPermission permission:permissions){
                content += permission.getValue();
                content += ";";
            }
            content += "]";
            content += "\n";
        }
        String baseDir = this._project.getBaseDir().getPath();
        File temp  = new File(baseDir+File.separator+"permission_recommendation.txt");
        FileUtil.writeFile(temp,content);

        for(int i = 0; i < _activityNodes.size();i++){
            ActivityNode source = _activityNodes.get(i);
            List<AndroidPermission> sourcepermission = _activityPermissions.get(source);
            for(int j=i+1;j < _activityNodes.size();j++){
                ActivityNode sink = _activityNodes.get(j);
                List<AndroidPermission> sinkpermission = _activityPermissions.get(sink);

                List<AndroidPermission> overlaps = new LinkedList<AndroidPermission> (sourcepermission);
                overlaps.removeAll(sinkpermission);

                if(!overlaps.isEmpty()){
                    ActivityGroup sourcegroup = _activityNodeToGroup.get(source);
                    ActivityGroup targetgroup = _activityNodeToGroup.get(sink);

                    sourcegroup.merge(targetgroup);
                    _activityNodeToGroup.put(sink,sourcegroup);
                    _activitySet.remove(targetgroup);
                }

            }
        }


        _solutiontempdialog = new RecommendationSolutionTempDialog(_project,_activitySet.size(),_activityNodes.size(),this);
        return _solutiontempdialog;
    }

    @Override
    public APLRecommendationApps getRecommendationName() {
        return APLRecommendationApps.PERMISSION_DEP;
    }


}
