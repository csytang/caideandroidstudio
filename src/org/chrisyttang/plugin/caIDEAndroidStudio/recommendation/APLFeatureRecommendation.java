package org.chrisyttang.plugin.caIDEAndroidStudio.recommendation;

import com.intellij.openapi.progress.ProgressManager;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.DialogWrapper;
import org.chrisyttang.androidmanifest.dom.ActivityNode;
import org.chrisyttang.plugin.caIDEAndroidStudio.configuration.gui.ConfigurationForm;
import org.chrisyttang.plugin.caIDEAndroidStudio.features.Feature;
import org.chrisyttang.plugin.caIDEAndroidStudio.instantapp.Module;
import org.chrisyttang.plugin.caIDEAndroidStudio.variant.CreatingVariantTask;

import javax.swing.*;
import java.util.List;
import java.util.Set;

public class APLFeatureRecommendation extends IRecommendation {

    private ConfigurationForm _configurationForm;
    private Set<Feature> _alldiabledFeature;
    private CreatingVariantTask creatingVariantTask;

    public APLFeatureRecommendation(Project pproject, com.intellij.openapi.module.Module pseletedModule){
        super(pproject,pseletedModule);
    }

    @Override
    public void buildVariant() {


        // get all disabled features and enabled features
        _alldiabledFeature = _configurationForm.getDisabledFeatures();


        // create variant with variant creation task class
        if (_alldiabledFeature.size() != 0) {
            creatingVariantTask = new CreatingVariantTask(_project, _alldiabledFeature);
            ProgressManager.getInstance().run(creatingVariantTask);
        }

    }

    @Override
    public DialogWrapper generateRecommendations() {
        _configurationForm = new ConfigurationForm(_project);
        return _configurationForm;
    }

    @Override
    public APLRecommendationApps getRecommendationName() {
        return APLRecommendationApps.FEATURE_MODEL;
    }

    @Override
    public String[] getPossibleSolution(int numberfeatures_required) {
        return new String[0];
    }

}
