package org.chrisyttang.plugin.caIDEAndroidStudio.recommendation.gui;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.DialogWrapper;
import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;
import org.chrisyttang.plugin.caIDEAndroidStudio.recommendation.APLRecommendationApps;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.Vector;

public class RecommendationConfigDialog extends DialogWrapper {
    private JPanel rootpanel;
    private JComboBox recommendationComboBox;
    private JLabel descriptionLabel;
    private Vector<String> aplapproaches = new Vector<String>();
    private APLRecommendationApps _approchtype = APLRecommendationApps.FEATURE_MODEL;


    public RecommendationConfigDialog(@Nullable Project project) {
        super(project);
        aplapproaches.addElement("[Base] Feature Model based APL");
        aplapproaches.addElement("[Base_Feature] Activity Dependency based APL");
        aplapproaches.addElement("[Base_Feature] UI Dependency based APL");
        aplapproaches.addElement("[Base_Feature] Permission based APL");
        $$$setupUI$$$();
        setResizable(true);
        init();
        recommendationComboBox.addItemListener(new ItemListener() {
            /**
             * Invoked when an item has been selected or deselected by the user.
             * The code written for this method performs the operations
             * that need to occur when an item is selected (or deselected).
             *
             * @param e
             */
            @Override
            public void itemStateChanged(ItemEvent e) {
                String selectedItem = (String) e.getItem();
                switch (selectedItem) {
                    case "[Base] Feature Model based APL": {
                        descriptionLabel.setText("<html>" +
                                "The android product line is build <br/>" +
                                "by the feature model with configuration <br/>" +
                                "(only base module) <br/>" +
                                "</html>");
                        _approchtype = APLRecommendationApps.FEATURE_MODEL;
                        break;
                    }
                    case "[Base_Feature] Activity Dependency based APL": {
                        descriptionLabel.setText("<html>" +
                                "The android product line is build <br/>" +
                                "by analyzing the dependencies between <br/>" +
                                "actives (base + feature module) <br/>" +
                                "</html>");
                        _approchtype = APLRecommendationApps.ACTIVITY_TRANS;
                        break;
                    }
                    case "[Base_Feature] UI Dependency based APL": {
                        descriptionLabel.setText("<html>" +
                                "The android product line is build <br/>" +
                                "by analyzing the dependencies between <br/>" +
                                "UI elements (base + feature module) <br/>" +
                                "</html>");
                        _approchtype = APLRecommendationApps.UI_DEP;
                        break;
                    }
                    case "[Base_Feature] Permission based APL": {
                        descriptionLabel.setText("<html>" +
                                "The android product line is build <br/>" +
                                "by analyzing the dependencies between <br/>" +
                                "permission and code base (base + feature module) <br/>" +
                                "</html>");
                        _approchtype = APLRecommendationApps.PERMISSION_DEP;
                        break;
                    }
                }
            }
        });


    }

    @Nullable
    @Override
    protected JComponent createCenterPanel() {
        return rootpanel;
    }

    private void createUIComponents() {
        rootpanel = new JPanel();
        descriptionLabel = new JLabel();
        recommendationComboBox = new JComboBox(aplapproaches);
    }


    @Override
    protected void doOKAction() {
        super.doOKAction();
    }

    public APLRecommendationApps getSelectedRecommendationApproach() {
        return _approchtype;
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$() {
        createUIComponents();
        rootpanel.setLayout(new GridLayoutManager(3, 2, new Insets(0, 0, 0, 0), -1, -1));
        final JLabel label1 = new JLabel();
        label1.setText("Please select the recommendation approach for building APL");
        rootpanel.add(label1, new GridConstraints(0, 0, 1, 2, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        rootpanel.add(recommendationComboBox, new GridConstraints(1, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label2 = new JLabel();
        label2.setText("Select recommendations");
        rootpanel.add(label2, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        descriptionLabel.setText("<html>The android product line is build <br/>\nby the feature model with configuration <br/>\n(only base module) <br/></html>");
        rootpanel.add(descriptionLabel, new GridConstraints(2, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    }

    /**
     * @noinspection ALL
     */
    public JComponent $$$getRootComponent$$$() {
        return rootpanel;
    }
}
