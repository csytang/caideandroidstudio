package org.chrisyttang.plugin.caIDEAndroidStudio.recommendation;

import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.module.Module;
import com.intellij.openapi.progress.PerformInBackgroundOption;
import com.intellij.openapi.progress.ProgressManager;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.DialogWrapper;
import org.chrisyttang.androidmanifest.dom.ActivityNode;
import org.chrisyttang.plugin.caIDEAndroidStudio.instantapp.configuration.InstantAppConfiguration;
import org.chrisyttang.plugin.caIDEAndroidStudio.parser.*;
import org.chrisyttang.plugin.caIDEAndroidStudio.recommendation.gui.RecommendationSolutionTempDialog;
import org.chrisyttang.plugin.caIDEAndroidStudio.recommendation.recommendationconfig.RecommendationBaseIAConfigurationForm;
import org.chrisyttang.plugin.caIDEAndroidStudio.util.FileUtil;
import org.chrisyttang.plugin.caIDEAndroidStudio.util.RecommendationSolutionUtil;
import org.chrisyttang.plugin.caIDEAndroidStudio.variant.CreateBFVariantTask;

import java.io.File;
import java.util.*;

public class APLActivityTransRecommendation extends IRecommendation {


    private GatorsetupDialog _gatorSetupDialog;
    private ActivityTransactionGraph _activityTransactionGraph;
    private AnActionEvent _anActionEvent;
    private ATGraph _atggraph;




    public APLActivityTransRecommendation(Project pproject, Module pmodule,AnActionEvent panActionEvent){
        super(pproject,pmodule);
        _anActionEvent = panActionEvent;
    }



    @Override
    public DialogWrapper generateRecommendations() {
        // 1. build the apk transaction graph between activity
        // start a static analysis for the target Android project
        _gatorSetupDialog = new GatorsetupDialog(_project);
        _gatorSetupDialog.show();


        if(_gatorSetupDialog.getExitCode()== DialogWrapper.OK_EXIT_CODE){
            String androidLocation = _gatorSetupDialog.getAndroidSDKLocation();
            String gatorLocation = _gatorSetupDialog.getGatorLocation();
            _activityTransactionGraph = new ActivityTransactionGraph(androidLocation,
                    gatorLocation,_project,_module,_anActionEvent);

            try {
                _activityTransactionGraph.parse();
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            }

            _atggraph = _activityTransactionGraph.getATGraph();

            // 2. get all activities
            _androidManifestParserWrapper = AndroidManifestParserWrapper.getInstant(_project,_module);
            _activityNodes = _androidManifestParserWrapper.getAllActivities();


            for(int i = 0;i < _activityNodes.size();i++){
                ActivityNode activityNode = _activityNodes.get(i);
                ActivityGroup activityGroup = new ActivityGroup(activityNode);
                _activitySet.add(activityGroup);
                _activityNodeToGroup.put(activityNode,activityGroup);
            }

            for(int i = 0; i < _activityNodes.size();i++){
                ActivityNode source = _activityNodes.get(i);
                for(int j=i+1;j < _activityNodes.size();j++){
                    ActivityNode sink = _activityNodes.get(j);
                    if(_atggraph.isReachable(source,sink)&&
                            _atggraph.isReachable(sink,source)){
                        ActivityGroup sourcegroup = _activityNodeToGroup.get(source);
                        ActivityGroup targetgroup = _activityNodeToGroup.get(sink);
                        sourcegroup.merge(targetgroup);
                        _activityNodeToGroup.put(sink,sourcegroup);
                        _activitySet.remove(targetgroup);
                    }
                }
            }

            // Write a temp result
            String content = "";

            // Write a temp result Map<ActivityNode,ActivityGroup> _activityNodeToGroup
            for(ActivityGroup group:_activityNodeToGroup.values()){
                content += group.toString();
                content += "\n";
            }
            String baseDir = this._project.getBaseDir().getPath();

            File temp  = new File(baseDir+File.separator+"acttrans_recommendation.txt");

            FileUtil.writeFile(temp,content);

            _solutiontempdialog = new RecommendationSolutionTempDialog(_project,_activitySet.size(),_activityNodes.size(),this);
            return _solutiontempdialog;

        }else{
            return null;
        }

    }


    @Override
    public APLRecommendationApps getRecommendationName() {
        return APLRecommendationApps.ACTIVITY_TRANS;
    }


}
