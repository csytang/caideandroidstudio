package org.chrisyttang.plugin.caIDEAndroidStudio.featuremodel.io;

import com.intellij.openapi.vfs.VirtualFile;
import org.chrisyttang.plugin.caIDEAndroidStudio.featuremodel.FeatureModel;

public interface IFeatureModelWriter {

    /**
     * Returns the feature model to write out.
     *
     * @return the model to write
     */
    public FeatureModel getFeatureModel();

    /**
     * Sets the feature model to be saved in a textual representation.
     *
     * @param featureModel the model to write
     */
    public void setFeatureModel(FeatureModel featureModel);

    /**
     * Saves a feature model to a file.
     *
     * @param file
     */
    public abstract void writeToFile(VirtualFile file);


    /**
     * Converts a feature model to a textual representation.
     *
     * @return
     */
    public abstract String writeToString();

}
