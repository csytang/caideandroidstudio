package org.chrisyttang.plugin.caIDEAndroidStudio.featuremodel.io;

import com.intellij.openapi.vfs.VirtualFile;
import org.chrisyttang.plugin.caIDEAndroidStudio.featuremodel.FeatureModel;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.LinkedList;
import java.util.List;

public abstract class AbstractFeatureModelReader implements IFeatureModelReader {
    /**
     * the structure to store the parsed data
     */
    protected FeatureModel featureModel;

    /**
     * warnings occurred while parsing
     */
    protected LinkedList<ModelWarning> warnings = new LinkedList<ModelWarning>();

    public void setFeatureModel(FeatureModel featureModel) {
        this.featureModel = featureModel;
    }

    public FeatureModel getFeatureModel() {
        return featureModel;
    }

    @Override
    public void readFromFile(VirtualFile file) throws UnsupportedModelException, FileNotFoundException {
        warnings.clear();
        String fileName = file.getPath();
        InputStream inputStream = new FileInputStream(fileName);
        parseInputStream(inputStream);
    }

    public void readFromString(String text) throws UnsupportedModelException {
        warnings.clear();
        InputStream inputStream = new ByteArrayInputStream(text.getBytes());
        parseInputStream(inputStream);
    }

    public List<ModelWarning> getWarnings() {
        return warnings;
    }

    /**
     * Reads a feature model from an input stream.
     *
     * @param  inputStream  the textual representation of the feature model
     * @throws UnsupportedModelException
     */
    protected abstract void parseInputStream(InputStream inputStream)
            throws UnsupportedModelException;
}
