package org.chrisyttang.plugin.caIDEAndroidStudio.featuremodel.io;

import com.intellij.openapi.vfs.VirtualFile;
import org.chrisyttang.plugin.caIDEAndroidStudio.featuremodel.FeatureModel;

import java.io.IOException;
import java.io.OutputStream;

public abstract class AbstractFeatureModelWriter implements IFeatureModelWriter {

    /**
     * the feature model to write out
     */
    protected FeatureModel featureModel;

    public void setFeatureModel(FeatureModel featureModel) {
        this.featureModel = featureModel;
    }

    public FeatureModel getFeatureModel() {
        return featureModel;
    }

    public void writeToFile(VirtualFile file) {
        try {
            if (!file.exists()) {
                try {
                    throw new Exception("file is not exception");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                System.exit(-1);
            }
            OutputStream stream = file.getOutputStream(null);
            stream.write(writeToString().getBytes());
            stream.flush();
            stream.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}