package org.chrisyttang.plugin.caIDEAndroidStudio.featuremodel;

import org.chrisyttang.plugin.caIDEAndroidStudio.features.PropertyConstants;
import org.prop4j.Node;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.LinkedList;

public class Constraint implements PropertyConstants {

    private FeatureModel featureModel;

    private int index;

    public Constraint(FeatureModel featureModel, int index) {
        this.featureModel = featureModel;
        this.index = index;
    }

    public Node getNode() {
        return featureModel.getConstraint(index);
    }


    private LinkedList<PropertyChangeListener> listenerList = new LinkedList<PropertyChangeListener>();

    public void addListener(PropertyChangeListener listener) {
        if (!listenerList.contains(listener))
            listenerList.add(listener);
    }

    public void removeListener(PropertyChangeListener listener) {
        listenerList.remove(listener);
    }

    public void fire(PropertyChangeEvent event) {
        for (PropertyChangeListener listener : listenerList)
            listener.propertyChange(event);
    }

}
