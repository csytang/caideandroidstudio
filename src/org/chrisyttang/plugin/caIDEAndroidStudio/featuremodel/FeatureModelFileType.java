package org.chrisyttang.plugin.caIDEAndroidStudio.featuremodel;

import com.intellij.openapi.fileTypes.LanguageFileType;
import com.intellij.openapi.util.IconLoader;
import org.chrisyttang.plugin.caIDEAndroidStudio.featuremodel.language.FeatureModelLanguage;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;

public class FeatureModelFileType extends LanguageFileType {


    public static final String DEFAULT_EXTENSION = "afm";

    protected FeatureModelFileType(){
        super(FeatureModelLanguage.INSTANCE);
    }

    /**
     *
     * @return the default extension of a feature model is fm
     */
    @NotNull
    @Override
    public String getName() {
        return DEFAULT_EXTENSION;//
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Feature model grammar file";
    }

    @NotNull
    @Override
    public String getDefaultExtension() {
        return "afm";
    }

    @Nullable
    @Override
    public Icon getIcon() {
        return IconLoader.getIcon("featuremodel.png");
    }
}
