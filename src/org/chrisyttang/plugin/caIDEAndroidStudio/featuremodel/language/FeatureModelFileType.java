package org.chrisyttang.plugin.caIDEAndroidStudio.featuremodel.language;

import com.intellij.openapi.fileTypes.LanguageFileType;
import org.chrisyttang.plugin.caIDEAndroidStudio.util.IconUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;

public class FeatureModelFileType extends LanguageFileType {

    public static final FeatureModelFileType INSTANCE = new FeatureModelFileType();

    private FeatureModelFileType(){
        super(FeatureModelLanguage.INSTANCE);
    }


    @NotNull
    @Override
    public String getName() {
        return "Feature model file";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Feature model file used in caIDE";
    }

    @NotNull
    @Override
    public String getDefaultExtension() {
        return "afm";
    }

    @Nullable
    @Override
    public Icon getIcon() {
        return IconUtil.FEATUREMODEL_FILE;
    }
}

