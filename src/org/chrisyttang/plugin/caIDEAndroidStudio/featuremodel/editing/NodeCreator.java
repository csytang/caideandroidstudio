package org.chrisyttang.plugin.caIDEAndroidStudio.featuremodel.editing;

import org.chrisyttang.plugin.caIDEAndroidStudio.featuremodel.FeatureModel;
import org.chrisyttang.plugin.caIDEAndroidStudio.features.Feature;
import org.prop4j.*;

import java.util.HashMap;
import java.util.LinkedList;

public class NodeCreator {

    private static final HashMap<Object, Node> EMPTY_MAP = new HashMap<Object, Node>();

    public static Node createNodes(FeatureModel featureModel) {
        return createNodes(featureModel, EMPTY_MAP);
    }

    public static Node createNodes(FeatureModel featureModel, HashMap<Object, Node> replacingMap) {
        Feature root = featureModel.getRoot();
        LinkedList<Node> nodes = new LinkedList<Node>();
        if (root != null) {
            nodes.add(new Literal(getVariable(root, featureModel)));
            //convert grammar rules into propositional formulas
            createNodes(nodes, root, featureModel, true, replacingMap);
            //add extra constraints
            for (Node node : featureModel.getPropositionalNodes())
                nodes.add(node.clone());
        }
        return eliminateAbstractVariables(new And(nodes), replacingMap);
    }

    public static Node eliminateAbstractVariables(Node node, HashMap<Object, Node> map) {
        if (node instanceof Literal) {
            Literal literal = (Literal) node;
            if (map.containsKey(literal.var)) {
                Node replacing = map.get(literal.var);
                node = literal.positive ? replacing : new Not(replacing);
            }
        }
        else {
            Node[] children = node.getChildren();
            for (int i = 0; i < children.length; i++)
                children[i] = eliminateAbstractVariables(children[i], map);
        }
        return node;
    }

    private static void createNodes(LinkedList<Node> nodes, Feature rootFeature, FeatureModel featureModel, boolean recursive, HashMap<Object, Node> replacings) {
        if (rootFeature == null || !rootFeature.hasChildren())
            return;

        String s = getVariable(rootFeature, featureModel);

        Node[] children = new Node[rootFeature.getChildrenCount()];
        for (int i = 0; i < children.length; i++) {
            String var = getVariable(rootFeature.getChildren().get(i), featureModel);
            children[i] = new Literal(var);
        }
        Node definition = children.length == 1 ? children[0] : new Or(children);

        if (rootFeature.isAnd()) {// && (!replacings.containsKey(featureModel.getOldName(rootFeature.getName())) || !rootFeature.isPossibleEmpty())) {
            LinkedList<Node> manChildren = new LinkedList<Node>();
            for (Feature feature : rootFeature.getChildren())
                if (feature.isMandatory()) {
                    String var = getVariable(feature, featureModel);
                    manChildren.add(new Literal(var));
                }

            //add constraints for all mandatory children S => (A & B)
            if (manChildren.size() == 1)
                nodes.add(new Implies(new Literal(s), manChildren.getFirst()));
            else if (manChildren.size() > 1)
                nodes.add(new Implies(new Literal(s), new And(manChildren)));

            //add contraint (A | B | C) => S
            if (!replacings.containsKey(featureModel.getOldName(rootFeature.getName()))) {
                if (rootFeature.isAbstract())
                    nodes.add(new Equals(definition, new Literal(s)));
                else
                    nodes.add(new Implies(definition, new Literal(s)));
            }
        }
        else {
            //add constraint S <=> (A | B | C)
            if (!replacings.containsKey(featureModel.getOldName(rootFeature.getName()))) {
                nodes.add(new Equals(new Literal(s), definition));
            }

            if (rootFeature.isAlternative()) {
                //add constraint atmost1(A, B, C)
                if (children.length > 1)
                    nodes.add(new AtMost(1, Node.clone(children)));
            }
        }

        if (recursive)
            for (Feature feature : rootFeature.getChildren())
                createNodes(nodes, feature, featureModel, true, replacings);
    }

    public static String getVariable(Feature feature, FeatureModel featureModel) {
        return featureModel.getOldName(feature.getName());
    }

}
