/*
 * Copyright (c) 2017. Chris Tang Inc. All rights reserved.
 *
 * Anroid Product Line IDE (short for APLIDE) (@link http://chrisyttang.org/project/aplide)
 * This software is distributed to you under the "aplIDE - Terms and conditions" license(hereinafter: The Agreenment).
 * Using this software indicates the agreement between you, a Business Entity, and Chris tang Inc. has been signed and set.
 *
 */

package org.chrisyttang.plugin.caIDEAndroidStudio.featuremodel;

/**
 *
 */
public class FeatureModelNotFoundException extends Exception {

    private static final long serialVersionUID = 1L;

    /**
     *
     * @param string
     */
    public FeatureModelNotFoundException(String string) {
        super(string);
    }

    /**
     *
     * @param string
     * @param e
     */
    public FeatureModelNotFoundException(String string, Exception e) {
        super(string, e);
    }

    /**
     *
     */
    public FeatureModelNotFoundException() {
        super();
    }

}