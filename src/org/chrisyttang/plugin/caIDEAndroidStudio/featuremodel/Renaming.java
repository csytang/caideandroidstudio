package org.chrisyttang.plugin.caIDEAndroidStudio.featuremodel;

public class Renaming {
    public final String oldName;

    public final String newName;

    public Renaming(String oldName, String newName) {
        this.oldName = oldName;
        this.newName = newName;
    }
}
