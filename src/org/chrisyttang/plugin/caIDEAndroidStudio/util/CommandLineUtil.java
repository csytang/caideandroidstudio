package org.chrisyttang.plugin.caIDEAndroidStudio.util;

import com.intellij.openapi.diagnostic.Logger;
import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.Executor;
import org.apache.commons.exec.PumpStreamHandler;
import org.apache.commons.exec.environment.EnvironmentUtils;

import java.io.*;
import java.util.List;
import java.util.Map;

public class CommandLineUtil {
    public static final Logger LOG = Logger.getInstance("org.chrisyttang.plugin.caIDEAndroidStudio.util.CommandLineUtil");

    public static String executCommand(List<String> commands) throws IOException, InterruptedException {

        StringBuffer output = new StringBuffer();

        ProcessBuilder pb = new ProcessBuilder(commands);
        pb.inheritIO();

        Process p = null;

        if (pb != null) {


            p = pb.start();

            if (p != null) {
                p.waitFor();
            }
        }
        BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));

        String line = "";

        while ((line = reader.readLine())!= null)
        {
            output.append(line+"\n");
            LOG.info(line+"\n");
        }

        return output.toString();
    }

    public static void executCommand(File dir, List<String> commands,Map<String,String> environments,String outputPath) throws IOException, InterruptedException {


        ProcessBuilder pb = new ProcessBuilder(commands);
        pb.inheritIO();
        for(Map.Entry<String,String>entry:environments.entrySet()){
            pb.environment().put(entry.getKey(),entry.getValue());
        }
        File outputfile = new File(outputPath);



        pb.redirectOutput(new File(outputPath));


        pb.directory(dir);

        Process p = null;

        if (pb != null) {


            p = pb.start();

            if (p != null) {
                p.waitFor();
            }
        }
    }

    public static String executBashScriptFile(String scriptPath,List<String>arguments,File workDir){
        CommandLine cmd = new CommandLine(new File(scriptPath));
        for(String argument:arguments){
            cmd.addArgument(argument);
        }
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        Executor executor = new DefaultExecutor();
        executor.setWorkingDirectory(workDir);

        PumpStreamHandler streamHandler = new PumpStreamHandler(outputStream);
        executor.setStreamHandler(streamHandler);

        try {
            executor.execute(cmd);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return outputStream.toString();
    }

    public static String executBashScriptContent(String arguments,File workDir,Map environmentVariables){
        CommandLine cmd = new CommandLine("/bin/bash");
        cmd.addArguments(arguments);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        Executor executor = new DefaultExecutor();
        executor.setWorkingDirectory(workDir);

        PumpStreamHandler streamHandler = new PumpStreamHandler(outputStream);
        executor.setStreamHandler(streamHandler);

        try {
            executor.execute(cmd, environmentVariables);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return outputStream.toString();
    }


    public static String executPythonScriptContent(String arguments,File workDir,Map environmentVariables){


        CommandLine cmd = new CommandLine("python2");
        cmd.addArguments(arguments);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        Executor executor = new DefaultExecutor();
        executor.setWorkingDirectory(workDir);

        PumpStreamHandler streamHandler = new PumpStreamHandler(outputStream);
        executor.setStreamHandler(streamHandler);

        try {
            executor.execute(cmd, environmentVariables);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return outputStream.toString();
    }
}
