package org.chrisyttang.plugin.caIDEAndroidStudio.util;

import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.command.CommandProcessor;
import com.intellij.openapi.project.Project;
/**
 * Helper class for launching filesystem write operations on background.
 *
 * @author David Vávra (david@inmite.eu)
 */
public class RunnableHelper
{
    private RunnableHelper()
    {
    }

    public static void runReadCommand(Project project, Runnable cmd)
    {
        ApplicationManager.getApplication().invokeLater(new ReadAction(cmd));
    }

    public static void runWriteCommand(Project project, Runnable cmd)
    {
        ApplicationManager.getApplication().invokeLater(new WriteAction(cmd));
    }

    // ---- Action
    static class ReadAction implements Runnable
    {
        Runnable cmd;

        ReadAction(Runnable cmd)
        {
            this.cmd = cmd;
        }

        public void run()
        {
            ApplicationManager.getApplication().runReadAction(cmd);
        }
    }

    static class WriteAction implements Runnable
    {
        Runnable cmd;

        WriteAction(Runnable cmd)
        {
            this.cmd = cmd;
        }

        public void run()
        {
            ApplicationManager.getApplication().runWriteAction(cmd);
        }
    }

}