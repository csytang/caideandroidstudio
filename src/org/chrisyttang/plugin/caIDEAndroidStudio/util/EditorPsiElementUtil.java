package org.chrisyttang.plugin.caIDEAndroidStudio.util;

import com.intellij.lang.ASTNode;
import com.intellij.psi.*;

import java.util.LinkedList;
import java.util.List;

public class EditorPsiElementUtil {

    /**
     *
     * @param startOffset the start offset
     * @param endOffset the end offset
     * @param file the file to lookup
     * @return a list of PsiElement within the range
     */
    public static List<PsiElement> getAllPsiElementInRange(int startOffset, int endOffset, PsiFile file){
        List<PsiElement> psiElements = new LinkedList<PsiElement>();

        file.accept(new PsiRecursiveElementWalkingVisitor() {
            @Override
            public void visitElement(PsiElement element) {

                if(element==null)
                    return;

                if(element instanceof PsiComment){
                    return;
                }

                if(element instanceof PsiWhiteSpace){
                    return;
                }

                if(element.getTextRange().getStartOffset()>=startOffset &&
                        element.getTextRange().getEndOffset()<=endOffset){
                    psiElements.add(element);
                    return;
                }
                super.visitElement(element);

            }
        });

        return psiElements;
    }

    private static PsiElement findFirstLeafNodeInPsiElement(PsiElement element) {
        PsiElement firstchild = element;
        while(firstchild.getFirstChild()!=null){
            firstchild = firstchild.getFirstChild();
        }
        return firstchild;
    }

    protected static PsiElement firstNonWhiteElement(int offset, PsiFile file, final boolean lookRight) {
        final ASTNode leafElement = file.getNode().findLeafElementAt(offset);
        return leafElement == null ? null : firstNonWhiteElement(leafElement.getPsi(), lookRight);
    }

    protected static PsiElement firstNonWhiteElement(PsiElement element, final boolean lookRight) {
        if (element instanceof PsiWhiteSpace) {
            element = lookRight ? element.getNextSibling() : element.getPrevSibling();
        }
        return element;
    }
}
