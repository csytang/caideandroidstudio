package org.chrisyttang.plugin.caIDEAndroidStudio.util;

import com.intellij.lang.java.JavaFindUsagesProvider;
import com.intellij.openapi.util.text.StringUtil;
import com.intellij.psi.*;
import com.intellij.psi.util.PsiExpressionTrimRenderer;
import com.intellij.psi.util.PsiFormatUtil;
import com.intellij.psi.util.PsiTreeUtil;
import com.intellij.util.BitUtil;
import com.intellij.util.VisibilityUtil;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class PsiFormater {

    public static String getExternalName(PsiElement owner, boolean showParamName) {
        JavaFindUsagesProvider usagesProvider = new JavaFindUsagesProvider();
        return usagesProvider.getDescriptiveName(owner);
    }

}
