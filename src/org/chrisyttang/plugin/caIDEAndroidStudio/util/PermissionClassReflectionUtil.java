package org.chrisyttang.plugin.caIDEAndroidStudio.util;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class PermissionClassReflectionUtil {

    private Map<String,List<String>> permissionToClasses = new HashMap<String,List<String>>();
    public PermissionClassReflectionUtil(){
        LoadPermission();
    }



    public void LoadPermission(){
        // add all permissions
        permissionToClasses.put("android.permission.ACCESS_ALL_DOWNLOADS", new LinkedList<String>());
        permissionToClasses.put("android.permission.ACCESS_COARSE_LOCATION", new LinkedList<String>());
        permissionToClasses.put("android.permission.ACCESS_DOWNLOAD_MANAGER_ADVANCED", new LinkedList<String>());
        permissionToClasses.put("android.permission.ACCESS_DRM", new LinkedList<String>());
        permissionToClasses.put("android.permission.ACCESS_FINE_LOCATION", new LinkedList<String>());
        permissionToClasses.put("android.permission.ACCESS_LOCATION_EXTRA_COMMANDS", new LinkedList<String>());
        permissionToClasses.put("android.permission.ACCESS_MOCK_LOCATION", new LinkedList<String>());
        permissionToClasses.put("android.permission.ACCESS_NETWORK_STATE", new LinkedList<String>());
        permissionToClasses.put("android.permission.ACCESS_WIFI_STATE", new LinkedList<String>());
        permissionToClasses.put("android.permission.AUTHENTICATE_ACCOUNTS", new LinkedList<String>());
        permissionToClasses.put("android.permission.BATTERY_STATS", new LinkedList<String>());
        permissionToClasses.put("android.permission.BIND_DEVICE_ADMIN", new LinkedList<String>());
        permissionToClasses.put("android.permission.BLUETOOTH", new LinkedList<String>());
        permissionToClasses.put("android.permission.BLUETOOTH_ADMIN", new LinkedList<String>());
        permissionToClasses.put("android.permission.CALL_PRIVILEGED", new LinkedList<String>());
        permissionToClasses.put("android.permission.CHANGE_COMPONENT_ENABLED_STATE", new LinkedList<String>());
        permissionToClasses.put("android.permission.CHANGE_NETWORK_STATE", new LinkedList<String>());
        permissionToClasses.put("android.permission.CHANGE_WIFI_MULTICAST_STATE", new LinkedList<String>());
        permissionToClasses.put("android.permission.CHANGE_WIFI_STATE", new LinkedList<String>());
        permissionToClasses.put("android.permission.CLEAR_APP_CACHE", new LinkedList<String>());
        permissionToClasses.put("android.permission.CLEAR_APP_USER_DATA", new LinkedList<String>());
        permissionToClasses.put("android.permission.CONNECTIVITY_INTERNAL", new LinkedList<String>());
        permissionToClasses.put("android.permission.DELETE_CACHE_FILES", new LinkedList<String>());
        permissionToClasses.put("android.permission.DELETE_PACKAGES", new LinkedList<String>());
        permissionToClasses.put("android.permission.DEVICE_POWER", new LinkedList<String>());
        permissionToClasses.put("android.permission.DISABLE_KEYGUARD", new LinkedList<String>());
        permissionToClasses.put("android.permission.DOWNLOAD_CACHE_NON_PURGEABLE", new LinkedList<String>());
        permissionToClasses.put("android.permission.DUMP", new LinkedList<String>());
        permissionToClasses.put("android.permission.EXPAND_STATUS_BAR", new LinkedList<String>());
        permissionToClasses.put("android.permission.GET_ACCOUNTS", new LinkedList<String>());
        permissionToClasses.put("android.permission.GET_PACKAGE_SIZE", new LinkedList<String>());
        permissionToClasses.put("android.permission.GLOBAL_SEARCH", new LinkedList<String>());
        permissionToClasses.put("android.permission.GRANT_REVOKE_PERMISSIONS", new LinkedList<String>());
        permissionToClasses.put("android.permission.INSTALL_DRM", new LinkedList<String>());
        permissionToClasses.put("android.permission.INSTALL_LOCATION_PROVIDER", new LinkedList<String>());
        permissionToClasses.put("android.permission.INSTALL_PACKAGES", new LinkedList<String>());
        permissionToClasses.put("android.permission.INTERNET", new LinkedList<String>());
        permissionToClasses.put("android.permission.MANAGE_ACCOUNTS", new LinkedList<String>());
        permissionToClasses.put("android.permission.MANAGE_APP_TOKENS", new LinkedList<String>());
        permissionToClasses.put("android.permission.MANAGE_NETWORK_POLICY", new LinkedList<String>());
        permissionToClasses.put("android.permission.MANAGE_USB", new LinkedList<String>());
        permissionToClasses.put("android.permission.MODIFY_AUDIO_SETTINGS", new LinkedList<String>());
        permissionToClasses.put("android.permission.MODIFY_NETWORK_ACCOUNTING", new LinkedList<String>());
        permissionToClasses.put("android.permission.MODIFY_PHONE_STATE", new LinkedList<String>());
        permissionToClasses.put("android.permission.MOVE_PACKAGE", new LinkedList<String>());
        permissionToClasses.put("android.permission.NFC", new LinkedList<String>());
        permissionToClasses.put("android.permission.PACKAGE_USAGE_STATS", new LinkedList<String>());
        permissionToClasses.put("android.permission.PACKAGE_VERIFICATION_AGENT", new LinkedList<String>());
        permissionToClasses.put("android.permission.READ_CONTACTS", new LinkedList<String>());
        permissionToClasses.put("android.permission.READ_FRAME_BUFFER", new LinkedList<String>());
        permissionToClasses.put("android.permission.READ_LOGS", new LinkedList<String>());
        permissionToClasses.put("android.permission.READ_NETWORK_USAGE_HISTORY", new LinkedList<String>());
        permissionToClasses.put("android.permission.READ_PHONE_STATE", new LinkedList<String>());
        permissionToClasses.put("android.permission.READ_PRIVILEGED_PHONE_STATE", new LinkedList<String>());
        permissionToClasses.put("android.permission.READ_PROFILE", new LinkedList<String>());
        permissionToClasses.put("android.permission.READ_SOCIAL_STREAM", new LinkedList<String>());
        permissionToClasses.put("android.permission.READ_SYNC_SETTINGS", new LinkedList<String>());
        permissionToClasses.put("android.permission.READ_SYNC_STATS", new LinkedList<String>());
        permissionToClasses.put("android.permission.REBOOT", new LinkedList<String>());
        permissionToClasses.put("android.permission.RECEIVE_SMS", new LinkedList<String>());
        permissionToClasses.put("android.permission.REMOTE_AUDIO_PLAYBACK", new LinkedList<String>());
        permissionToClasses.put("android.permission.SEND_SMS", new LinkedList<String>());
        permissionToClasses.put("android.permission.SERIAL_PORT", new LinkedList<String>());
        permissionToClasses.put("android.permission.SET_ANIMATION_SCALE", new LinkedList<String>());
        permissionToClasses.put("android.permission.SET_KEYBOARD_LAYOUT", new LinkedList<String>());
        permissionToClasses.put("android.permission.SET_ORIENTATION", new LinkedList<String>());
        permissionToClasses.put("android.permission.SET_POINTER_SPEED", new LinkedList<String>());
        permissionToClasses.put("android.permission.SET_PREFERRED_APPLICATIONS", new LinkedList<String>());
        permissionToClasses.put("android.permission.SHUTDOWN", new LinkedList<String>());
        permissionToClasses.put("android.permission.STATUS_BAR", new LinkedList<String>());
        permissionToClasses.put("android.permission.STATUS_BAR_SERVICE", new LinkedList<String>());
        permissionToClasses.put("android.permission.UPDATE_DEVICE_STATS", new LinkedList<String>());
        permissionToClasses.put("android.permission.UPDATE_LOCK", new LinkedList<String>());
        permissionToClasses.put("android.permission.USE_CREDENTIALS", new LinkedList<String>());
        permissionToClasses.put("android.permission.USE_SIP", new LinkedList<String>());
        permissionToClasses.put("android.permission.VIBRATE", new LinkedList<String>());
        permissionToClasses.put("android.permission.WAKE_LOCK", new LinkedList<String>());
        permissionToClasses.put("android.permission.WRITE_APN_SETTINGS", new LinkedList<String>());
        permissionToClasses.put("android.permission.WRITE_CONTACTS", new LinkedList<String>());
        permissionToClasses.put("android.permission.WRITE_PROFILE", new LinkedList<String>());
        permissionToClasses.put("android.permission.WRITE_SECURE_SETTINGS", new LinkedList<String>());
        permissionToClasses.put("android.permission.WRITE_SETTINGS", new LinkedList<String>());
        permissionToClasses.put("android.permission.WRITE_SOCIAL_STREAM", new LinkedList<String>());
        permissionToClasses.put("android.permission.WRITE_SYNC_SETTINGS", new LinkedList<String>());
        permissionToClasses.put("com.android.email.permission.ACCESS_PROVIDER", new LinkedList<String>());
        permissionToClasses.put("com.android.voicemail.permission.ADD_VOICEMAIL", new LinkedList<String>());
        permissionToClasses.put("com.android.voicemail.permission.READ_WRITE_ALL_VOICEMAIL", new LinkedList<String>());
        permissionToClasses.put("Parent", new LinkedList<String>());
        permissionToClasses.put("Unknown", new LinkedList<String>());
        // add all mappings

        permissionToClasses = PermissionClassReflectionPartOne.loadPermissionPartOne(permissionToClasses);
        permissionToClasses = PermissionClassReflectionPartTwo.loadPermissionPartTwo(permissionToClasses);
        permissionToClasses = PermissionClassReflectionPartThree.loadPermissionPartThree(permissionToClasses);
        permissionToClasses = PermissionClassReflectionPartFour.loadPermissionPartFour(permissionToClasses);
        permissionToClasses = PermissionClassReflectionPartFive.loadPermissionPartFive(permissionToClasses);
        permissionToClasses = PermissionClassReflectionPartSix.loadPermissionPartSix(permissionToClasses);
        permissionToClasses = PermissionClassReflectionPartSeven.loadPermissionPartSeven(permissionToClasses);


    }


    public List<String> getMethodsfromPermission(String permission){
        return permissionToClasses.get(permission);
    }

    public String getPermissionfromMethod(String method){
        for(Map.Entry<String,List<String>>entry:permissionToClasses.entrySet()){
            if(entry.getValue().contains(method)){
                return entry.getKey();
            }
        }
        return "";
    }

    public Map<String,List<String>> getPermissionClassMapping(){
        return permissionToClasses;
    }

}
