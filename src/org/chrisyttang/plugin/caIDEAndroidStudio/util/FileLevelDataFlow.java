package org.chrisyttang.plugin.caIDEAndroidStudio.util;

import com.intellij.codeInspection.dataFlow.StandardDataFlowRunner;
import com.intellij.codeInspection.dataFlow.value.DfaValueFactory;
import com.intellij.openapi.project.Project;
import com.intellij.psi.*;
import com.intellij.psi.util.PsiTreeUtil;
import com.intellij.refactoring.util.classRefs.ClassReferenceScanner;
import com.intellij.refactoring.util.classRefs.ClassReferenceSearchingScanner;

import java.util.LinkedList;
import java.util.List;

public class FileLevelDataFlow {

    public static List<PsiFile> getDataFlow(PsiFile file,Project project){
        List<PsiFile>files = new LinkedList<>();
        files.add(file);

        //StandardDataFlowRunner dataFlowRunner = new StandardDataFlowRunner();
        //PsiMethodVisitor psiMethodVisitor = new PsiMethodVisitor();
        PsiJavaFile javaFile = (PsiJavaFile)file;
        PsiClass[] psiClasses = javaFile.getClasses();

        for(PsiClass psiClass:psiClasses) {
            ClassReferenceSearchingScanner scanner = new ClassReferenceSearchingScanner(psiClass);
            PsiReference[] psiReferences = scanner.findReferences();
            for(PsiReference reference:psiReferences){
                PsiElement element = reference.getElement();
                PsiJavaFile targetFile = PsiTreeUtil.getParentOfType(element,PsiJavaFile.class);
                if(targetFile!=null && !files.contains(targetFile)) {
                    if(targetFile.getVirtualFile()!=null) {
                        files.add(targetFile);
                    }
                }
            }
        }

        return files;
    }

}
