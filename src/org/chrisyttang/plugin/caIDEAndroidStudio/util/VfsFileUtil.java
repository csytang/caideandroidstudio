package org.chrisyttang.plugin.caIDEAndroidStudio.util;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.LocalFileSystem;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.search.FilenameIndex;
import com.intellij.psi.search.GlobalSearchScope;

import java.io.File;

public class VfsFileUtil {

    public static boolean FileExist(Project project, String relatedfilepath){
        return FilenameIndex.getFilesByName(project,relatedfilepath, GlobalSearchScope.allScope(project)).length!=0;
    }

    public static VirtualFile getFile(Project project, String relatedfilepath){
        if(FilenameIndex.getFilesByName(project,relatedfilepath, GlobalSearchScope.allScope(project)).length!=0){
            return FilenameIndex.getFilesByName(project,relatedfilepath, GlobalSearchScope.allScope(project))[0].getVirtualFile();
        }else{
            return null;
        }
    }
}
