package org.chrisyttang.plugin.caIDEAndroidStudio.util;

import org.chrisyttang.androidmanifest.dom.ActivityNode;
import org.chrisyttang.plugin.caIDEAndroidStudio.recommendation.ActivityGroup;

import java.util.*;

public class RecommendationSolutionUtil {


    /*
     *
     */
    public static Map<String,List<Object>> getPossibleSolutions(Set<ActivityGroup> _activitySet, Map<ActivityNode,ActivityGroup> _activityNodeToGroup, int feature_num_required){
        Map<String,List<Object>> solutionstr_activitygroup_mapping = new HashMap<String,List<Object>>();

        List<List<Object>> tempresults = new LinkedList<>();
        ActivityGroup[] activityGroups = (ActivityGroup[])_activitySet.toArray(new ActivityGroup[_activitySet.size()]);

        Combination.combinationSelect(activityGroups,feature_num_required,tempresults);

        for(List<Object> result:tempresults){
            String solution_content = "[";
            for(Object obj:result){
                ActivityGroup activityGroup = (ActivityGroup)obj;
                solution_content+=activityGroup.toString();
                solution_content+=",";
            }
            if(result.size()>=1){
                solution_content = solution_content.substring(0,solution_content.length()-1);
            }
            solution_content+="]";
            solutionstr_activitygroup_mapping.put(solution_content,result);
        }

        return solutionstr_activitygroup_mapping;
    }
}
