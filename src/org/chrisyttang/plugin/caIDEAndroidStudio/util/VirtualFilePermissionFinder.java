package org.chrisyttang.plugin.caIDEAndroidStudio.util;

import com.intellij.openapi.fileEditor.impl.LoadTextUtil;
import com.intellij.openapi.vfs.VirtualFile;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class VirtualFilePermissionFinder {

    public static List<AndroidPermission> getPermissionsFromVirtualFile(VirtualFile file){
        List<AndroidPermission> usedPermissions = new LinkedList<>();
        String content = LoadTextUtil.loadText(file).toString();

        // check whether there is an import
        PermissionClassReflectionUtil permissionClassReflectionUtil = new PermissionClassReflectionUtil();
        Map<String,List<String>> permissionToClasses =  permissionClassReflectionUtil.getPermissionClassMapping();

        for(Map.Entry<String,List<String>>entry:permissionToClasses.entrySet()){
            List<String> values = entry.getValue();
            String permission = entry.getKey();
            for(String value:values){
                String className = value.split("#")[0];
                if(content.contains(className) && !usedPermissions.contains(AndroidPermission.fromString(permission))){
                    if(!permission.contains("android.permission.DUMP"))
                        usedPermissions.add(AndroidPermission.fromString(permission));
                }
            }
        }

        return usedPermissions;
    }
}
