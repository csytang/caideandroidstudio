package org.chrisyttang.plugin.caIDEAndroidStudio.util;

import java.util.LinkedList;

public enum AndroidPermission {
    ACCESS_ALL_DOWNLOADS("android.permission.ACCESS_ALL_DOWNLOADS"),
    ACCESS_COARSE_LOCATION("android.permission.ACCESS_COARSE_LOCATION"),
    ACCESS_DOWNLOAD_MANAGER_ADVANCED("android.permission.ACCESS_DOWNLOAD_MANAGER_ADVANCED"),
    ACCESS_DRM("android.permission.ACCESS_DRM"),
    ACCESS_FINE_LOCATION("android.permission.ACCESS_FINE_LOCATION"),
    ACCESS_LOCATION_EXTRA_COMMANDS("android.permission.ACCESS_LOCATION_EXTRA_COMMANDS"),
    ACCESS_MOCK_LOCATION("android.permission.ACCESS_MOCK_LOCATION"),
    ACCESS_NETWORK_STATE("android.permission.ACCESS_NETWORK_STATE"),
    ACCESS_WIFI_STATE("android.permission.ACCESS_WIFI_STATE"),
    AUTHENTICATE_ACCOUNTS("android.permission.AUTHENTICATE_ACCOUNTS"),
    BATTERY_STATS("android.permission.BATTERY_STATS"),
    BIND_DEVICE_ADMIN("android.permission.BIND_DEVICE_ADMIN"),
    BLUETOOTH("android.permission.BLUETOOTH"),
    BLUETOOTH_ADMIN("android.permission.BLUETOOTH_ADMIN"),
    CALL_PRIVILEGED("android.permission.CALL_PRIVILEGED"),
    CHANGE_COMPONENT_ENABLED_STATE("android.permission.CHANGE_COMPONENT_ENABLED_STATE"),
    CHANGE_NETWORK_STATE("android.permission.CHANGE_NETWORK_STATE"),
    CHANGE_WIFI_MULTICAST_STATE("android.permission.CHANGE_WIFI_MULTICAST_STATE"),
    CHANGE_WIFI_STATE("android.permission.CHANGE_WIFI_STATE"),
    CLEAR_APP_CACHE("android.permission.CLEAR_APP_CACHE"),
    CLEAR_APP_USER_DATA("android.permission.CLEAR_APP_USER_DATA"),
    CONNECTIVITY_INTERNAL("android.permission.CONNECTIVITY_INTERNAL"),
    DELETE_CACHE_FILES("android.permission.DELETE_CACHE_FILES"),
    DELETE_PACKAGES("android.permission.DELETE_PACKAGES"),
    DEVICE_POWER("android.permission.DEVICE_POWER"),
    DISABLE_KEYGUARD("android.permission.DISABLE_KEYGUARD"),
    DOWNLOAD_CACHE_NON_PURGEABLE("android.permission.DOWNLOAD_CACHE_NON_PURGEABLE"),
    DUMP("android.permission.DUMP"),
    EXPAND_STATUS_BAR("android.permission.EXPAND_STATUS_BAR"),
    GET_ACCOUNTS("android.permission.GET_ACCOUNTS"),
    GET_PACKAGE_SIZE("android.permission.GET_PACKAGE_SIZE"),
    GLOBAL_SEARCH("android.permission.GLOBAL_SEARCH"),
    GRANT_REVOKE_PERMISSIONS("android.permission.GRANT_REVOKE_PERMISSIONS"),
    INSTALL_DRM("android.permission.INSTALL_DRM"),
    INSTALL_LOCATION_PROVIDER("android.permission.INSTALL_LOCATION_PROVIDER"),
    INSTALL_PACKAGES("android.permission.INSTALL_PACKAGES"),
    INTERNET("android.permission.INTERNET"),
    MANAGE_ACCOUNTS("android.permission.MANAGE_ACCOUNTS"),
    MANAGE_APP_TOKENS("android.permission.MANAGE_APP_TOKENS"),
    MANAGE_NETWORK_POLICY("android.permission.MANAGE_NETWORK_POLICY"),
    MANAGE_USB("android.permission.MANAGE_USB"),
    MODIFY_AUDIO_SETTINGS("android.permission.MODIFY_AUDIO_SETTINGS"),
    MODIFY_NETWORK_ACCOUNTING("android.permission.MODIFY_NETWORK_ACCOUNTING"),
    MODIFY_PHONE_STATE("android.permission.MODIFY_PHONE_STATE"),
    MOVE_PACKAGE("android.permission.MOVE_PACKAGE"),
    NFC("android.permission.NFC"),
    PACKAGE_USAGE_STATS("android.permission.PACKAGE_USAGE_STATS"),
    PACKAGE_VERIFICATION_AGENT("android.permission.PACKAGE_VERIFICATION_AGENT"),
    READ_CONTACTS("android.permission.READ_CONTACTS"),
    READ_FRAME_BUFFER("android.permission.READ_FRAME_BUFFER"),
    READ_LOGS("android.permission.READ_LOGS"),
    READ_NETWORK_USAGE_HISTORY("android.permission.READ_NETWORK_USAGE_HISTORY"),
    READ_PHONE_STATE("android.permission.READ_PHONE_STATE"),
    READ_PRIVILEGED_PHONE_STATE("android.permission.READ_PRIVILEGED_PHONE_STATE"),
    READ_PROFILE("android.permission.READ_PROFILE"),
    READ_SOCIAL_STREAM("android.permission.READ_SOCIAL_STREAM"),
    READ_SYNC_SETTINGS("android.permission.READ_SYNC_SETTINGS"),
    READ_SYNC_STATS("android.permission.READ_SYNC_STATS"),
    REBOOT("android.permission.REBOOT"),
    RECEIVE_SMS("android.permission.RECEIVE_SMS"),
    REMOTE_AUDIO_PLAYBACK("android.permission.REMOTE_AUDIO_PLAYBACK"),
    SEND_SMS("android.permission.SEND_SMS"),
    SERIAL_PORT("android.permission.SERIAL_PORT"),
    SET_ANIMATION_SCALE("android.permission.SET_ANIMATION_SCALE"),
    SET_KEYBOARD_LAYOUT("android.permission.SET_KEYBOARD_LAYOUT"),
    SET_ORIENTATION("android.permission.SET_ORIENTATION"),
    SET_POINTER_SPEED("android.permission.SET_POINTER_SPEED"),
    SET_PREFERRED_APPLICATIONS("android.permission.SET_PREFERRED_APPLICATIONS"),
    SHUTDOWN("android.permission.SHUTDOWN"),
    STATUS_BAR("android.permission.STATUS_BAR"),
    STATUS_BAR_SERVICE("android.permission.STATUS_BAR_SERVICE"),
    UPDATE_DEVICE_STATS("android.permission.UPDATE_DEVICE_STATS"),
    UPDATE_LOCK("android.permission.UPDATE_LOCK"),
    USE_CREDENTIALS("android.permission.USE_CREDENTIALS"),
    USE_SIP("android.permission.USE_SIP"),
    VIBRATE("android.permission.VIBRATE"),
    WAKE_LOCK("android.permission.WAKE_LOCK"),
    WRITE_APN_SETTINGS("android.permission.WRITE_APN_SETTINGS"),
    WRITE_CONTACTS("android.permission.WRITE_CONTACTS"),
    WRITE_PROFILE("android.permission.WRITE_PROFILE"),
    WRITE_SECURE_SETTINGS("android.permission.WRITE_SECURE_SETTINGS"),
    WRITE_SETTINGS("android.permission.WRITE_SETTINGS"),
    WRITE_SOCIAL_STREAM("android.permission.WRITE_SOCIAL_STREAM"),
    WRITE_SYNC_SETTINGS("android.permission.WRITE_SYNC_SETTINGS"),
    ACCESS_PROVIDER("com.android.email.permission.ACCESS_PROVIDER"),
    ADD_VOICEMAIL("com.android.voicemail.permission.ADD_VOICEMAIL"),
    READ_WRITE_ALL_VOICEMAIL("com.android.voicemail.permission.READ_WRITE_ALL_VOICEMAIL"),
    PARENT("Parent"),
    UNKNOWN("Unknown");

    private String _value;

    private AndroidPermission(String pvalue){
        this._value = pvalue;
    }

    public String getValue(){
        return _value;
    }

    public static AndroidPermission fromString(String text){
        for(AndroidPermission permission:AndroidPermission.values()){
            if(permission._value.equalsIgnoreCase(text)){
                return permission;
            }
        }
        return null;
    }

}
