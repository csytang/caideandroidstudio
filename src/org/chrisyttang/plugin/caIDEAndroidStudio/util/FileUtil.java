package org.chrisyttang.plugin.caIDEAndroidStudio.util;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * File related util
 */
public class FileUtil {

   public static void writeFile(File targetFile, String content){
        try {
            File current = targetFile;
            while(!current.getParentFile().exists()){
                current.getParentFile().mkdir();
                current = current.getParentFile();
            }
            FileWriter fileWriter = new FileWriter(targetFile);
            fileWriter.write(content);
            fileWriter.flush();
            fileWriter.close();
        }catch (IOException e){
            e.printStackTrace();
        }
    }
}
