package org.chrisyttang.plugin.caIDEAndroidStudio.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class Combination {


    public static void combinationSelect(Object[]dataList,int n,List<List<Object>> result){
        combinationSelect(dataList,0,new Object[n],0,result);
    }

    private static void combinationSelect(Object[] dataList,int dataIndex, Object[] resultList,int resultIndex,List<List<Object>> result){

        int resultLen = resultList.length;
        int resultCount = resultIndex+1;
        if(resultCount > resultLen){
            //System.out.println(Arrays.asList(resultList));
            result.add(new LinkedList<Object>(Arrays.asList(resultList)));
            return;
        }

        for(int i = dataIndex; i < dataList.length + resultCount - resultLen;i++){
            resultList[resultIndex] = dataList[i];
            combinationSelect(dataList,i+1,resultList,resultIndex+1,result);
        }
    }



    /*
    public static void main(String[] args) {
        List<List<Object>> contentresult = new LinkedList<>();
         combinationSelect(new String[] {
                "1", "2", "3", "4","5"
        }, 3,contentresult);

         System.out.println(contentresult);
    }
    */
}
