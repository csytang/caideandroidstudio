package org.chrisyttang.plugin.caIDEAndroidStudio.util;


import com.intellij.openapi.module.Module;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * Created by szeibert on 2014.11.27..
 */
public class AndroidResourceUtils extends CommonUtils {

    private static File androidManifest;
    private static List<File> androidResFolders;


    public static List<File> getAndroidResFolders(Module module) {
        if (androidResFolders == null) {
            androidResFolders = new ArrayList<File>();
            findAndroidResFolders(new File(module.getModuleFile().getPath().substring(0,module.getModuleFile().getPath().lastIndexOf("/"))));
        }
        return androidResFolders;
    }

    public static List<File> getAndroidAssetsFolders(Module module) {
        if (androidResFolders == null) {
            androidResFolders = new ArrayList<File>();
            findAndroidAssetsFolders(new File(module.getModuleFile().getPath().substring(0,module.getModuleFile().getPath().lastIndexOf("/"))));
        }
        return androidResFolders;
    }

    public static void reset() {
        androidResFolders = null;
        androidManifest = null;
    }

    private static void findAndroidResFolders(File folder) {
        for (File f : folder.listFiles()) {
            if (f.getName().equals("res") && f.isDirectory()) {
                androidResFolders.add(f);
                return;
            }
            if (isNonBuildDirectory(f)) {
                findAndroidResFolders(f);
            }
        }
    }

    private static void findAndroidAssetsFolders(File folder) {
        for (File f : folder.listFiles()) {
            if (f.getName().equals("assets") && f.isDirectory()) {
                androidResFolders.add(f);
                return;
            }
            if (isNonBuildDirectory(f)) {
                findAndroidResFolders(f);
            }
        }
    }
}
