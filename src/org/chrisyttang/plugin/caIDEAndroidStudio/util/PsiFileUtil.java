package org.chrisyttang.plugin.caIDEAndroidStudio.util;

import com.intellij.analysis.AnalysisScope;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.command.WriteCommandAction;
import com.intellij.openapi.module.Module;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.roots.ContentIterator;
import com.intellij.openapi.roots.ProjectFileIndex;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiFile;
import com.intellij.psi.PsiJavaFile;
import com.intellij.psi.PsiManager;
import com.intellij.psi.PsiRecursiveElementVisitor;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.HashSet;
import java.util.Set;

public class PsiFileUtil {

    /**
     *
     * @param file input file in virtual file system
     * @param project current project
     * @return the corresponding PsiFile for the input VirtualFile
     */
    public static PsiFile VirtualFileToPsiFileConverter(VirtualFile file,Project project){
        PsiManager psiManager = PsiManager.getInstance(project);
        return psiManager.findFile(file);
    }


    public static Set<VirtualFile> exploreAllFileInProject(Project project){
        Set<VirtualFile> allVirtualFiles = new HashSet<VirtualFile>();
        ProjectFileIndex projectFileIndex = ProjectFileIndex.SERVICE.getInstance(project);
        // visit all files

        Runnable runnable = new Runnable() {

            @Override
            public void run() {
                projectFileIndex.iterateContent(new ContentIterator() {
                    @Override
                    public boolean processFile(VirtualFile virtualFile) {
                        allVirtualFiles.add(virtualFile);
                        return true;
                    }
                });
            }
        };
        WriteCommandAction.runWriteCommandAction(project, runnable);

        return allVirtualFiles;
    }



    public static Set<VirtualFile> exploreAllFileInModule(Module module){

        Set<VirtualFile> allVirtualFiles = new HashSet<VirtualFile>();

        Project project = module.getProject();

        Runnable runnable = new Runnable() {

            @Override
            public void run() {
                AnalysisScope moduleScope = new AnalysisScope(module);
                moduleScope.accept(new PsiRecursiveElementVisitor() {
                    @Override
                    public void visitFile(final PsiFile file) {
                        if (file instanceof PsiJavaFile) {
                            PsiJavaFile psiJavaFile = (PsiJavaFile) file;

                            allVirtualFiles.add(file.getVirtualFile());
                        }
                    }

                });
            }
        };

        WriteCommandAction.runWriteCommandAction(project, runnable);

        return allVirtualFiles;
    }

    public static Set<VirtualFile> exploreAllFileNotInModule(Project project,Module module){
        Set<VirtualFile> allVirtualFiles = new HashSet<VirtualFile>();
        ProjectFileIndex projectFileIndex = ProjectFileIndex.SERVICE.getInstance(project);
        // visit all files


        Runnable runnable = new Runnable() {

            @Override
            public void run() {
                projectFileIndex.iterateContent(new ContentIterator() {
                    @Override
                    public boolean processFile(VirtualFile virtualFile) {
                        if(!projectFileIndex.getModuleForFile(virtualFile).equals(module)){
                            allVirtualFiles.add(virtualFile);
                        }

                        return true;
                    }
                });
            }
        };
        WriteCommandAction.runWriteCommandAction(project, runnable);

        return allVirtualFiles;
    }


    public static Set<VirtualFile> exploreAllNonModuleFile(Project project){

        Set<VirtualFile> allVirtualFiles = new HashSet<VirtualFile>();
        ProjectFileIndex projectFileIndex = ProjectFileIndex.SERVICE.getInstance(project);
        // visit all files

        Runnable runnable = new Runnable() {

            @Override
            public void run() {
                projectFileIndex.iterateContent(new ContentIterator() {
                    @Override
                    public boolean processFile(VirtualFile virtualFile) {
                        Module module = projectFileIndex.getModuleForFile(virtualFile);
                        if(module==null){
                            allVirtualFiles.add(virtualFile);
                        }

                        return true;
                    }
                });
            }
        };
        WriteCommandAction.runWriteCommandAction(project, runnable);

        return allVirtualFiles;
    }




    /**
     * this action will clone a file from source project to the target project
     */
    public static void cloneFile(VirtualFile source_File, String targetVariantPath, Project source){
        // 1. get the source path of sourcefile
        String source_FilePath = source_File.getPath();

        // 2. build the target file path of target file
        String source_projectPath = source.getBasePath();
        String source_FileRelativePath = source_FilePath.substring(source_projectPath.length(),source_FilePath.length());

        // 3. check whether the parent directory is created
        String target_FilePath = targetVariantPath+source_FileRelativePath;

        // 4. check whether the target file exists
        File target_File = new File(target_FilePath);

        // 4.1 already created
        if(target_File.exists()){
            return;
        }

        // 4.2 not created, get its parents
        VirtualFile source_ParentDir = source_File.getParent();

        // source directory path
        String source_DirPath = source_ParentDir.getPath();
        String source_DirRelativePath = source_DirPath.substring(source_projectPath.length(),source_DirPath.length());

        // the relative file path of its parent
        String target_ParentDirPath = targetVariantPath+source_DirRelativePath;


        File parentDir = new File(target_ParentDirPath);
        if(!parentDir.exists()){
            // 4.3 create the target folder first
            cloneFile(source_ParentDir, targetVariantPath, source);
        }

        // 4.4 copy the file
        if(source_File.isDirectory()){
            boolean makedir = target_File.mkdir();
            if(!makedir){
                try {
                    throw new Exception("Fail to create a directory");
                } catch (Exception e) {
                    e.printStackTrace();
                    //System.exit(-1);
                }
            }
        }else{
            File sourceFile = new File(source_FilePath);
            try {
                Files.copy(sourceFile.toPath(),target_File.toPath(), StandardCopyOption.REPLACE_EXISTING);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    /**
     * this action will clone a file from source project to the target project
     */
    public static void cloneFile(File source_File, String targetVariantPath, Project source){
        // 1. get the source path of sourcefile
        String source_FilePath = source_File.getPath();

        // 2. build the target file path of target file
        String source_projectPath = source.getBasePath();
        String source_FileRelativePath = source_FilePath.substring(source_projectPath.length(),source_FilePath.length());

        // 3. check whether the parent directory is created
        String target_FilePath = targetVariantPath+source_FileRelativePath;

        // 4. check whether the target file exists
        File target_File = new File(target_FilePath);

        // 4.1 already created
        if(target_File.exists()){
            return;
        }

        // 4.2 not created, get its parents
        File source_ParentDir = source_File.getParentFile();

        // source directory path
        String source_DirPath = source_ParentDir.getPath();
        String source_DirRelativePath = source_DirPath.substring(source_projectPath.length(),source_DirPath.length());

        // the relative file path of its parent
        String target_ParentDirPath = targetVariantPath+source_DirRelativePath;


        File parentDir = new File(target_ParentDirPath);
        if(!parentDir.exists()){
            // 4.3 create the target folder first
            cloneFile(source_ParentDir, targetVariantPath, source);
        }

        // 4.4 copy the file
        if(source_File.isDirectory()){
            boolean makedir = target_File.mkdir();
            if(!makedir){
                try {
                    throw new Exception("Fail to create a directory");
                } catch (Exception e) {
                    e.printStackTrace();
                    //System.exit(-1);
                }
            }
        }else{
            File sourceFile = new File(source_FilePath);
            try {
                Files.copy(sourceFile.toPath(),target_File.toPath(), StandardCopyOption.REPLACE_EXISTING);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

}
