package org.chrisyttang.plugin.caIDEAndroidStudio.util;

import com.intellij.openapi.util.IconLoader;

import javax.swing.*;

public class IconUtil {
    public static final Icon FEATUREMODEL_FILE = IconLoader.getIcon("icons/featuremodel.png");

    public static final Icon FEATUERMODEL_EDITOR = IconLoader.getIcon("icons/featuremodeleditor.png");

    public static final Icon FEATUREMODLE_EDITOR_FEATURE = IconLoader.getIcon("icons/funcmodule.png");

    public static final Icon FEATUREMODLE_EDITOR_COLOR = IconLoader.getIcon("icons/color.png");
}
