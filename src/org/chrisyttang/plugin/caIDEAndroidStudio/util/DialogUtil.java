package org.chrisyttang.plugin.caIDEAndroidStudio.util;

import com.intellij.openapi.ui.DialogBuilder;
import com.intellij.openapi.ui.Messages;

import javax.swing.*;

public class DialogUtil {
    public static boolean createDialog(String message) {
        DialogBuilder builder = new DialogBuilder();
        builder.setTitle("Message");
        builder.resizable(false);
        builder.setCenterPanel(new JLabel(message, Messages.getInformationIcon(), SwingConstants.CENTER));
        builder.setButtonsAlignment(SwingConstants.CENTER);
        return  builder.show() == 0;
    }
}
