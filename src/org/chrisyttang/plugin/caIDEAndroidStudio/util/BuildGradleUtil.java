package org.chrisyttang.plugin.caIDEAndroidStudio.util;

import com.intellij.openapi.diagnostic.Logger;

import java.util.Stack;

public class BuildGradleUtil {

    public static final Logger LOG = Logger.getInstance("com.intellij.openapi.diagnostic.Logger.BuildGradleUtil");

    public static String getDependencies(String gradleContent){
        String dependencies = "";

        int startIndex = gradleContent.indexOf("dependencies");
        Stack <Character> stack = new Stack<Character>();
        stack.push('{');

        boolean firstBracket = true;
        int endIndex = startIndex;

        for(int index = startIndex;index < gradleContent.length();index++){
            endIndex = index;

            if(stack.isEmpty()){
                break;
            }
            char current = gradleContent.charAt(index);
            if(current=='{'){
                if(firstBracket){
                    firstBracket = false;
                    continue;
                }
                stack.push('{');
            }else if(current=='}'){
                stack.pop();
            }
        }

        dependencies = gradleContent.substring(startIndex,endIndex);

        LOG.info(dependencies);

        return dependencies;
    }
    public static String getAndroid(String gradleContent){
        String android = "";

        int startIndex = gradleContent.indexOf("android");
        Stack <Character> stack = new Stack<Character>();
        stack.push('{');

        boolean firstBracket = true;
        int endIndex = startIndex;

        for(int index = startIndex;index < gradleContent.length();index++){
            endIndex = index;
            if(stack.isEmpty()){
                break;
            }
            char current = gradleContent.charAt(index);
            if(current=='{'){
                if(firstBracket){
                    firstBracket = false;
                    continue;
                }
                stack.push('{');
            }else if(current=='}'){
                stack.pop();
            }
        }

        android = gradleContent.substring(startIndex,endIndex);

        LOG.info(android);

        return android;
    }
}
