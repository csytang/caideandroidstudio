/*
* Copyright (C) 2012 The Android Open Source Project
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package org.chrisyttang.androidmanifest.dom;

import org.w3c.dom.Node;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.StringWriter;
import java.util.Map;


/**
 * Class that represents a comment on the AndroidManifest.xml file
 */
public class CommentNode extends AndroidManifestNode
{
    private String comment = null;
    private Node _node;
    private String _content;

    @Override
    public void addXMLMapping(Node node) {
        _node = node;
        StringWriter writer = new StringWriter();
        Transformer transformer = null;
        try {
            transformer = TransformerFactory.newInstance().newTransformer();
            transformer.transform(new DOMSource(node), new StreamResult(writer));
            _content = writer.toString();
        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        }

    }

    /* (non-Javadoc)
     * @see com.motorola.studio.android.model.manifest.dom.AndroidManifestNode#canContains(com.motorola.studio.android.model.manifest.dom.AndroidManifestNode.NodeType)
     */
    @Override
    protected boolean canContains(NodeType nodeType)
    {
        // Always return false. No child node can be added
        return false;
    }

    /* (non-Javadoc)
     * @see com.motorola.studio.android.model.manifest.dom.AndroidManifestNode#getNodeProperties()
     */
    @Override
    public Map<String, String> getNodeProperties()
    {
        properties.clear();

        return properties;
    }

    /* (non-Javadoc)
     * @see com.motorola.studio.android.model.manifest.dom.AndroidManifestNode#getNodeType()
     */
    @Override
    public NodeType getNodeType()
    {
        return NodeType.Comment;
    }

    /* (non-Javadoc)
     * @see com.motorola.studio.android.model.manifest.dom.AndroidManifestNode#isNodeValid()
     */
    @Override
    protected boolean isNodeValid()
    {
        // Always returns true
        return true;
    }

    /* (non-Javadoc)
     * @see com.motorola.studio.android.model.manifest.dom.AndroidManifestNode#addChild(com.motorola.studio.android.model.manifest.dom.AndroidManifestNode)
     */
    @Override
    public void addChild(AndroidManifestNode child)
    {
        throw new IllegalArgumentException("child nodes cannot be added to a comment node");
    }

    /* (non-Javadoc)
     * @see com.motorola.studio.android.model.manifest.dom.AndroidManifestNode#addUnknownProperty(java.lang.String, java.lang.String)
     */
    @Override
    public boolean addUnknownProperty(String property, String value)
    {
        // Comments do not have properties
        return false;
    }

    /* (non-Javadoc)
     * @see com.motorola.studio.android.model.manifest.dom.AndroidManifestNode#canAddUnknownProperty(java.lang.String)
     */
    @Override
    protected boolean canAddUnknownProperty(String property)
    {
        // Comments do not have properties
        return false;
    }

    /* (non-Javadoc)
     * @see com.motorola.studio.android.model.manifest.dom.AndroidManifestNode#getAllChildrenFromType(com.motorola.studio.android.model.manifest.dom.AndroidManifestNode.NodeType)
     */
    @Override
    protected AndroidManifestNode[] getAllChildrenFromType(NodeType type)
    {
        return new AndroidManifestNode[0];
    }

    @Override
    public String toString() {
        return _content;
    }

    @Override
    public void applyChanges() {

        for(AndroidManifestNode child:this.getChildren()){
            String substring = child.toString();
            child.applyChanges();
            String changedstring = child.toString();
            _content = _content.replaceAll(substring,changedstring);
        }

    }

    /**
     * Sets the comment node content
     * 
     * @param comment the comment node content
     */
    public void setComment(String comment)
    {
        this.comment = comment;
    }

    /**
     * Gets the comment node content
     * 
     * @return the comment node content
     */
    public String getComment()
    {
        return comment;
    }


}
