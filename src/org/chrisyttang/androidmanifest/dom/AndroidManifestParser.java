package org.chrisyttang.androidmanifest.dom;

import org.apache.xerces.parsers.DOMParser;
import org.chrisyttang.androidmanifest.dom.AndroidManifestNode.NodeType;
import org.w3c.dom.*;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.apache.xerces.xni.parser.XMLParserConfiguration;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.nio.file.Files;
import java.util.LinkedList;
import java.util.List;


public class AndroidManifestParser extends AndroidManifestNodeParser{

    DOMParser domParser = new DOMParser();

    /**
     * The nodes present on the xml file root
     */
    protected List<AndroidManifestNode> rootNodes = new LinkedList<AndroidManifestNode>();
    StringReader stringReader = null;
    public AndroidManifestParser(File file){
        try {

            String content = new String (Files.readAllBytes(file.toPath()));
            stringReader = new StringReader(content);
            rootNodes.clear();
            domParser.parse(new InputSource(stringReader));

        } catch (IOException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } finally {
            if (stringReader != null)
            {
                stringReader.close();
            }
        }

        NodeList children = domParser.getDocument().getChildNodes();
        for (int i = 0; i < children.getLength(); i++)
        {
            Node node = children.item(i);
            parseNode(node, null);
        }

    }

    protected void parseNode(Node node, AndroidManifestNode rootNode){
        AndroidManifestNode amNode;
        NodeType nodeType = identifyNode(node);
        Node xmlNode;
        NodeList xmlChildNodes;
        NamedNodeMap attributes = node.getAttributes();
        switch (nodeType)
        {
            case Manifest:
                amNode = parseManifestNode(attributes);
                break;
            case UsesPermission:
                amNode = parseUsesPermissionNode(attributes);
                break;
            case Permission:
                amNode = parsePermissionNode(attributes);
                break;
            case PermissionTree:
                amNode = parsePermissionTreeNode(attributes);
                break;
            case PermissionGroup:
                amNode = parsePermissionGroupNode(attributes);
                break;
            case Instrumentation:
                amNode = parseInstrumentationNode(attributes);
                break;
            case UsesSdk:
                amNode = parseUsesSdkNode(attributes);
                break;
            case Application:
                amNode = parseApplicationNode(attributes);
                break;
            case Activity:
                amNode = parseActivityNode(attributes);
                break;
            case IntentFilter:
                amNode = parseIntentFilterNode(attributes);
                break;
            case Action:
                amNode = parseActionNode(attributes);
                break;
            case Category:
                amNode = parseCategoryNode(attributes);
                break;
            case Data:
                amNode = parseDataNode(attributes);
                break;
            case MetaData:
                amNode = parseMetadataNode(attributes);
                break;
            case ActivityAlias:
                amNode = parseActivityAliasNode(attributes);
                break;
            case Service:
                amNode = parseServiceNode(attributes);
                break;
            case Receiver:
                amNode = parseReceiverNode(attributes);
                break;
            case Provider:
                amNode = parseProviderNode(attributes);
                break;
            case GrantUriPermission:
                amNode = parseGrantUriPermissionNode(attributes);
                break;
            case UsesLibrary:
                amNode = parseUsesLibraryNode(attributes);
                break;
            case UsesFeature:
                amNode = parseUsesFeatureNode(attributes);
                break;
            case Comment:
                amNode = parseCommentNode((Comment) node);
                break;
            default:
                amNode = parseUnknownNode(node.getNodeName(), attributes);
        }

        // add mapping
        amNode.addXMLMapping(node);

        xmlChildNodes = node.getChildNodes();
        for (int i = 0; i < xmlChildNodes.getLength(); i++)
        {
            xmlNode = xmlChildNodes.item(i);
            if ((xmlNode instanceof Element) || (xmlNode instanceof Comment))
            {
                parseNode(xmlNode, amNode);
            }
        }
        if (rootNode == null)
        {
            rootNodes.add(amNode);
        }
        else
        {
            rootNode.addChild(amNode);
        }

    }


    /**
     * Identifies a XML Node type as an AndroidManifestNode type.
     *
     * @param xmlNode The XML Node
     * @return The corresponding AndroidManifestNode type to the XML Node
     */
    private NodeType identifyNode(Node xmlNode)
    {
        NodeType identifiedType = NodeType.Unknown;
        String nodeName = xmlNode.getNodeName();
        String thisNodeName;
        if (xmlNode instanceof Comment)
        {
            identifiedType = NodeType.Comment;
        }
        else
        {
            for (NodeType nodeType : NodeType.values())
            {
                thisNodeName = AndroidManifestNode.getNodeName(nodeType);
                if (thisNodeName.equalsIgnoreCase(nodeName))
                {
                    identifiedType = nodeType;
                    break;
                }
            }
        }
        return identifiedType;
    }


    public List<AndroidManifestNode> getRoots() {
        return rootNodes;
    }

    public static void main(String[]args){
        AndroidManifestParser parser = new AndroidManifestParser(new File(args[0]));

        for(AndroidManifestNode node:parser.getRoots()){
            if(node instanceof ManifestNode){
                ManifestNode manifestNode = (ManifestNode)node;

                manifestNode.setPackage("mypacakge");
            }
        }
        for(AndroidManifestNode node:parser.getRoots()){
            System.out.println(node.toString());
        }

    }
}
