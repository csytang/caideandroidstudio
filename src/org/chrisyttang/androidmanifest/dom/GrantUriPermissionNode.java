/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.chrisyttang.androidmanifest.dom;

import org.w3c.dom.Node;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.StringWriter;
import java.util.Map;


/**
 * Class that represents a <grant-uri-permission> node on AndroidManifest.xml file
 */
public class GrantUriPermissionNode extends AndroidManifestNode implements
        IAndroidManifestProperties
{
    private Node _node;
    private String _content;
    static
    {
        defaultProperties.add(PROP_PATH);
        defaultProperties.add(PROP_PATHPREFIX);
        defaultProperties.add(PROP_PATHPATTERN);
    }

    /**
     * The path property
     */
    private String propPath = null;

    /**
     * The pathPrefix property
     */
    private String propPathPrefix = null;

    /**
     * The pathPattern property
     */
    private String propPathPattern = null;

    @Override
    public void addXMLMapping(Node node) {
        _node = node;
        StringWriter writer = new StringWriter();
        Transformer transformer = null;
        try {
            transformer = TransformerFactory.newInstance().newTransformer();
            transformer.transform(new DOMSource(node), new StreamResult(writer));
            _content = writer.toString();
        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        }
    }

    /* (non-Javadoc)
     * @see com.motorola.studio.android.model.manifest.dom.AndroidManifestNode#canContains(com.motorola.studio.android.model.manifest.dom.AndroidManifestNode.NodeType)
     */
    @Override
    protected boolean canContains(NodeType nodeType)
    {
        // Always returns false. This node can not contain children.
        return false;
    }

    /* (non-Javadoc)
     * @see com.motorola.studio.android.model.manifest.dom.AndroidManifestNode#getNodeProperties()
     */
    @Override
    public Map<String, String> getNodeProperties()
    {
        properties.clear();

        if ((propPath != null) && (propPath.trim().length() > 0))
        {
            properties.put(PROP_PATH, propPath);
        }

        if ((propPathPattern != null) && (propPathPattern.trim().length() > 0))
        {
            properties.put(PROP_PATHPATTERN, propPathPattern);
        }

        if ((propPathPrefix != null) && (propPathPrefix.trim().length() > 0))
        {
            properties.put(PROP_PATHPREFIX, propPathPrefix);
        }

        return properties;
    }

    @Override
    public String toString() {
        return _content;
    }

    @Override
    public void applyChanges() {
        for(AndroidManifestNode child:this.getChildren()){
            String substring = child.toString();
            child.applyChanges();
            String changedstring = child.toString();
            _content.replaceAll(substring,changedstring);
        }
    }

    /* (non-Javadoc)
     * @see com.motorola.studio.android.model.manifest.dom.AndroidManifestNode#getNodeType()
     */
    @Override
    public NodeType getNodeType()
    {
        return NodeType.GrantUriPermission;
    }

    /* (non-Javadoc)
     * @see com.motorola.studio.android.model.manifest.dom.AndroidManifestNode#isNodeValid()
     */
    @Override
    protected boolean isNodeValid()
    {
        return ((propPath != null) && (propPath.trim().length() > 0))
                || ((propPathPattern != null) && (propPathPattern.trim().length() > 0))
                || ((propPathPrefix != null) && (propPathPrefix.trim().length() > 0));
    }

    /**
     * Gets the path property value
     * 
     * @return the path property value
     */
    public String getPath()
    {
        return propPath;
    }

    /**
     * Sets the path property value. Set it to null to remove it.
     * 
     * @param path the path property value
     */
    public void setPath(String path)
    {
        if(this.propPath!=null){
            if(this.propPath.isEmpty()){
                _content = _content.replaceAll(this.propPath,path);
            }
        }
        this.propPath = path;
    }

    /**
     * Gets the pathPrefix property value
     * 
     * @return the pathPrefix property value
     */
    public String getPathPrefix()
    {
        return propPathPrefix;
    }

    /**
     * Sets the pathPrefix property value. Set it to null to remove it.
     * 
     * @param pathPrefix the pathPrefix property value
     */
    public void setPathPrefix(String pathPrefix)
    {
        if(this.propPathPrefix!=null){
            if(this.propPathPrefix.isEmpty()){
                _content = _content.replaceAll(this.propPathPrefix,pathPrefix);
            }
        }
        this.propPathPrefix = pathPrefix;
    }

    /**
     * Gets the pathPattern property value
     * 
     * @return the pathPattern property value
     */
    public String getPathPattern()
    {
        return propPathPattern;
    }

    /**
     * Sets the pathPattern property value. Set it to null to remove it.
     * 
     * @param pathPattern the pathPattern property value
     */
    public void setPathPattern(String pathPattern)
    {
        if(this.propPathPattern!=null){
            if(this.propPathPattern.isEmpty()){
                _content = _content.replaceAll(this.propPathPattern,pathPattern);
            }
        }
        this.propPathPattern = pathPattern;
    }


}
